/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.monteCarlo.pi

import bench.kernels.monteCarlo.MonteCarloCore

import bench.utils.components.random.{TauswortheConfig, TauswortheURNG}
import bench.utils.hardware.{Delay, RingElem}

import chisel3._
import chisel3.experimental.FixedPoint
import chisel3.util.{Cat, ShiftRegister}
import dsptools.numbers.Ring

/** The Monte Carlo core for computing Pi (might be not the best way to compute but it's a core)
 *  @param elemType
 *    type of the data
 *  @param urngConfig
 *    tuple of TauswortheConfig (needed to setup random generator)
 */
class PiCore[T <: Data: Ring](elemType: RingElem[T], urngConfig: (Array[TauswortheConfig], Array[TauswortheConfig]))
    extends MonteCarloCore(elemType) {
  require(elemType.value.isInstanceOf[FixedPoint], "Only fixed point are supported.")

  val fixedPoint = elemType.value.asInstanceOf[FixedPoint]
  val urng0      = Module(new TauswortheURNG(elemType.getWidth, urngConfig._1))
  val urng1      = Module(new TauswortheURNG(elemType.getWidth, urngConfig._2))

  // Normalize Tausworthe output to create a 2d point in a unit square
  val x = Cat(
      0.U(fixedPoint.getWidth - fixedPoint.binaryPoint.get),
      urng0.io.urng(fixedPoint.binaryPoint.get - 1, 0)
  ).asTypeOf(elemType)
  val y = Cat(
      0.U(fixedPoint.getWidth - fixedPoint.binaryPoint.get),
      urng1.io.urng(fixedPoint.binaryPoint.get - 1, 0)
  ).asTypeOf(elemType)

  // Compute the squared norm
  val normPow2 = (x * x + y * y) - elemType.one

  // Return 1 if it's inside the trigonometric circle
  when(normPow2.isPositive) {
    io.output.bits := elemType.zero
  } otherwise {
    io.output.bits := elemType.one
  }

  io.output.valid := ShiftRegister(
      true.B,
      Delay.getMultDelay(elemType) + Delay.getAddDelay(elemType) * 2,
      false.B,
      true.B
  )
}
