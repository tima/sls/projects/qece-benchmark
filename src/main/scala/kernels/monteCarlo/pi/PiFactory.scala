/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.monteCarlo.pi

import bench.kernels.monteCarlo.{MonteCarloCore, MonteCarloFactory}

import bench.utils.components.random.TauswortheConfig
import bench.utils.hardware.RingElem

import chisel3.util.log2Ceil
import chisel3.{Data, Module}
import dsptools.numbers.Ring

/** Factory for Pi cores
 *  @param elemType
 *    type of the data (must be a fixed point because of the LUT functions)
 *  @param urngConfig
 *    iterator of TauswortheConfig (needed to setup random generator)
 */
class PiFactory[T <: Data: Ring](
    elemType: RingElem[T],
    urngConfig: Iterator[(Array[TauswortheConfig], Array[TauswortheConfig])]
) extends MonteCarloFactory[T] {
  override def generateCores(nbCore: Int): Seq[MonteCarloCore[T]] = {
    val cores = (0 until nbCore).map(_ =>
      Module(
          new PiCore(
              elemType,
              urngConfig.next()
          )
      )
    )

    // Up cast
    cores.map(core => core.asInstanceOf[MonteCarloCore[T]])
  }

  override def postProcessing(input: RingElem[T]): RingElem[T] = {
    input << log2Ceil(4) // Multiply by 4 to get Pi
  }

  override def getPostProcessingPipelineLevel(elemType: RingElem[T]): Int = 0
}
