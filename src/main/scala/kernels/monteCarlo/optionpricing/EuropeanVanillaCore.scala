/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.monteCarlo.optionpricing

import bench.kernels.monteCarlo.MonteCarloCore

import bench.utils.components.LUTFunctionConfig
import bench.utils.components.random.{BoxMuller, GaussGenerator, TauswortheConfig}
import bench.utils.hardware.{Delay, RingElem}
import bench.utils.software.Numeric

import chisel3._
import chisel3.util.{ShiftRegister, log2Ceil}
import dsptools.DspContext
import dsptools.numbers._

/** The Monte Carlo core for computing the European vanilla option pricing using Black Scholes stochastic equations
 *  solved by the Euler-Maruyama method
 *
 *  @param elemType
 *    type of the data (must be a fixed point because of the LUT functions)
 *  @param urngConfig
 *    tuple of TauswortheConfig (needed to setup random generator)
 *  @param boxMullerConfig
 *    configuration of BoxMuller (gives the LUT config)
 *  @param config
 *    Black Scholes parameters
 *  @param sharedROM
 *    Tells to the factory if all BoxMuller module take the same ROM
 */
class EuropeanVanillaCore[T <: Data: Ring](
    elemType: RingElem[T],
    urngConfig: (Array[TauswortheConfig], Array[TauswortheConfig]),
    boxMullerConfig: (LUTFunctionConfig[T], LUTFunctionConfig[T]),
    config: EuropeanVanillaConfig,
    sharedROM: Boolean
) extends MonteCarloCore(elemType) {

  val rom = IO(new Bundle() {
    val sin2pi = Input(Vec(boxMullerConfig._1.size + 1, elemType))
    val sqrtln = Input(Vec(boxMullerConfig._2.size + 1, elemType))
  })

  val signal = elemType.value

  // It will use the multiplication delay to compute different samples (no loosing time)
  val nbElem = Delay.getMultDelay(elemType) + 1
  require(nbElem >= 1, "The multiplication must be pipelined.")

  val gaussGenerator = Module(new GaussGenerator(elemType, urngConfig, boxMullerConfig, sharedROM))
  if (sharedROM) {
    gaussGenerator.rom <> rom
  } else {
    gaussGenerator.rom.sin2pi := boxMullerConfig._1.getROM().values
    gaussGenerator.rom.sqrtln := boxMullerConfig._2.getROM().values
  }

  val s0 = Numeric(config.S0).toRingElem(elemType)
  val q  = Numeric(config.getQW()._1).toRingElem(elemType)
  val w  = Numeric(config.getQW()._2).toRingElem(elemType)

  // Each element represent a different solution of the equation.
  val sums = RegInit(VecInit((0 until nbElem).map(_ => elemType.zero.asTypeOf(elemType))))

  // Counters
  val sumIndex       = RegInit(UInt(log2Ceil(nbElem).W), 0.U)
  val iterationIndex = RegInit(UInt(log2Ceil(config.N).W), 0.U)

  val factor = Wire(signal)
  DspContext.alter(
      DspContext.current.copy(numAddPipes = Delay.getAddDelay(elemType), numMulPipes = Delay.getMultDelay(elemType))
  ) {
    factor := q.asTypeOf(signal) context_+ (w.asTypeOf(signal) context_* gaussGenerator.io.gauss.bits.asTypeOf(signal))
  }

  //val factor = q + w * gaussGenerator.io.gauss.bits

  // Validity signals
  val factorValid = ShiftRegister(
      gaussGenerator.io.gauss.valid,
      Delay.getMultDelay(elemType) + Delay.getAddDelay(elemType),
      false.B,
      true.B
  )
  val delayedOutputValidity = ShiftRegister(factorValid, nbElem + 1)

  // Counter logic
  when(factorValid) {
    when(sumIndex === (nbElem - 1).U) {
      sumIndex := 0.U
      when(iterationIndex === (config.N - 1).U) {
        iterationIndex := 0.U
      } otherwise {
        iterationIndex := iterationIndex + 1.U
      }
    } otherwise {
      sumIndex := sumIndex + 1.U
    }
  }

  val result = Reg(signal)
  DspContext.withNumMulPipes(Delay.getMultDelay(elemType)) {
    result := Mux(iterationIndex === 0.U, s0.asTypeOf(signal), result) context_* factor.asTypeOf(signal)
  }
  // Iteration
  sums(sumIndex) := result.asTypeOf(elemType)

  // Core post processing
  val post = config.HWCorePostProcessing(sums(RegNext(sumIndex)))
  val outputValidity = ShiftRegister(
      delayedOutputValidity && RegNext(iterationIndex === 0.U),
      config.HWCorePostProcessingPipelineLevel(elemType)
  )

  // Outputs
  io.output.valid := outputValidity // Give the validity window
  io.output.bits := post            // Select the valid value in the vector
}

object EuropeanVanillaCore {
  def apply[T <: Data: Ring](
      elemType: RingElem[T],
      urngConfig: (Array[TauswortheConfig], Array[TauswortheConfig]),
      config: EuropeanVanillaConfig,
      boxMullerLUTsize: Int
  ): EuropeanVanillaCore[T] = {
    new EuropeanVanillaCore(elemType, urngConfig, BoxMuller.defaultConfig(elemType, boxMullerLUTsize), config, false)
  }
}
