/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.monteCarlo.optionpricing

import bench.kernels.monteCarlo.{MonteCarloCore, MonteCarloFactory}

import bench.utils.hardware.RingElem
import bench.utils.components.LUTFunctionConfig
import bench.utils.components.random.{BoxMuller, TauswortheConfig}

import chisel3._
import dsptools.numbers.Ring

/** Factory for European Vanilla cores
 *  @param elemType
 *    type of the data (must be a fixed point because of the LUT functions)
 *  @param urngConfig
 *    iterator of TauswortheConfig (needed to setup random generator)
 *  @param boxMullerConfig
 *    configuration of BoxMuller (gives the LUT config)
 *  @param config
 *    Black Scholes parameters
 *  @param sharedROM
 *    Tells to the factory if all BoxMuller module take the same ROM
 */
class EuropeanVanillaFactory[T <: Data: Ring](
    elemType: RingElem[T],
    urngConfig: Iterator[(Array[TauswortheConfig], Array[TauswortheConfig])],
    boxMullerConfig: (LUTFunctionConfig[T], LUTFunctionConfig[T]),
    config: EuropeanVanillaConfig,
    sharedROM: Boolean
) extends MonteCarloFactory[T] {
  override def generateCores(nbCore: Int): Seq[MonteCarloCore[T]] = {
    val cores = (0 until nbCore).map(_ =>
      Module(
          new EuropeanVanillaCore(
              elemType,
              urngConfig.next(),
              boxMullerConfig,
              config,
              sharedROM
          )
      )
    )
    // Instantiate a shared ROM if necessary
    if (sharedROM) {
      val sin2pi = boxMullerConfig._1.getROM()
      val sqrtln = boxMullerConfig._2.getROM()
      cores.foreach(core => {
        core.rom.sin2pi := sin2pi.values
        core.rom.sqrtln := sqrtln.values
      })
    } else {
      cores.foreach(core => {
        core.rom.sin2pi := DontCare
        core.rom.sqrtln := DontCare
      })
    }
    // Up cast
    cores.map(core => core.asInstanceOf[MonteCarloCore[T]])
  }

  override def postProcessing(input: RingElem[T]): RingElem[T] = {
    config.HWMonteCarloPostProcessing(input)
  }

  override def getPostProcessingPipelineLevel(elemType: RingElem[T]): Int = {
    config.HWMonteCarloPostProcessingPipelineLevel(elemType)
  }
}

object EuropeanVanillaFactory {
  def apply[T <: Data: Ring](
      elemType: RingElem[T],
      urngConfig: Iterator[(Array[TauswortheConfig], Array[TauswortheConfig])],
      config: EuropeanVanillaConfig,
      boxMullerLUTsize: Int,
      sharedROM: Boolean
  ): EuropeanVanillaFactory[T] = {
    new EuropeanVanillaFactory(
        elemType,
        urngConfig,
        BoxMuller.defaultConfig(elemType, boxMullerLUTsize),
        config,
        sharedROM
    )
  }
}
