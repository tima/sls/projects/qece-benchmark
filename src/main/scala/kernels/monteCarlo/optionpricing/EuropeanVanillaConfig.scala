/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.monteCarlo.optionpricing

import bench.utils.hardware.{Delay, RingElem}
import bench.utils.software.Numeric

import chisel3._
import dsptools.numbers.Ring

/** Abstract configuration of European Vanilla option pricing parameters
 *
 *  @param S0
 *    Option price
 *  @param volatility
 *    Volatility of the underlying
 *  @param riskFreeRate
 *    Risk-free rate
 *  @param maturity
 *    Duration until expiry
 *  @param N
 *    Number of iterations for the Euler Maruyama method
 */
abstract class EuropeanVanillaConfig(
    val S0: Double,
    val volatility: Double,
    val riskFreeRate: Double,
    val maturity: Double,
    val N: Int
) {
  val dt = maturity / N
  require(dt < 1, "Not enough iterations : maturity/N must be < 1")

  def getQW(): (Double, Double) = {
    (1 + riskFreeRate * dt, volatility * math.sqrt(dt))
  }

  // Core Post Processing
  def HWCorePostProcessing[T <: Data: Ring](input: RingElem[T]): RingElem[T]
  def HWCorePostProcessingPipelineLevel[T <: Data: Ring](elemType: RingElem[T]): Int
  def SWCorePostProcessing(input: Double): Double

  // Monte Carlo Post Processing
  def HWMonteCarloPostProcessing[T <: Data: Ring](input: RingElem[T]): RingElem[T]
  def HWMonteCarloPostProcessingPipelineLevel[T <: Data: Ring](elemType: RingElem[T]): Int
  def SWMonteCarloPostProcessing(input: Double): Double
}

/** Pricing a European vanilla pay-off with a Monte Carlo method
 *
 *  @param S0
 *    Option price
 *  @param volatility
 *    Volatility of the underlying
 *  @param riskFreeRate
 *    Risk-free rate
 *  @param maturity
 *    Duration until expiry
 *  @param N
 *    Number of iterations for the Euler Maruyama method
 */
class EuropeanVanillaPayOff(S0: Double, volatility: Double, riskFreeRate: Double, maturity: Double, N: Int)
    extends EuropeanVanillaConfig(S0, volatility, riskFreeRate, maturity, N) {

  override def HWCorePostProcessing[T <: Data: Ring](input: RingElem[T]): RingElem[T]         = input
  override def HWCorePostProcessingPipelineLevel[T <: Data: Ring](elemType: RingElem[T]): Int = 0
  override def SWCorePostProcessing(input: Double): Double                                    = input

  override def HWMonteCarloPostProcessing[T <: Data: Ring](input: RingElem[T]): RingElem[T]         = input
  override def HWMonteCarloPostProcessingPipelineLevel[T <: Data: Ring](elemType: RingElem[T]): Int = 0
  override def SWMonteCarloPostProcessing(input: Double): Double                                    = input
}

/** Pricing a European vanilla put option with a Monte Carlo method
 *
 *  @param S0
 *    Option price
 *  @param strikePrice
 *    Strike Price
 *  @param volatility
 *    Volatility of the underlying
 *  @param riskFreeRate
 *    Risk-free rate
 *  @param maturity
 *    Duration until expiry
 *  @param N
 *    Number of iterations for the Euler Maruyama method
 */
class EuropeanVanillaPutPrice(
    S0: Double,
    strikePrice: Double,
    volatility: Double,
    riskFreeRate: Double,
    maturity: Double,
    N: Int
) extends EuropeanVanillaConfig(S0, volatility, riskFreeRate, maturity, N) {
  val expRT = Math.exp(-riskFreeRate * maturity)

  override def HWCorePostProcessing[T <: Data: Ring](input: RingElem[T]): RingElem[T] = {
    val putPrice = Numeric(strikePrice).toRingElem(input) - input
    Mux(putPrice.isPositive, putPrice, input.zero.asTypeOf(input))
  }
  override def HWCorePostProcessingPipelineLevel[T <: Data: Ring](elemType: RingElem[T]): Int =
    Delay.getAddDelay(elemType)
  override def SWCorePostProcessing(input: Double): Double = {
    val putPrice = strikePrice - input
    if (putPrice < 0) 0 else putPrice
  }

  override def HWMonteCarloPostProcessing[T <: Data: Ring](input: RingElem[T]): RingElem[T] = {
    input * Numeric(expRT).toRingElem(input)
  }
  override def HWMonteCarloPostProcessingPipelineLevel[T <: Data: Ring](elemType: RingElem[T]): Int =
    Delay.getMultDelay(elemType)
  override def SWMonteCarloPostProcessing(input: Double): Double = {
    input * expRT
  }
}

/** Pricing a European vanilla call option with a Monte Carlo method
 *
 *  @param S0
 *    Option price
 *  @param strikePrice
 *    Strike Price
 *  @param volatility
 *    Volatility of the underlying
 *  @param riskFreeRate
 *    Risk-free rate
 *  @param maturity
 *    Duration until expiry
 *  @param N
 *    Number of iterations for the Euler Maruyama method
 */
class EuropeanVanillaCallPrice(
    S0: Double,
    strikePrice: Double,
    volatility: Double,
    riskFreeRate: Double,
    maturity: Double,
    N: Int
) extends EuropeanVanillaConfig(S0, volatility, riskFreeRate, maturity, N) {
  val expRT = Math.exp(-riskFreeRate * maturity)

  override def HWCorePostProcessing[T <: Data: Ring](input: RingElem[T]): RingElem[T] = {
    val callPrice = input - Numeric(strikePrice).toRingElem(input)
    Mux(callPrice.isPositive, callPrice, input.zero.asTypeOf(input))
  }
  override def HWCorePostProcessingPipelineLevel[T <: Data: Ring](elemType: RingElem[T]): Int =
    Delay.getAddDelay(elemType)
  override def SWCorePostProcessing(input: Double): Double = {
    val callPrice = input - strikePrice
    if (callPrice < 0) 0 else callPrice
  }

  override def HWMonteCarloPostProcessing[T <: Data: Ring](input: RingElem[T]): RingElem[T] = {
    input * Numeric(expRT).toRingElem(input)
  }
  override def HWMonteCarloPostProcessingPipelineLevel[T <: Data: Ring](elemType: RingElem[T]): Int =
    Delay.getMultDelay(elemType)
  override def SWMonteCarloPostProcessing(input: Double): Double = {
    input * expRT
  }
}
