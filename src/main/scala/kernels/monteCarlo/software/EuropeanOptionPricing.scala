/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.monteCarlo.software

import bench.kernels.monteCarlo.optionpricing.{EuropeanVanillaConfig, EuropeanVanillaPayOff}

import qece.estimation.qor.{StatisticHelper, StandardNormalDistribution}

import scala.collection.mutable.ArrayBuffer

class EuropeanOptionPricing(
  config: EuropeanVanillaConfig,
  nbIterations: Int,
  confidence: Double = 0.99,
  workload: Seq[Double] = Seq.empty) {

  private var index = 0
  private val distribution = StandardNormalDistribution
  private def getSample: Double = 
    workload.isEmpty match {
      case true   => distribution.sample
      case false  => 
        index = index + 1
        workload(index - 1)
    }

  /** Compute both mean and confidence interval, assuming results follow a normal distribution
   *
   *  @param results
   *    experimental results
   *  @param confidence
   *    confidence wanted for the interval
   *  @return
   *    mean and confidence interval at specified confidence threshold
   */
  private def getMeanAndInterval(results: Array[Double]): (Double, (Double, Double)) = {
    val (mean, std) = StatisticHelper.getMeanAndStd(results)
    val alpha       = StandardNormalDistribution.getZAlpha(confidence)
    val N           = results.size
    (mean, (mean - alpha * std / math.sqrt(N), mean + alpha * std / math.sqrt(N)))
  }

  def monteCarlo(): (Double, (Double, Double)) = {
    val q = (config.riskFreeRate - math.pow(config.volatility, 2) / 2.0) * config.maturity
    val w = config.volatility * math.sqrt(config.maturity)

    val results = ArrayBuffer[Double]()
    for (_ <- 0 until nbIterations) {
      results.append(config.SWCorePostProcessing(config.S0 * math.exp(q + w * getSample)))
    }

    val (mean, bounds) = getMeanAndInterval(results.toArray)
    (config.SWMonteCarloPostProcessing(mean), bounds)
  }

  def monteCarloEulerMaruyama(): (Double, (Double, Double)) = {
    val q       = config.getQW()._1
    val w       = config.getQW()._2
    val results = ArrayBuffer[Double]()
    for (_ <- 0 until nbIterations) {
      results.append(config.SWCorePostProcessing(monteCarloEulerMaruyamaIterator(q, w)))
    }

    val (mean, bounds) = getMeanAndInterval(results.toArray)
    (config.SWMonteCarloPostProcessing(mean), bounds)
  }

  private def monteCarloEulerMaruyamaIterator(q: Double, w: Double): Double = {
    var S: Double = config.S0
    for (_ <- 0 until config.N) {
      S = S * (q + w * getSample)
    }
    S
  }
}

object EuropeanOptionPricing {
  def resultToString(result: (Double, (Double, Double))): String = {
    result._1 + " (Ic: [" + result._2._1 + "; " + result._2._2 + "])"
  }
}

object EuropeanOptionPricingTest extends App with logger.LazyLogging {
  val nbIterations = 10000 // Nb iterations of Monte Carlo
  val config = new EuropeanVanillaPayOff(
      S0 = 100.0,          // Option price
      volatility = 0.2,    // Volatility of the underlying (20%)
      riskFreeRate = 0.05, // Risk-free rate (5%)
      maturity = 1,        // One year until expiry
      N = 10               // Nb iterations of Euler-Maruyama
  )
  val confidence = 0.99

  val pricing = new EuropeanOptionPricing(config, nbIterations, confidence)
  logger.info("Stock price " + pricing.monteCarlo())
  logger.info("Stock price (Euler-Maruyama) " + pricing.monteCarloEulerMaruyama())
}
