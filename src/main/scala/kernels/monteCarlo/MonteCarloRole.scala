/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.monteCarlo

import bench.utils.components.Role
import bench.utils.hardware.{Delay, RingElem}
import bench.utils.generation.Require

import chisel3._
import chisel3.util.{ShiftRegister, log2Ceil}
import dsptools.numbers.Ring

/** Monte Carlo top level
 *
 *  @param elemType
 *    type of data
 *  @param factory
 *    core factory
 *  @param nbIterations
 *    Number of iteration before taking the average
 *  @param nbCore
 *    Number of core.
 */
class MonteCarloRole[T <: Data: Ring](
    elemType: RingElem[T],
    factory: MonteCarloFactory[T],
    nbIterations: Int,
    nbCore: Int
) extends Role(elemType.getWidth) {
  io.out.valid := true.B
  io.out.bits := io.in.bits
  io.in.ready := true.B

  require(Require.isPow2(nbIterations), "The number of iteration must be a power of 2.")
  require(Require.divide(nbCore, nbIterations), "The number of cores must divide the number of iteration")

  // Instantiate all cores
  val cores = factory.generateCores(nbCore)

  // Reduce the result of all cores with an adder tree
  val sum      = VecInit(cores.map(core => core.io.output.bits)).reduceTree(_ + _)
  val sumValid = ShiftRegister(cores(0).io.output.valid, log2Ceil(nbCore) * Delay.getAddDelay(elemType))
  // Accumulate the results until the number of iteration is done.
  val elemTypeExtended = elemType.cloneType(elemType.getWidth + log2Ceil(nbIterations))
  val accu             = RegInit(elemTypeExtended, elemTypeExtended.zero)
  val counter          = RegInit(UInt(log2Ceil(nbIterations).W), 0.U)

  // Counter logic for accumulation
  when(sumValid) {
    when(counter === 0.U) {
      accu := sum
    } otherwise {
      Delay.preventDelay {
        accu := accu + sum
      }
    }

    counter := counter + nbCore.U
  }

  // Average
  val averageValidity = (counter === 0.U) && RegNext(sumValid)
  val average         = accu >> log2Ceil(nbIterations)

  // Post Processing
  val output         = factory.postProcessing(average)
  val outputValidity = ShiftRegister(averageValidity, factory.getPostProcessingPipelineLevel(elemType), false.B, true.B)

  // Return the average result
  io.out.bits := output.asUInt()
  io.out.valid := outputValidity
}
