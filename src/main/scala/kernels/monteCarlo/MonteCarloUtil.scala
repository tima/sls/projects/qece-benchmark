/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.monteCarlo

import bench.kernels.monteCarlo.optionpricing.{
  EuropeanVanillaConfig,
  EuropeanVanillaPayOff,
  EuropeanVanillaPutPrice,
  EuropeanVanillaCallPrice
}

object MonteCarloUtil {

  def getEuropeanVanillaConfig(
      kind: String,
      s0: Double,
      volatility: Double,
      riskFreeRate: Double,
      maturity: Double,
      n: Int,
      strikePrice: Double = 0
  ): EuropeanVanillaConfig = {
    kind match {
      case "payoff" => new EuropeanVanillaPayOff(s0, volatility, riskFreeRate, maturity, n)
      case "put"    => new EuropeanVanillaPutPrice(s0, strikePrice, volatility, riskFreeRate, maturity, n)
      case "call"   => new EuropeanVanillaCallPrice(s0, strikePrice, volatility, riskFreeRate, maturity, n)
      case _        => throw new UnsupportedOperationException(s"Don't know kind $kind for EuropeanVanillaConfig")
    }
  }
}
