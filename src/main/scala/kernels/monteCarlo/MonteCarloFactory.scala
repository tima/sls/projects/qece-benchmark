/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.monteCarlo

import bench.utils.hardware.RingElem

import chisel3.Data
import dsptools.numbers.Ring

/** Abstract factory which provide the tools to build the Monte Carlo method */
abstract class MonteCarloFactory[T <: Data: Ring]() {

  /** Generate the Monte Carlo cores
   *  @param nbCore
   *    Number of cores needed
   *  @return
   *    Sequence of cores initialized
   */
  def generateCores(nbCore: Int): Seq[MonteCarloCore[T]]

  /** Define the post processing for the output of Monte Carlo computation
   *  @param input
   *    @return post processed input
   */
  def postProcessing(input: RingElem[T]): RingElem[T]

  /** Get level of the post processing pipeline
   *  @param elemType
   *    type of data
   *  @return
   *    the pipeline level
   */
  def getPostProcessingPipelineLevel(elemType: RingElem[T]): Int
}
