/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.monteCarlo

import bench.utils.hardware.RingElem

import chisel3._
import chisel3.util.Valid
import dsptools.numbers.Ring

/** Abstract class of all Monte Carlo Cores. WARNING : this class MUST NOT be instantiated for itself
 *  @param elemType
 *    type of the data
 */
abstract class MonteCarloCore[T <: Data: Ring](elemType: RingElem[T]) extends MultiIOModule {
  val io = IO(new Bundle() {
    val output = Output(Valid(elemType))
  })

  def getPipelineLevel(): Int = {
    throw new UnsupportedOperationException("This method must be overrided")
  }

}
