/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.matrixRef

import bench.utils.software.{Numeric, BitVector, Comparator}

import logger.LazyLogging

import chisel3._

final case class MatrixException(private val message: String = "", private val cause: Throwable = None.orNull)
    extends Exception(message, cause)

abstract class SquareMatrix[T <: Any](content: Array[Array[T]]) {

  /** Printing method for matrix
   *
   *  @param title
   *    title to be printed [optional]
   */
  def display(title: Option[String] = None): Unit = {
    println(prettyToString(title.getOrElse("")))
  }

  def prettyToString(title: String = ""): String = {
    title match {
      case "" => this.toString
      case _  => Seq(title.map(_ + " :"), this.toString).mkString("\n")
    }
  }

  /** printing utility */
  override def toString: String = {
    var str = ""
    content.foreach { a => a.foreach { c => str += s"$c " }; str += s"\n" }
    str
  }

  /** Simplify function call to display */
  def display(title: String): Unit = display(Some(title))

  /** Check weither matrixes sizes are compatible or not
   *
   *  @param a
   *    first matrix
   *  @param b
   *    second matrix
   *  @return
   *    weither a and b have same dimensions or not
   */
  def isCompatible[U <: Any](other: SquareMatrix[U]): Boolean = {
    (for {
      myRow    <- content
      otherRow <- other.getContent
    } yield myRow.length == otherRow.length).reduce(_ & _)
  }

  /** Check if the matrix is square or not
   *
   *  @return
   *    weither the matrix is square or not
   */
  def isSquare: Boolean = {
    content.length == content.count(_ => true)
  }

  /** Content accessor.
   *
   *  @return
   *    the content of the matrix, as an Array[Array[Any]]
   */
  def getContent = content

  /** Size accessor.
   *
   *  @return
   *    the size of the matrix
   */
  def getSize = {
    this.isSquare match {
      case true => content.length
      case _    => throw new MatrixException("Matrix is not a square one")
    }
  }
}

class Matrix(content: Array[Array[Numeric]], val signal: Data) extends SquareMatrix[Numeric](content) with LazyLogging {
  private var overflow = false

  /** Detect an overflow in a matrix. */
  def detectOverflow: Matrix = {
    content.map(a => a.map(b => b.detectOverflow(signal)).reduce(_ | _)).reduce(_ | _) match {
      case false => this
      case _     => this.overflow = true; this
    }
  }

  /** Basic multiplication operation between two software matrices
   *
   *  @param other
   *    the other matrix to multiply with.
   */
  def *(other: Matrix): Matrix = {
    Matrix(
        for (row <- content)
          yield for (col <- other.getContent.transpose)
            yield row zip col map Function.tupled(_ * _) reduceLeft (_ + _),
        signal
    ).detectOverflow
  }

  /** Basic addition operation between two software matrices
   *
   *  @param other
   *    the other matrix to multiply with.
   */
  def +(other: Matrix): Matrix = {
    Matrix(
        (content zip other.getContent).map { a =>
          (a._1 zip a._2).map { b =>
            b._1 + b._2
          }
        },
        signal
    ).detectOverflow
  }

  /** Scalar product
   *
   *  @param num
   *    scalar value to multiply with
   */
  def *(num: Numeric): Matrix = {
    Matrix(content.map(_.map(_ * num)), signal).detectOverflow
  }

  /** Equality check between a Soft and a Hard matrix.
   *
   *  @param other
   *    the hardware Matrix to be checked against
   *  @return
   *    weither the matrices are equals or not.
   */
  def ==(other: BitMatrix)(implicit c: Comparator): Boolean = {
    overflow match {
      case true =>
        logger.warn(s"Representation overflow occured in matrix comparison. Continuing");
        logger.debug(s"$this")
        true
      case false =>
        (content zip other.getContent)
          .map { a =>
            (a._1 zip a._2).map { b => b._1 == b._2 }.reduce(_ & _)
          }
          .reduce(_ & _)
    }
  }
}

object Matrix {
  def apply(size: Int, signal: Data, max: Int = 10): Matrix = {
    val myArray = Array.ofDim[Numeric](size, size)
    for {
      i <- 0 until size
      j <- 0 until size
    } yield myArray(i)(j) = Numeric.random(signal, max)
    new Matrix(myArray, signal)
  }

  def apply(content: Array[Array[Numeric]], signal: Data): Matrix = {
    new Matrix(content, signal)
  }
}

/** A hardware matrix, represented as a 2D array of BigInt
 *
 *  Each BigInt must be seen as a bit vector
 *  @constructor
 *    create a new bit matrix from a content
 *  @param content
 *    the 2D array of bit vectors
 */
class BitMatrix(content: Array[Array[BitVector]], readOnly: Boolean = true) extends SquareMatrix[BitVector](content) {
  private var rowIndex = 0
  private var colIndex = 0
  private var maxIndex = false

  def pushTokens(tokens: Array[BitVector]): Unit = {
    tokens.map { token => pushToken(token) }
  }

  def pushToken(token: BitVector): Unit = {
    if (readOnly) throw MatrixException("Can not write in a read only BitMatrix")
    if (!isFull) {
      content(rowIndex)(colIndex) = token
      incrementIndex()
    }
  }

  def getTokens(nbToken: Int): Array[BitVector] = (0 until nbToken).toArray.map(_ => getToken)

  def getToken: BitVector = {
    if (!readOnly) throw MatrixException("Can not read from a write only BitMatrix")
    if (!isEmpty) {
      val token = content(rowIndex)(colIndex)
      incrementIndex()
      token
    } else throw MatrixException("Can not read from empty BitMatrix")
  }

  private def incrementIndex() = {
    colIndex = (colIndex + 1) % getSize
    rowIndex = rowIndex + (if (colIndex == 0) 1 else 0) // colIndex is 0 when a new line is started
    maxIndex = (rowIndex >= getSize)
  }

  def isEmpty = readOnly && maxIndex

  def isFull = !readOnly && maxIndex

  def transpose: BitMatrix = {
    new BitMatrix(content.transpose, readOnly)
  }
}

/** Factory for [[BitMatrix]] instances. */
object BitMatrix {

  /** Creates a new hardware matrix from a software one
   *
   *  @param signal
   *    generic type used in the mut
   *  @param softMatrix
   *    software representation for the given matrix
   */
  def apply(signal: Data, softMatrix: Matrix) =
    new BitMatrix(softMatrix.getContent map { row => row.map { cell => BitVector(cell.toBits(signal))(signal) } })

  /** Creates a new hardware matrix from an Array of Array of BigInt
   *
   *  @param content
   *    content for the given matrix
   */
  def apply(content: Array[Array[BitVector]]) = new BitMatrix(content)

  def apply(size: Int) = new BitMatrix(Array.ofDim[BitVector](size, size), false)

  /** Transform a 1D-array to a 2D-square matrix
   *
   *  @param array
   *    original 1D-array
   *  @param size
   *    dimension of 2D-matrix target
   *  @param baseIndex
   *    if defined, offset of values in array
   *  @return
   *    a 2D-square matrix representing the values in array
   */
  def arrayToMatrix(array: Array[BitVector], size: Int, baseIndex: Int = 0): BitMatrix = {
    assert(array.length >= size * size + baseIndex)
    val result = Array.ofDim[BitVector](size, size)
    var index  = baseIndex
    var i      = 0
    var j      = 0
    while (index <= size * size + baseIndex - 1) {
      result(i)(j) = array(index)
      index += 1
      j = if (j + 1 < size) { j + 1 }
      else { 0 }
      i = if (j == 0) { i + 1 }
      else { i }
    }

    BitMatrix(result)
  }
}
