/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.gemm

/** Gemm utils for the different modes. */
object GemmMode {

  val GETC_INDEX     = 2
  val STOREC_INDEX   = 1
  val GETCOEFF_INDEX = 0

  /** Return a BigInt corresponding to the right gemm mode.
   *
   *  @param getC
   *    if true, you need to send the C matrix. when false, it is already stored in the design.
   *  @param storeC
   *    if true, the result will be stored in memory. When false, it is sent back instead.
   *  @param getCoeff
   *    if true, the coefficients alpha and beta will be retrieved from input. When false, they are already stored in
   *    memory.
   *  @return
   *    a Bigint representing the bitfield of options
   */
  def getMode(getC: Boolean = true, storeC: Boolean = false, getCoeff: Boolean = true): BigInt = {
    var result = BigInt(0)
    if (getC)
      result = result | (1 << GETC_INDEX)
    if (storeC)
      result = result | (1 << STOREC_INDEX)
    if (getCoeff)
      result = result | (1 << GETCOEFF_INDEX)
    result
  }
}
