/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.gemm

import bench.utils.components.{Role, DeserMac}
import bench.utils.components.memory.BankedMemory
import bench.utils.hardware.{RingElem, Delay}
import bench.utils.generation.Require

import chisel3._
import chisel3.util._
import dsptools.numbers._

/** GEMM implementation with differents modes. The full-mode protocol (default) is the following :
 *    - send alpha
 *    - send beta
 *    - send C matrix (by rows)
 *    - send B matrix (by columns)
 *    - stream A matrix (by rows) while retrieving D matrix on-the-fly
 */
class GemmRole[T <: Data: Ring](bitWidth: Int)(genArray: RingElem[T], size: Int) extends Role(bitWidth) {
  require(Require.divide(genArray.getWidth, bitWidth), s"Element width must divide input bitwidth")
  require(
      Require.divide(bitWidth / genArray.getWidth, size),
      s"Matrixes size must be a multiple of the number of element sent by cycle"
  )

  /* Constants for generic computation purposes */
  val nbAccessCycle = 2 /* BRAM with read buffer registers */
  val elemWidth     = genArray.getWidth
  val nbElemCycle   = bitWidth / elemWidth
  require(Require.isPow2(nbElemCycle), "number of element by cycle must be a power of 2")
  val nbCycleLine = size / nbElemCycle
  require(Require.isPow2(nbCycleLine), "number of cycle by line must be a power of 2")
  val nbBlock = size * nbCycleLine

  val alpha = RegInit(genArray.cloneType, genArray.one)
  val beta  = RegInit(genArray.cloneType, genArray.one)

  /* Instantiate operators - uses Mac units defined in mult project */
  /* One MAC Unit per column, in order to be able to compute every result on the fly */
  val MACs          = Array.fill(size)(Module(new DeserMac(genArray.cloneType, bitWidth)))
  val pipelineLevel = MACs(0).getPipelineLevel

  /* Instantiating indexes for memory access */
  /* index in memory bank to load subcolumn */
  val index  = Reg(UInt((log2Up(nbBlock)).W))
  val indexC = ShiftRegister(index, pipelineLevel + nbCycleLine + 1)
  /* index used to know which sub block is being send while computing */
  val count = Reg(UInt((log2Up(size * size) + log2Up(nbAccessCycle)).W))
  /* index to control the mac units and the output */
  val macCount = Reg(UInt((log2Up(nbCycleLine)).W))

  /* Variables for memory addressing (writeB) */
  val bank      = Reg(UInt(log2Up(size).W))
  val addr      = Reg(UInt(log2Up(nbCycleLine).W))
  val incrAddr  = Wire(Bool())
  val resetAddr = Wire(Bool())

  val multLatency = Delay.getMultDelay(genArray)

  val bankB = ShiftRegister(bank, multLatency)
  val addrB = ShiftRegister(addr, multLatency)

  val addrC = ShiftRegister((bank << log2Ceil(nbCycleLine)) + addr, multLatency)
  // val addrC     = ShiftRegister(bank*nbCycleLine.U + addr, multLatency)

  /* index for serialization */
  val buf_index = Reg(UInt((log2Up(nbCycleLine)).W))

  /* Instantiating memory (for internal memory designs) */
  val matB = BankedMemory(size, nbCycleLine, bitWidth)
  val matC = BankedMemory(1, nbBlock, bitWidth)
  /* Buffer the matB reads to shorten the critical path */
  val ramBuffer = Reg(Vec(size, UInt(bitWidth.W)))
  /* Buffer the matC reads to shorten the critical path */
  val cBuffer = Reg(UInt(bitWidth.W))

  /* Buffer for outputs */
  val bufferMult = Reg(Vec(size, genArray.cloneType))
  /* Used for serialization of output from buffer to io.out.bits */
  val outBuffer = WireInit(VecInit(Seq.fill(nbElemCycle)(0.U(genArray.getWidth.W))))
  val valid     = RegInit(Bool(), false.B)

  /* ======= Control signal declaration ====== */
  /* -------------- Mode control ------------- */
  val getC     = RegInit(Bool(), true.B) /* Retrieve matrix C from io.in */
  val storeC   = RegInit(Bool(), false.B) /* Send result of the gemm algorithm, instead of storing it as next C */
  val getCoeff = RegInit(Bool(), true.B) /* Retrieve alpha and beta from io.in */
  /* ----------------------------------------- */

  /* Memory accesses */
  val writeB      = Wire(Bool())
  val writeC      = Wire(Bool())
  val writeBReg   = ShiftRegister(writeB, multLatency)
  val writeCReg   = ShiftRegister(writeC, multLatency)
  val readB       = Wire(Bool())
  val readC       = Wire(Bool())
  val useAlpha    = Wire(Bool())
  val useAlphaReg = ShiftRegister(useAlpha, multLatency)

  /* Output control */
  val serialize        = Wire(Bool())
  val validResultCycle = Wire(Bool())

  /* Mac control */
  val resetMac = Wire(Bool())
  /* Shift Register used to know when mac results are valid */
  val macResIn  = Wire(Bool())
  val macResult = ShiftRegister(macResIn, pipelineLevel)
  /* Define states where mac results might be valid */
  val validState = Wire(Bool())
  /* ========================================= */

  /* ========= Signal initialization ========= */
  /* Initialize signals (default values) */
  /* I/O */
  io.in.ready := true.B
  io.out.valid := valid
  io.out.bits := outBuffer.asUInt

  /* Control signals */
  writeB := false.B
  writeC := false.B
  useAlpha := false.B
  resetMac := true.B
  readB := false.B
  readC := false.B
  serialize := false.B
  incrAddr := false.B
  resetAddr := false.B
  macCount := Mux(macCount === (nbCycleLine - 1).U, 0.U, macCount + 1.U)
  /* ========================================= */

  /* ================ Datapath =============== */

  /* Control address and bank for memory writes */
  when(incrAddr) {
    bank := Mux(addr === (nbCycleLine - 1).U, bank + 1.U, bank)
    addr := Mux(addr === (nbCycleLine - 1).U, 0.U, addr + 1.U)
  }.elsewhen(resetAddr) {
    bank := 0.U
    addr := 0.U
  }

  val scalarMultRegister = Wire(UInt((genArray.getWidth * nbElemCycle).W))
  when(useAlphaReg) {
    scalarMultRegister := alpha * io.in.bits
  }.otherwise {
    scalarMultRegister := beta * io.in.bits
  }

  when(writeBReg) {
    matB.write(bankB, addrB, scalarMultRegister)
  }

  when(writeCReg) {
    matC.write(0.U, addrC, scalarMultRegister)
  }

  /* Serializing output */
  when(serialize) {
    for (i <- 0 until nbElemCycle) {
      Delay.preventDelay {
        // var resultBuffer = bufferMult(buf_index * nbElemCycle.U + i.U) + cBuffer((nbElemCycle - i)*elemWidth - 1,
        // (nbElemCycle - i - 1)*elemWidth).asTypeOf(genArray)
        val resultBuffer = bufferMult((buf_index << log2Ceil(nbElemCycle)) + i.U) + cBuffer(
            (nbElemCycle - i) * elemWidth - 1,
            (nbElemCycle - i - 1) * elemWidth
        ).asTypeOf(genArray)
        outBuffer(nbElemCycle - i - 1) := resultBuffer.asUInt
      }
    }
  }

  /* ------------- RAM BUFFERING -------------- */
  /* buffer ram output for timing optimization */
  when(readB) {
    /* WARNING : Can't read and write at same time on a bank (address bus is shared) */
    for (i <- 0 until size) {
      /* index indicates the block to read in a column */
      // ramBuffer(i) := matB.read(i.U, index%nbCycleLine.U)
      ramBuffer(i) := matB.read(i.U, index(log2Ceil(nbCycleLine), 0))
    }
  }

  when(readC) {
    cBuffer := matC.read(0.U, indexC)
  }
  /* ------------------------------------------ */

  /* Feed the Mac units */
  for (i <- 0 until size) {
    val mac = MACs(i)
    mac.io.a_line := io.in.bits
    mac.io.reset := resetMac
    mac.io.b_col := ramBuffer(i)
  }

  /* Buffer the mac results when results are valid */
  when(validResultCycle) {
    for (i <- 0 until size) {
      val mac = MACs(i)
      bufferMult(i) := mac.io.result
    }
    valid := true.B
  }

  /* ------------------------- FSM declaration ----------------------------- */
  val stIDLE :: stLoadingAlpha :: stLoadingBeta :: stLoadingMatC :: stLoadingMatB :: stStall :: stFetch :: stCompute :: stSend :: Nil =
    Enum(9)
  val wrapperState = RegInit(stIDLE)
  val stallLevel   = Reg(UInt(log2Ceil(multLatency).W))
  /* ----------------------------------------------------------------------- */

  /* Define the states where results might be valid */
  validState := (wrapperState === stCompute) || (wrapperState === stSend)

  /* Condition for the result to be valid, will be delayed by the level of pipeline */
  macResIn := validState && (macCount === 0.U)

  validResultCycle := validState && (macResult && count =/= (nbAccessCycle + pipelineLevel).U)

  /* ========================================= */

  /* ================== FSM ================== */

  switch(wrapperState) {
    is(stIDLE) {
      /* IDLE */
      index := 0.U
      count := 0.U
      macCount := 0.U
      resetAddr := true.B

      buf_index := 0.U
      valid := false.B
      when(io.in.valid) {
        getC := io.in.bits(GemmMode.GETC_INDEX)
        storeC := io.in.bits(GemmMode.STOREC_INDEX)
        getCoeff := io.in.bits(GemmMode.GETCOEFF_INDEX)
        when(io.in.bits(GemmMode.GETCOEFF_INDEX)) {
          wrapperState := stLoadingAlpha
        }.elsewhen(io.in.bits(GemmMode.GETC_INDEX)) {
          wrapperState := stLoadingMatC
        }.otherwise {
          wrapperState := stLoadingMatB
        }
      }
    }
    is(stLoadingAlpha) {
      when(io.in.valid) {
        alpha := io.in.bits.asTypeOf(genArray)
        wrapperState := stLoadingBeta
      }
    }
    is(stLoadingBeta) {
      when(io.in.valid) {
        beta := io.in.bits.asTypeOf(genArray)
        when(getC) {
          wrapperState := stLoadingMatC
        }.otherwise {
          wrapperState := stLoadingMatB
        }
      }
    }
    is(stLoadingMatC) {
      /* Loading first matrix (C) */
      when(io.in.valid) {
        writeC := true.B
        when(count >= (nbBlock - 1).U) {
          count := 0.U
          wrapperState := stLoadingMatB
          resetAddr := true.B
        }.otherwise {
          count := count + 1.U
          incrAddr := true.B
        }
      }
    }
    is(stLoadingMatB) {
      /* Loading second matrix (B) */
      when(io.in.valid) {
        writeB := true.B
        useAlpha := true.B
        when(count >= (nbBlock - 1).U) {
          index := 0.U
          count := 0.U
          io.in.ready := false.B
          wrapperState := stStall
          stallLevel := (multLatency - 1).U
          resetAddr := true.B
        }.otherwise {
          count := count + 1.U
          incrAddr := true.B
        }
      }
    }
    is(stStall) {
      /* Stall when accessing memory (because memory does not support dual port) */
      when(io.in.valid) {
        io.in.ready := false.B
        useAlpha := true.B
        when(stallLevel === 0.U) {
          wrapperState := stFetch
        }.otherwise {
          stallLevel := stallLevel - 1.U
        }
      }
    }
    is(stFetch) {
      /* Fetching memory (generic fetch with a 'nbAccessCycle' latency) */
      readB := true.B
      count := count + 1.U
      index := index + 1.U
      when(count < (nbAccessCycle - 1).U) {
        io.in.ready := false.B
      }.otherwise {
        macCount := 0.U
        wrapperState := stCompute
      }
    }
    is(stCompute) {
      /* On-the-fly result computing Part of the result is sent on the fly */
      readB := true.B
      readC := true.B
      when(io.in.valid) {
        resetMac := (macCount === 0.U)
        when(count > (nbBlock + nbAccessCycle - 1).U) {
          count := 0.U
          index := 0.U
          wrapperState := stSend
        }.otherwise {
          count := count + 1.U
          index := index + 1.U
        }
        when(index === (nbBlock - 1).U) {
          index := 0.U
        }
        when(valid) {
          buf_index := Mux(buf_index < nbCycleLine.U, buf_index + 1.U, 0.U)
          serialize := true.B
        }
      }
    }
    is(stSend) {
      readC := true.B
      /* The rest of the result is sent */
      val limit = nbCycleLine - 1 + pipelineLevel
      when(count === limit.U) {
        wrapperState := stIDLE
        count := 0.U
        buf_index := 0.U
        valid := false.B
      }
      buf_index := Mux(buf_index < nbCycleLine.U, buf_index + 1.U, 0.U)
      serialize := true.B
      count := count + 1.U
    }
  }
  /* ========================================= */
}
