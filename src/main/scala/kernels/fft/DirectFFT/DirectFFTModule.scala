/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.fft.DirectFFT

import bench.utils.generation.Require
import bench.utils.hardware.RingElem

import chisel3._
import chisel3.util.log2Ceil
import dsptools.numbers.Ring

/** Direct FFT computation (non pipeline way to compute FFT). It's used as preprocessor to get independent lanes or to
 *  have a full parallelized FFT
 *  @param elemType
 *    type of data
 *  @param size
 *    size of the FFT
 *  @param lanes
 *    number of lanes
 */
class DirectFFTModule[T <: Data: Ring](elemType: RingElem[T], size: Int, lanes: Int) extends Module {
  require(Require.isPow2(size), s"size must a power of 2")
  require(Require.isPow2(lanes), s"lanes must a power of 2")
  require(lanes >= 2, s"lanes must be >= 2")

  val io = IO(new Bundle() {
    val in      = Input(Vec(lanes, elemType))
    val inValid = Input(Bool())

    val out      = Output(Vec(lanes, elemType))
    val outValid = Output(Bool())
  })

  def getPipelineLevel(): Int = {
    pipelineLevel
  }

  val stages = new Array[Stage[T]](log2Ceil(lanes))
  stages(0) = Module(new Stage[T](elemType, 0, size, lanes))
  stages(0).io.in := io.in
  stages(0).io.inValid := io.inValid

  var pipelineLevel = stages(0).getPipelineLevel()

  for (stageLevel <- 1 until stages.size) {
    stages(stageLevel) = Module(new Stage[T](elemType, stageLevel, size, lanes))
    stages(stageLevel).io.in := stages(stageLevel - 1).io.out
    stages(stageLevel).io.inValid := stages(stageLevel - 1).io.outValid
    pipelineLevel += stages(stageLevel).getPipelineLevel()
  }

  io.out := stages(stages.size - 1).io.out
  io.outValid := stages(stages.size - 1).io.outValid
}
