/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.fft.DirectFFT

import bench.utils.generation.Require
import bench.utils.hardware.RingElem
import bench.kernels.fft.FFTUtils.{DIFButterfly, TwiddleFactorROM}

import logger.LazyLogging

import chisel3._
import chisel3.util.log2Ceil
import dsptools.numbers.Ring

/** A Direct FFT Stage composed of a butterfly.
 *  @param elemType
 *    type of data
 *  @param stageLevel
 *    stage level
 *  @param size
 *    fft size
 *  @param lanes
 *    number of lanes
 */
class Stage[T <: Data: Ring](elemType: RingElem[T], stageLevel: Int, size: Int, lanes: Int)
    extends Module
    with LazyLogging {
  require(Require.isPow2(size), s"size must a power of 2")

  val io = IO(new Bundle() {
    val in       = Input(Vec(lanes, elemType))
    val inValid  = Input(Bool())
    val out      = Output(Vec(lanes, elemType))
    val outValid = Output(Bool())
  })

  def getPipelineLevel(): Int = {
    pipelinedLevel
  }

  var pipelinedLevel = 0
  val skip           = size / lanes
  val stageSize      = math.pow(2, log2Ceil(size) - stageLevel).toInt

  val config          = new DirectFFTIndexes(size, lanes)
  val previousIndexes = if (stageLevel == 0) config.inputIndexes() else config.indexes(stageLevel - 1)
  val currentIndexes  = config.indexes(stageLevel)

  for (laneNumber <- 0 until lanes by 2) {
    // Get all the twiddle indexes necessary for this stage and this lane number
    val twiddleIndexes =
      TwiddleFactorROM.DirectFftTwiddleIndexes(stageLevel, size, (laneNumber % (stageSize / skip)) / 2 * skip, skip)
    // logger.debug(s"Stage $stageLevel Lane " + (laneNumber / 2) + s" Twiddle indexes are : $indexes")

    val butterfly = Module(new DIFButterfly(elemType, twiddleIndexes, stageLevel, size))

    // Get the input indexes in order to connect the butterfly module to the right input.
    val in0Index = previousIndexes.indexOf(currentIndexes(laneNumber))
    val in1Index = previousIndexes.indexOf(currentIndexes(laneNumber + 1))
    logger.debug(s"stage $stageLevel lane $laneNumber : $in0Index and $in1Index")

    butterfly.io.in0 := io.in(in0Index)
    butterfly.io.in1 := io.in(in1Index)
    butterfly.io.inEnable := true.B
    butterfly.io.inValid := io.inValid

    io.out(laneNumber) := butterfly.io.out0
    io.out(laneNumber + 1) := butterfly.io.out1
    io.outValid := butterfly.io.outValid

    pipelinedLevel = butterfly.getPipelineLevel()
  }

}

/* Debug function Uncomment to use */
/* object test extends App { chisel3.Driver.execute( Array[String]("--target-dir", "generated"), () => new
 * Stage(RingElem(DspComplex(FixedPoint(32.W, 16.BP))), 2, 64, 8) ) } */
