/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.fft.DirectFFT

import chisel3.util.log2Ceil

/** Helper to get input/output samples indexes for the Direct FFT computation
 *  @param fftSize
 *    size of the FFT
 *  @param lanes
 *    number of lanes
 */
class DirectFFTIndexes(fftSize: Int, lanes: Int) {

  def indexes(stageLevel: Int): Array[Int] = {
    val stageSize = math.pow(2, log2Ceil(fftSize) - stageLevel).toInt
    val laneSize  = fftSize / lanes
    val result    = new Array[Int](lanes)
    val groups    = math.pow(2, stageLevel).toInt
    for (group <- 0 until groups) {
      for (lane <- 0 until lanes / groups by 2) {
        val index = group * (lanes / groups) + lane
        result(index) = stageSize * group + laneSize * (lane / 2)
        result(index + 1) = stageSize * group + stageSize / 2 + laneSize * (lane / 2)
      }
    }
    result
  }

  def inputIndexes(): Array[Int] = {
    indexes(0)
  }

  def outputIndexes(): Array[Int] = {
    indexes(log2Ceil(lanes) - 1)
  }
}

/** Debug function */
/* Uncomment to use object DirectFFTIndexesTest extends App { val size = 32 val lanes = 32 val m = new
 * DirectFFTIndexes(size, lanes) println(m.computeOutputIndexes(m.inputIndexes(), 2).toIndexedSeq)
 * println(m.indexes(0).toIndexedSeq) println(m.indexes(3).toIndexedSeq) println(m.indexes(4).toIndexedSeq) } */
