/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.fft.FFTUtils

import chisel3._
import chisel3.util.log2Ceil

/** Counter that raise a top when the period is reached
 *  @param period
 *    delay between tops
 */
class CounterWithTop(period: Int) extends Module {
  val io = IO(new Bundle {
    val enable = Input(Bool())
    val count  = Output(UInt(log2Ceil(period).W))
    val top    = Output(Bool())
  })

  val x = RegInit(UInt(log2Ceil(period).W), 0.U)

  io.count := x
  io.top := x === (period - 1).U

  when(io.enable) { x := Mux(io.top, 0.U, x + 1.U) }
}
