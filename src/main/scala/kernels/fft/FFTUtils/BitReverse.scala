/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.fft.FFTUtils

/** Software way to compute the bit reverse */
object BitReverse {
  // Bit reverse a value (only for software)
  def apply(in: Int, width: Int): Int = {
    var test = in & (math.pow(2, width).toInt - 1)
    var out  = 0
    for (i <- 0 until width) {
      if (test / math.pow(2, width - i - 1) >= 1) {
        out += math.pow(2, i).toInt
        test -= math.pow(2, width - i - 1).toInt
      }
    }
    out
  }
}

/** Debug function Uncomment to use */
/* object FFTIndexOutputOrder extends App { val fftSize = 32 val lanes = 16 for(i <- 0 until (fftSize / lanes) * 2 by 2)
 * { println("(" + i + ") index = " + BitReverse(i, log2Ceil(fftSize))) } } */
