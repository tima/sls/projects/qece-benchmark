/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.fft.FFTUtils

import bench.utils.hardware.RingElem
import bench.utils.software.{Complex, Numeric}

import chisel3._
import chisel3.util.log2Ceil
import dsptools.numbers.Ring

/** ROM that contains the twiddle value
 *  @param elemType
 *    type of data
 *  @param size
 *    fft size
 *  @param indexes
 *    sequence of indexes used to compute the corresponding twiddle values
 */
class TwiddleFactorROM[T <: Data: Ring](elemType: RingElem[T], size: Int, indexes: Seq[Int]) extends Module {

  val ROMSize = indexes.size

  val io = IO(new Bundle() {
    val index   = Input(UInt(log2Ceil(ROMSize + 1).W))
    val twiddle = Output(elemType)
  })

  def getTwiddle(k: Int): RingElem[T] = {
    val phase     = -2 * Math.PI * k / size
    val twiddleSw = Numeric(Complex(math.cos(phase), math.sin(phase)))
    twiddleSw.toRingElem(elemType)
  }

  val twiddleRom = VecInit(indexes.map(index => getTwiddle(index)))
  io.twiddle := twiddleRom(io.index)
}

object TwiddleFactorROM {
  def twiddleIndexes(stageLevel: Int, size: Int): Seq[Int] = {
    val pow2level = math.pow(2, stageLevel + 1).intValue()
    (0 until size / pow2level).map(twiddleNum => (pow2level * twiddleNum) / 2)
  }

  def DirectFftTwiddleIndexes(stageLevel: Int, size: Int, start: Int, count: Int): Seq[Int] = {
    val pow2level = math.pow(2, stageLevel + 1).intValue()
    assert(start + count <= size / pow2level)

    (start until start + count).map(twiddleNum => (pow2level * twiddleNum) / 2)
  }
}
