/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.fft.FFTUtils

import bench.utils.generation.Require
import bench.utils.hardware.{Delay, RingElem}
import bench.utils.components.memory.ShiftRegisterMem

import chisel3._
import chisel3.util.{Counter, ShiftRegister}
import dsptools.numbers.Ring

/** Decimation in Frequency Butterfly.
 *  @param elemType
 *    type of data
 *  @param twiddleIndexes
 *    sequence of indexes (used to generate the right twiddle values)
 *  @param stageLevel
 *    stage level
 *  @param size
 *    fft size
 */
class DIFButterfly[T <: Data: Ring](elemType: RingElem[T], twiddleIndexes: Seq[Int], stageLevel: Int, size: Int)
    extends Module {
  require(Require.isPow2(size), s"size must a power of 2")

  val io = IO(new Bundle() {
    val in0 = Input(elemType)
    val in1 = Input(elemType)

    val inEnable = Input(Bool())
    val inValid  = Input(Bool())

    val out0 = Output(elemType)
    val out1 = Output(elemType)

    val outEnable = Output(Bool())
    val outValid  = Output(Bool())
  })

  def getPipelineLevel(): Int = {
    Delay.getAddDelay(elemType) + Delay.getMultDelay(elemType)
  }

  val twiddleRom = Module(new TwiddleFactorROM(elemType, size, twiddleIndexes))

  // The counter is enabled only if a NEW VALID data is ready when the multiplication start.
  val enableCounter = ShiftRegister(io.inValid && io.inEnable, Delay.getAddDelay(elemType))
  twiddleRom.io.index := Counter(enableCounter, twiddleRom.ROMSize)._1

  val addResult  = Wire(elemType)
  val subResult  = Wire(elemType)
  val out1Result = Wire(elemType)
  val out0Result = Wire(elemType)

  // Butterfly compute in DIF way.
  addResult := io.in0 + io.in1
  subResult := io.in0 - io.in1

  out0Result := ShiftRegisterMem(addResult, Delay.getMultDelay(elemType), name = "shift_out0") // Sync with out1 output
  out1Result := subResult * twiddleRom.io.twiddle

  // Connect outputs
  io.out0 := out0Result
  io.out1 := out1Result
  io.outEnable := ShiftRegister(io.inEnable, getPipelineLevel())
  io.outValid := ShiftRegister(io.inValid, getPipelineLevel())
}
