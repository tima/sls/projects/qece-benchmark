/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.fft.ParallelizedFFT

import bench.kernels.fft.DirectFFT.{DirectFFTIndexes, DirectFFTModule}
import bench.kernels.fft.R2MDC.{R2MDCModule, R2MDCIndexes}
import bench.kernels.fft.FFTUtils.BitReverse

import bench.utils.generation.Require
import bench.utils.hardware.RingElem

import chisel3._
import chisel3.util.{Reverse, log2Ceil}
import dsptools.numbers.Ring

import logger.LazyLogging

/** Raw FFT computation : the input and output are scrambled. It will use different strategy according to the lane
 *  value. In most cases it will begin with a DirectFFT and finish multiple independent R2MDC modules.
 *
 *  @param elemType
 *    type of data
 *  @param size
 *    size of the FFT
 *  @param lanes
 *    number of lanes (degree of parallelization)
 */
class ParallelizedFFTModule[T <: Data: Ring](elemType: RingElem[T], size: Int, lanes: Int)
    extends Module
    with LazyLogging {
  require(Require.isPow2(size), s"size must a power of 2")
  require(Require.isPow2(lanes), s"lanes must a power of 2")
  require(lanes >= 2, s"lanes must be >= 2")
  require(lanes <= size, s"lanes cannot exceed the FFT size")

  val io = IO(new Bundle() {
    val in       = Input(Vec(lanes, elemType))
    val inValid  = Input(Bool())
    val out      = Output(Vec(lanes, elemType))
    val outValid = Output(Bool())
    val outIndex = Output(UInt(log2Ceil(size).W))
  })

  def getPipelineLevel(): Int = {
    pipelineLevel
  }

  var pipelineLevel = 0

  if (lanes == size) { // The parallelization require a full DirectFFT
    logger.debug(s"FFT Mode : Direct ($lanes elements / cycle)")
    val directFFT = Module(new DirectFFTModule(elemType, size, lanes))

    directFFT.io.in := io.in
    directFFT.io.inValid := io.inValid

    // Reorganize the data according to the bit reverse technique
    for (lane <- 0 until lanes) {
      io.out(lane) := directFFT.io.out(BitReverse(lane, log2Ceil(lanes)))
    }

    io.outValid := directFFT.io.outValid
    io.outIndex := 0.U // This output is not necessary in this case.

    pipelineLevel = directFFT.getPipelineLevel() + 1

  } else { // DirectFFT until the data can be computed separately with R2DMC module(s)
    logger.debug(s"FFT Mode : Multi lane ($lanes elements / cycle)")
    val directFFT     = Module(new DirectFFTModule(elemType, size, lanes))
    val r2dmcFFTArray = new Array[R2MDCModule[T]](lanes / 2)

    val directFFTOutputIndexes = new DirectFFTIndexes(size, lanes).outputIndexes()
    val r2mdcInputIndexes      = new R2MDCIndexes(size, lanes).inputIndexes()

    // First we begin with a direct FFT in order to separate lanes that can be computed separately.
    directFFT.io.in := io.in
    directFFT.io.inValid := io.inValid

    // Then we continue with independent Radix 2 FFTs (each module take 2 lane)
    for ((indexes, lane) <- r2mdcInputIndexes.zipWithIndex) {
      val r2dmcFFT = Module(new R2MDCModule(elemType, size, lanes, lane * 2))
      r2dmcFFT.io.in0 := directFFT.io.out(directFFTOutputIndexes.indexOf(indexes(0)))
      r2dmcFFT.io.in1 := directFFT.io.out(directFFTOutputIndexes.indexOf(indexes(1)))
      r2dmcFFT.io.inValid := directFFT.io.outValid
      r2dmcFFTArray(lane) = r2dmcFFT
    }

    // Reorganize the R2DMC modules in order to have consecutive output index connected to the output
    // (because the output is scrambled in decimation in frequency mode)
    for ((index, r2dmc) <- (0 until lanes / 2).zip(r2dmcFFTArray.sortBy(r2dmc => r2dmc.getFirstOutputIndex()))) {
      //logger.debug("Output out[" + index + "] is connected to index " + r2dmc.getIndexationStart())
      // logger.debug("Output out["+ (index + lanes / 2) + "] is connected to index " + (r2dmc.getIndexationStart() +
      // size / 2))
      io.out(index) := r2dmc.io.out0
      io.out(index + lanes / 2) := r2dmc.io.out1
    }

    val outputValid  = r2dmcFFTArray(0).io.outValid
    val indexCounter = RegInit(UInt(log2Ceil(size).W), 0.U)

    when(outputValid) {
      when(indexCounter === (2 * (size / lanes - 1)).U) {
        indexCounter := 0.U
      } otherwise {
        indexCounter := indexCounter + 2.U
      }
    }

    io.outIndex := Reverse(indexCounter) // Return the first result index of the output vector.
    io.outValid := outputValid

    pipelineLevel = directFFT.getPipelineLevel() + r2dmcFFTArray(0).getPipelineLevel() + 1
  }

}
