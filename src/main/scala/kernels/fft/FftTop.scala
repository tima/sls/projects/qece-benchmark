/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.fft

import bench.kernels.fft.DirectFFT.DirectFFTIndexes
import bench.kernels.fft.ParallelizedFFT.ParallelizedFFTModule

import bench.utils.components.Role
import bench.utils.components.memory.PingPongBuffer
import bench.utils.hardware.RingElem
import bench.utils.generation.Require

import chisel3._
import chisel3.util._
import dsptools.numbers._

/** Fast Fourier Transformation implementation using Radix 2 Decimation in Frequency Multipath Delay Commutator design
 *  or Direct FFT according to the level of parallelization.
 *
 *  @param bitWidth
 *    bitwidth used for input on both FIFOs. It will define the number of lane used for computing
 *  @param elemType
 *    type of data (must be a DSPComplex)
 *  @param size
 *    defining the FFT size
 */
class FftRole[T <: Data: Ring](bitWidth: Int, elemType: RingElem[T], size: Int) extends Role(bitWidth) {
  require(Require.isPow2(size), s"size must a power of 2")

  val lanes = bitWidth / elemType.getWidth

  require(Require.isPow2(lanes), s"lanes must a power of 2")
  require(lanes >= 2, s"lanes must be >= 2")
  require(lanes <= size, s"lanes cannot exceed the FFT size")

  val sizePerLane = size / lanes

  def getDelay(): Int = {
    if (lanes == size) {
      fft.getPipelineLevel()
    } else {
      fft.getPipelineLevel() + 2 * sizePerLane + 2 // Overhead due to input / output buffers
    }
  }

  val fftInputIndexes = new DirectFFTIndexes(size, lanes).inputIndexes()
  val fft             = Module(new ParallelizedFFTModule(elemType, size, lanes))
  val output          = Wire(Vec(lanes, elemType))

  io.in.ready := true.B
  io.out.bits := output.asUInt()

  // Generate the FFT wrapper according to the mode
  if (lanes == size) {
    directFFTMode()
  } else {
    r2mdcMode()
  }

  def directFFTMode(): Unit = {
    val input = io.in.bits.asTypeOf(Vec(lanes, elemType))
    fft.io.inValid := io.in.valid

    for (lane <- 0 until lanes) {
      fft.io.in(fftInputIndexes.indexOf(lane * sizePerLane)) := input(lane)
      output(lane) := fft.io.out(lanes - 1 - lane)
    }
    io.out.valid := fft.io.outValid
  }

  def r2mdcMode(): Unit = {

    pingPongBufferIn()
    pingPongBufferOut()

    /** Interfacing FIFO output with the FFT input */
    def pingPongBufferIn(): Unit = {
      val bufferIn = Module(new FFTFIFOInterface(elemType, size, lanes))

      for (lane <- 0 until lanes) {
        fft.io.in(fftInputIndexes.indexOf(lane * sizePerLane)) := bufferIn.io.out(lane)
      }

      bufferIn.io.in := io.in.bits
      bufferIn.io.enable := io.in.valid
      fft.io.inValid := bufferIn.io.outValid
    }

    /** Interfacing FFT output with the FIFO Input */
    def pingPongBufferOut(): Unit = {
      // Because the fft output give only 2 packets (of size lanes/2) of data in the right order, we use 2 memories to
      // separate the buddy packets in order to access to a packet of consecutive elements in 1 cycle by accessing to
      // 2 packet of size lanes/2 in the same time.

      val bufferOut = Module(new PingPongBuffer(elemType, lanes / 2, sizePerLane, 2))

      val writeAddr = Wire(Vec(2, UInt(log2Ceil(size).W)))
      val writeData = Wire(Vec(2, Vec(lanes / 2, elemType)))
      val readAddr  = Wire(Vec(2, UInt(log2Ceil(size).W)))

      // The address is computed in order to reorganize each packet in the right order.
      val addr = fft.io.outIndex >> log2Ceil(lanes).U

      val counterReg       = RegInit(UInt(log2Ceil(sizePerLane).W), 0.U) // Counter to know the output progression
      val selectMem        = RegInit(Bool(), false.B)                    // Signal for switching the input with buffer banks
      val delayedSelectMem = RegNext(selectMem)
      val swapMem          = RegInit(Bool(), false.B)                    // Ping / Pong signal

      readAddr(0) := counterReg
      readAddr(1) := counterReg

      // Update the counter and update signals according to the counter value.
      when(fft.io.outValid) {
        when(counterReg === (sizePerLane / 2 - 1).U) {
          selectMem := !selectMem
        }
        when(counterReg === (sizePerLane - 1).U) {
          counterReg := 0.U
          selectMem := !selectMem
          swapMem := !swapMem
        } otherwise {
          counterReg := counterReg + 1.U
        }
      }

      // Reorganize the FFT output before giving to the buffer
      when(selectMem) {
        writeAddr(0) := addr + (sizePerLane / 2).U
        writeAddr(1) := addr
        for (i <- 0 until lanes / 2) {
          writeData(1)(i) := fft.io.out(i)
          writeData(0)(i) := fft.io.out(i + lanes / 2)
        }
      } otherwise {
        writeAddr(0) := addr
        writeAddr(1) := addr + (sizePerLane / 2).U
        for (i <- 0 until lanes / 2) {
          writeData(0)(i) := fft.io.out(i)
          writeData(1)(i) := fft.io.out(i + lanes / 2)
        }
      }

      // Reorganize the output buffer before giving to the FIFO
      when(delayedSelectMem) {
        for (i <- 0 until lanes / 2) {
          output(i) := bufferOut.io.out(0)(lanes / 2 - i - 1)
          output(i + lanes / 2) := bufferOut.io.out(1)(lanes / 2 - i - 1)
        }
      } otherwise {
        for (i <- 0 until lanes / 2) {
          output(i) := bufferOut.io.out(1)(lanes / 2 - i - 1)
          output(i + lanes / 2) := bufferOut.io.out(0)(lanes / 2 - i - 1)
        }
      }

      bufferOut.io.enable := fft.io.outValid
      bufferOut.io.inValid := fft.io.outValid
      bufferOut.io.swap := swapMem
      bufferOut.io.in := writeData
      bufferOut.io.outAddr := readAddr
      bufferOut.io.inAddr := writeAddr

      io.out.valid := bufferOut.io.outValid
    }
  }
}
