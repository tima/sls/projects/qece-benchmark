/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.fft.R2MDC

import bench.kernels.fft.FFTUtils.BitReverse

import bench.utils.hardware.RingElem

import chisel3._
import chisel3.util.log2Ceil
import dsptools.numbers.Ring

/** Radix 2 - Multipath Delay Commutator Implementation. The pipelined way to compute FFT.
 *
 *  @param elemType
 *    type of data
 *  @param size
 *    size of the FFT
 *  @param lanes
 *    number of lanes
 *  @param laneNumber
 *    in which lane the module is.
 */
class R2MDCModule[T <: Data: Ring](elemType: RingElem[T], size: Int, lanes: Int, laneNumber: Int) extends Module {

  val io = IO(new Bundle() {
    val in0      = Input(elemType)
    val in1      = Input(elemType)
    val inValid  = Input(Bool())
    val out0     = Output(elemType)
    val out1     = Output(elemType)
    val outValid = Output(Bool())
  })

  def getPipelineLevel(): Int = {
    pipelineLevel
  }

  def getFirstOutputIndex(): Int = {
    val indexationLength = size / (lanes / 2)
    BitReverse(indexationLength * (laneNumber / 2), log2Ceil(size))
  }

  val stageLevelStart = log2Ceil(lanes)

  val stages = new Array[Stage[T]](log2Ceil(size) - stageLevelStart)

  stages(0) = Module(new Stage[T](elemType, stageLevelStart, size))
  stages(0).io.inValid := io.inValid
  stages(0).io.inEnable := true.B // Todo: Update to have the possibility to flush
  stages(0).io.in0 := io.in0
  stages(0).io.in1 := io.in1

  var pipelineLevel = stages(0).getPipelineLevel()

  for (stageLevel <- stageLevelStart + 1 until log2Ceil(size)) {
    val previousStage = stages(stageLevel - (stageLevelStart + 1))
    val stage         = Module(new Stage[T](elemType, stageLevel, size))
    stage.io.inValid := previousStage.io.outValid
    stage.io.inEnable := previousStage.io.outEnable
    stage.io.in0 := previousStage.io.out0
    stage.io.in1 := previousStage.io.out1
    stages(stageLevel - stageLevelStart) = stage
    pipelineLevel += stage.getPipelineLevel()
  }

  val lastStage = stages(stages.size - 1)
  io.out0 := lastStage.io.out0
  io.out1 := lastStage.io.out1
  io.outValid := lastStage.io.outValid && lastStage.io.outEnable
}
