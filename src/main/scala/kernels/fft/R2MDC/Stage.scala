/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.fft.R2MDC

import bench.kernels.fft.FFTUtils.{DIFButterfly, TwiddleFactorROM}

import bench.utils.components.memory.ShiftRegisterMem
import bench.utils.hardware.RingElem

import chisel3._
import chisel3.util._
import dsptools.numbers.Ring

/** A R2MDC stage composed of a commutator, a butterfly and delays
 *  @param elemType
 *    type of data
 *  @param stageLevel
 *    stage level
 *  @param size
 *    size of the FFT
 */
class Stage[T <: Data: Ring](elemType: RingElem[T], stageLevel: Int, size: Int) extends MultiIOModule {

  val io = IO(new Bundle() {
    val in0 = Input(elemType)
    val in1 = Input(elemType)

    val inEnable = Input(Bool())
    val inValid  = Input(Bool())

    val out0 = Output(elemType)
    val out1 = Output(elemType)

    val outEnable = Output(Bool())
    val outValid  = Output(Bool())
  })

  def getPipelineLevel(): Int = {
    butterflyUnit.getPipelineLevel() + delay
  }

  val delay = math.pow(2, log2Ceil(size) - stageLevel - 1).intValue()

  // Modules
  val commutator     = Module(new Commutator(elemType, delay))
  val twiddleIndexes = TwiddleFactorROM.twiddleIndexes(stageLevel, size)
  val butterflyUnit  = Module(new DIFButterfly(elemType, twiddleIndexes, stageLevel, size))

  // Delaying signals
  val shiftReg0  = ShiftRegisterMem(commutator.io.out0, delay, io.inEnable, name = "shift_in0")
  val shiftReg1  = ShiftRegisterMem(io.in1, delay, io.inEnable, name = "shift_in1")
  val shiftValid = ShiftRegister(io.inValid, delay, io.inEnable)

  // Connect enable
  commutator.io.enable := io.inEnable && io.inValid
  butterflyUnit.io.inEnable := io.inEnable
  io.outEnable := butterflyUnit.io.outEnable

  // Connect valid
  butterflyUnit.io.inValid := shiftValid
  io.outValid := butterflyUnit.io.outValid

  // Connect in and out
  commutator.io.in0 := io.in0
  commutator.io.in1 := shiftReg1
  butterflyUnit.io.in0 := shiftReg0
  butterflyUnit.io.in1 := commutator.io.out1
  io.out0 := butterflyUnit.io.out0
  io.out1 := butterflyUnit.io.out1
}
