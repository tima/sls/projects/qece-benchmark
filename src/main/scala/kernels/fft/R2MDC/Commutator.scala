/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.fft.R2MDC

import bench.kernels.fft.FFTUtils.CounterWithTop

import bench.utils.hardware.RingElem

import chisel3._
import dsptools.numbers.Ring

/** Commutator implementation. Used in R2MDC module
 *  @param elemType
 *    type of data
 *  @param delay
 *    delay between commutations
 */
class Commutator[T <: Data: Ring](elemType: RingElem[T], delay: Int) extends Module {
  // WARNING: in0 and in1 must be sync.
  val io = IO(new Bundle() {
    val enable   = Input(Bool())
    val in0      = Input(elemType)
    val in1      = Input(elemType)
    val out0     = Output(elemType)
    val out1     = Output(elemType)
    val commuted = Output(Bool())
  })

  // Commute state
  val regCommute = RegInit(Bool(), false.B)
  val counter    = Module(new CounterWithTop(delay))
  counter.io.enable := io.enable

  // Commute when top and enable and valid
  when(counter.io.top && io.enable) {
    regCommute := !regCommute
  }

  when(regCommute) {
    io.out0 := io.in1
    io.out1 := io.in0
  } otherwise {
    io.out0 := io.in0
    io.out1 := io.in1
  }

  io.commuted := regCommute
}
