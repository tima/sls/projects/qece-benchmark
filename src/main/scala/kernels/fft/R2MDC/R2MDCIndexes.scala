/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.fft.R2MDC

import chisel3.util.log2Ceil

/** Helper to get the sample indexes for R2MDC module
 *  @param fftSize
 *    size of the fft
 *  @param lanes
 *    number of lane
 */
class R2MDCIndexes(fftSize: Int, lanes: Int) {

  private def commuteIndexes(indexes: Array[Int], stageLevel: Int): (Array[Int], Array[Int]) = {
    val result = (new Array[Int](indexes.size / 2), new Array[Int](indexes.size / 2))
    val skip   = indexes.size / math.pow(2, stageLevel).toInt

    for (i <- 0 until indexes.length by skip) {
      for (j <- 0 until skip / 2) {
        result._1(i / 2 + j) = indexes(i + j)
        result._2(i / 2 + j) = indexes(i + j + (skip / 2))
      }
    }
    result
  }

  private def regroupLanes(commutedIndexes: (Array[Int], Array[Int])): Array[Array[Int]] = {
    val result     = Array.ofDim[Int](lanes / 2, 2)
    val sizeByLane = fftSize / lanes
    for (lane <- 0 until lanes / 2) {
      result(lane)(0) = commutedIndexes._1(lane * sizeByLane)
      result(lane)(1) = commutedIndexes._2(lane * sizeByLane)
    }
    result
  }

  def inputIndexes(): Array[Array[Int]] = {
    val indexes = (0 until fftSize).toArray
    val out     = commuteIndexes(indexes, log2Ceil(lanes) - 1)
    regroupLanes(out)
  }
}

/** Uncomment to test R2MDC object R2MDCIndexesTest extends App { val m = new R2MDCIndexes(32, 16) for(lane <-
 *  m.inputIndexes) { println("Lane $lane : " + lane.toIndexedSeq) } }
 */
