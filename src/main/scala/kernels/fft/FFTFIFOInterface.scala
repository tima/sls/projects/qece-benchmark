/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.fft

import bench.utils.components.memory.BankedMemory
import bench.utils.hardware.RingElem

import chisel3._
import chisel3.util._
import dsptools.numbers._

/** Specific implementation of a PingPongBuffer used for interfacing the FIFO with the FFT. It will reorganize the
 *  entries to give the right ones to the FFT lanes
 *
 *  @param elemType
 *    type of data
 *  @param size
 *    size of the FFT
 *  @param lanes
 *    number of lanes
 */
class FFTFIFOInterface[T <: Data: Ring](elemType: RingElem[T], size: Int, lanes: Int) extends Module {
  val delta = size / lanes
  val io = IO(new Bundle {
    val in       = Input(UInt((lanes * elemType.getWidth).W))
    val out      = Output(Vec(lanes, elemType))
    val enable   = Input(Bool())
    val outValid = Output(Bool())
  })

  val input         = io.in.asTypeOf(Vec(lanes, elemType))
  val swap          = RegInit(Bool(), false.B)
  val delayedSwap   = RegNext(swap, false.B)
  val delayedEnable = RegNext(io.enable, false.B)
  val pingValidity  = RegInit(Bool(), false.B)
  val pongValidity  = RegInit(Bool(), false.B)

  def getPipelineLevel(): Int = {
    delta + 1
  }

  if (delta <= lanes) {
    // The FIFO give samples that must be in different bank

    val pingBuffer = BankedMemory(lanes, 1, delta * elemType.getWidth)
    val pongBuffer = BankedMemory(lanes, 1, delta * elemType.getWidth)

    // Input
    val inIndex = RegInit(UInt(log2Ceil(lanes).W), 0.U) // Counter to know the input progression

    when(io.enable) {
      when(inIndex === (lanes - 1).U) {
        inIndex := 0.U
      } otherwise {
        inIndex := inIndex + (lanes / delta).U
      }

      when(swap) {
        pongValidity := true.B
        for (lane <- 0 until lanes by delta) {
          val data = Wire(Vec(delta, elemType))
          for (i <- 0 until delta) data(i) := input(lane + i)
          pongBuffer.write(inIndex + (lane / delta).U, 0.U, data.asUInt())
        }
      } otherwise {
        pingValidity := true.B
        for (lane <- 0 until lanes by delta) {
          val data = Wire(Vec(delta, elemType))
          for (i <- 0 until delta) data(i) := input(lane + i)
          pingBuffer.write(inIndex + (lane / delta).U, 0.U, data.asUInt())
        }
      }
    }

    // Output
    val outIndex        = RegInit(UInt(log2Ceil(delta).W), 0.U) // Counter to know the output progression
    val delayedOutIndex = RegNext(outIndex, 0.U)

    when(io.enable) {
      when(outIndex === (delta - 1).U) {
        swap := !swap
      }
      outIndex := outIndex + 1.U
    }

    when(delayedSwap) {
      for (i <- 0 until lanes) io.out(i) := pingBuffer.read(i.U, 0.U).asTypeOf(Vec(delta, elemType))(delayedOutIndex)
      io.outValid := pingValidity && delayedEnable
    } otherwise {
      for (i <- 0 until lanes) io.out(i) := pongBuffer.read(i.U, 0.U).asTypeOf(Vec(delta, elemType))(delayedOutIndex)
      io.outValid := pongValidity && delayedEnable
    }

  } else {
    // The FIFO give samples that must be in the same bank
    val pingBuffer = BankedMemory(lanes, delta / lanes, lanes * elemType.getWidth)
    val pongBuffer = BankedMemory(lanes, delta / lanes, lanes * elemType.getWidth)

    // Input
    val bankIndex = RegInit(UInt(log2Ceil(lanes).W), 0.U)
    val addrIn    = RegInit(UInt(log2Ceil(delta / lanes).W), 0.U)

    when(io.enable) {
      when(addrIn === (delta / lanes - 1).U) {
        bankIndex := bankIndex + 1.U
        when(bankIndex === (lanes - 1).U) {
          swap := !swap
        }
      }
      addrIn := addrIn + 1.U

      when(swap) {
        pongValidity := true.B
        pongBuffer.write(bankIndex, addrIn, io.in)
      } otherwise {
        pingValidity := true.B
        pingBuffer.write(bankIndex, addrIn, io.in)
      }
    }

    // Output
    val addrOut       = RegInit(UInt(log2Ceil(delta / lanes).W), 0.U)
    val vecOut        = RegInit(UInt(log2Ceil(lanes).W), 0.U)
    val delayedVecOut = RegNext(vecOut, 0.U)

    when(io.enable) {
      when(vecOut === (lanes - 1).U) {
        addrOut := addrOut + 1.U
      }
      vecOut := vecOut + 1
    }

    when(delayedSwap) {
      for (lane <- 0 until lanes)
        io.out(lane) := pingBuffer.read(lane.U, addrOut).asTypeOf(Vec(lanes, elemType))(delayedVecOut)
      io.outValid := pingValidity && delayedEnable
    } otherwise {
      for (lane <- 0 until lanes)
        io.out(lane) := pongBuffer.read(lane.U, addrOut).asTypeOf(Vec(lanes, elemType))(delayedVecOut)
      io.outValid := pongValidity && delayedEnable
    }

  }
}
