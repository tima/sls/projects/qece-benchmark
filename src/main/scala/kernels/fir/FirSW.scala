/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.fir

import bench.utils.software.Numeric

class FirRef(
    var coeffs: Array[Numeric],
    nbElement: Int,
    signalGenerator: Iterator[Numeric],
    isDouble: Boolean = false
) {
  val buffer = new Array[Numeric](coeffs.length)
  this.initBuffer()

  def initBuffer(): Unit = {
    // Compute how many samples was necessary to fill the DataBuffer
    val buffSizeHW = nbElement + coeffs.length - 1
    val nbVectors  = if (buffSizeHW % nbElement == 0) buffSizeHW / nbElement else buffSizeHW / nbElement + 1
    val nbSamples  = nbVectors * nbElement
    (0 until nbSamples).map(_ => addBuff(signalGenerator.next()))
  }

  def resetBuffer(): Unit = {
    (0 until buffer.length).map(i => buffer(i) = Numeric(0))
  }

  def compute(): Numeric = {
    var result = if (isDouble) Numeric(0.0) else Numeric(0)
    addBuff(signalGenerator.next())
    for (i <- 0 until coeffs.length) {
      result += coeffs(i) * buffer(i)
    }
    result
  }

  def addBuff(sample: Numeric): Unit = {
    for (i <- coeffs.length - 1 to 1 by -1) {
      buffer(i) = buffer(i - 1)
    }
    buffer(0) = sample
  }

  /** Test the filter using a dirac signalGenerator must be a DiracSignalGenerator with delay = coeffs.length */
  def testDirac(): Unit = {
    for (i <- 0 until coeffs.length) {
      assert(compute() == coeffs(i))
    }
  }
}
