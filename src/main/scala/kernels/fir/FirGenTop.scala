/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.fir

import bench.utils.components.{Mac, Role}
import bench.utils.components.memory.{ReadOnlyMemory, CoeffRAM}

import bench.utils.hardware.RingElem
import bench.utils.generation.Require
import bench.utils.software.Numeric

import scala.math.ceil

import chisel3._
import chisel3.util._
import dsptools.numbers.Ring

/** Finite Impulse Response Filter implementation
 *
 *  @param bitWidth
 *    bitwidth used for input on both FIFOs
 *  @param elemType
 *    type of data
 *  @param nbTap
 *    number of taps used in this FIR
 *  @param coeffs
 *    coefficients used for filtering, if static
 *  @todo
 *    TODO add optional normalization
 */
class FirGenRole[T <: Data: Ring](
    bitWidth: Int,
    elemType: RingElem[T],
    nbTap: Int,
    coeffs: Option[Array[Numeric]] = None
) extends Role(bitWidth) {
  require(Require.divide(elemType.getWidth, bitWidth), s"Element width must divide input bitwidth")
  coeffs match {
    case Some(c) => require(c.size == nbTap, s"Number of taps must be equal to number of values in ROM")
    case _       =>
  }
  // TODO: add normalization
  //require(Require.isPow2(coeffs.sum), s"The sum of all coefficients must be a power of 2")

  // Constants
  val elemWidth = elemType.getWidth
  val nbElement = bitWidth / elemWidth
  val buffSize  = nbElement + nbTap - 1

  // How many vectors of elements are necessary to fill the buffer
  val nbFillCycle = ceil(buffSize.toDouble / nbElement).toInt

  // Manage memory loading delay if needed
  val nbLoadCycle = coeffs match {
    case Some(_) => 0
    case None    => ceil(nbTap.toDouble / nbElement).toInt
  }

  // Modules
  val (coeffMem, coeffValues) = coeffs match {
    case Some(c) =>
      val tmp = ReadOnlyMemory(elemType, c)
      (Left(tmp), tmp.values)
    case None =>
      val tmp = CoeffRAM(elemType, nbTap, nbElement)
      (Right(tmp), tmp.values)
  }
  val windowBuffer = Module(new DataBuffer(elemType, bitWidth, buffSize))
  val macUnits     = (0 until nbElement).map(_ => Module(new Mac(elemType, nbTap)))

  // output related signals
  val outputValid =
    ShiftRegister(io.in.valid, nbLoadCycle + nbFillCycle + 1 + macUnits.head.getPipelineLevel, io.in.valid)
  val serializedOutput = Wire(Vec(nbElement, elemType)) // Used to serialize all mac results

  // Default configuration
  io.out.valid := outputValid
  io.out.bits := serializedOutput.asTypeOf(UInt(bitWidth.W))
  io.in.ready := true.B

  // filling out window buffer
  windowBuffer.io.dataIn.zip(io.in.bits.asTypeOf(Vec(nbElement, elemType))).foreach { case (a, b) => a := b }
  windowBuffer.io.stall := !io.in.valid

  // Connect all macs.
  for (i <- 0 until nbElement) {
    macUnits(i).io.op2 := coeffValues
    // cf https://stackoverflow.com/questions/45373902/chisel3-partial-assignment-to-a-multi-bit-slice-of-a-vector-io
    macUnits(i).io.op1.zip(windowBuffer.io.dataOut.slice(i, nbTap + i)).foreach { case (a, b) => a := b }
    serializedOutput(i) := macUnits(i).io.result
  }

  coeffMem match {
    case Right(mem) =>
      val counter = RegInit(UInt(log2Up(nbLoadCycle + 1).W), 0.U)
      when(io.in.valid && counter < nbLoadCycle.U) {
        mem.write(counter, io.in.bits)
        counter := counter + 1.U
      }
    case _ =>
  }
}
