/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.fir

import bench.utils.hardware.RingElem

import chisel3._
import dsptools.numbers.Ring

/** Buffer acting like a FIFO buffer for updating values. The only difference is that we can access the entire buffer
 *
 *  @param gen
 *    type of data
 *  @param bitWidth
 *    define the size of the input (number of samples taken in each cycle)
 *  @param buffSize
 *    define the size of the buffer
 */
class DataBuffer[T <: Data: Ring](val gen: RingElem[T], val bitWidth: Int, val buffSize: Int) extends Module {
  // Constants
  val elemWidth = gen.getWidth
  val nbElement = bitWidth / elemWidth

  // IO definition
  val io = IO(new Bundle {
    val stall   = Input(Bool())
    val dataIn  = Input(Vec(nbElement, gen))
    val dataOut = Output(Vec(buffSize, gen))
  })

  val buff = Reg(Vec(buffSize, gen))

  // Default configuration
  io.dataOut := buff

  when(!io.stall) {
    // Shift values and put the freshest values from dataIn.
    for (i <- 0 until buffSize - nbElement) {
      buff(i + nbElement) := buff(i)
    }
    for (i <- 0 until nbElement) {
      buff(i) := io.dataIn(i)
    }
  }
  // Otherwise the register preserve their value.

}
