/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.huffman

import bench.kernels.deflate.tools.{CodeBundle, Constants}

import bench.utils.hardware.Delay

import chisel3._
import chisel3.util.{ShiftRegister, Valid, log2Ceil}
import dsptools.DspContext
import dsptools.numbers._

/** A Deflate Huffman encoder which convert codes (as defined in section 3.2.5 of the RFC1951) into Huffman code
 *  @param literalHuffCodes
 *    table to convert literals
 *  @param distanceHuffCodes
 *    table to convert distance code
 */
class HuffmanEncoder(literalHuffCodes: Seq[(Int, Int)], distanceHuffCodes: Seq[(Int, Int)]) extends Module {

  val outputWidth = HuffmanTable.getSymbolMaxWidth(literalHuffCodes) +
    Constants.LITERAL_EXTRA_BITS_WIDTH +
    HuffmanTable.getSymbolMaxWidth(distanceHuffCodes) +
    Constants.DISTANCE_EXTRA_BITS_WIDTH

  val io = IO(new Bundle() {
    val literalCode =
      Input(new CodeBundle(Constants.LITERAL_CODE_WIDTH, Constants.LITERAL_EXTRA_BITS_WIDTH)) // from 0 -> 285
    val distanceCode =
      Input(new CodeBundle(Constants.DISTANCE_CODE_WIDTH, Constants.DISTANCE_EXTRA_BITS_WIDTH)) // from 0 -> 29
    val inValid = Input(Bool())
    val output  = Output(Valid(UInt(outputWidth.W)))
    val size    = Output(UInt(log2Ceil(outputWidth).W))
  })

  val delay = Delay.getAddDelay(io.size.cloneType)

  def getPipelineLevel: Int = delay + literalHuffman.getPipelineLevel

  val literalHuffman = Module(
      new HuffmanTable(Constants.LITERAL_CODE_WIDTH, Constants.LITERAL_EXTRA_BITS_WIDTH, literalHuffCodes)
  )
  val distanceHuffman = Module(
      new HuffmanTable(Constants.DISTANCE_CODE_WIDTH, Constants.DISTANCE_EXTRA_BITS_WIDTH, distanceHuffCodes)
  )
  literalHuffman.io.code := io.literalCode
  distanceHuffman.io.code := io.distanceCode

  // Note: the output must be written in LSB first
  io.output.bits := ShiftRegister(
      (distanceHuffman.io.output << literalHuffman.io.size).asUInt() | literalHuffman.io.output,
      delay
  )
  io.output.valid := ShiftRegister(io.inValid, getPipelineLevel)

  DspContext.withNumAddPipes(delay) {
    io.size := literalHuffman.io.size context_+ distanceHuffman.io.size
  }

}
