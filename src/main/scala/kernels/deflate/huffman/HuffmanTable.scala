/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.huffman

import bench.utils.hardware.Delay

import bench.kernels.deflate.tools.{BitReverse, CodeBundle, Constants}

import chisel3._
import chisel3.util.{Cat, ShiftRegister, log2Ceil}
import dsptools.DspContext
import dsptools.numbers._

/** Huffman substitution using a STATIC table. It handles also extra bits which are specified by the input code bundle
 *  @param codeLength
 *    Maximum bitwidth of the huffman representation
 *  @param extraBitsLength
 *    Maximum bitwidth to code extra bits
 *  @param table
 *    Huffman table use to convert code to huffman representation. It's composed of 2 integer : the first is the code
 *    length, and the second is the Huffman code.
 */
class HuffmanTable(codeLength: Int, extraBitsLength: Int, table: Seq[(Int, Int)]) extends Module {
  val wordLength = HuffmanTable.getSymbolMaxWidth(table)

  val io = IO(new Bundle {
    val code   = Input(new CodeBundle(codeLength, extraBitsLength))
    val output = Output(UInt((wordLength + extraBitsLength).W))
    val size   = Output(UInt(log2Ceil(wordLength + extraBitsLength).W))
  })
  // Todo : improve to have a dynamic table.

  def getPipelineLevel: Int = Delay.getAddDelay(io.size.cloneType)

  val sizeLength = log2Ceil(wordLength)
  val mem        = VecInit(table.map(v => Cat(v._1.U(sizeLength.W), v._2.U(wordLength.W))))
  val result     = mem(io.code.code)

  DspContext.withNumAddPipes(Delay.getAddDelay(io.size.cloneType)) {
    // Note: the output must be written in lsb first
    io.output := ShiftRegister(
        (io.code.extraBits << result(result.getWidth - 1, wordLength)).asUInt() | result(wordLength - 1, 0).asUInt(),
        getPipelineLevel
    )
    io.size := io.code.extraBitsLength context_+ result(sizeLength + wordLength - 1, wordLength)
  }
}

object HuffmanTable {

  /** Generate the fixed Huffman tree (table) for literals only. Table RFC1951 compilant (section 3.2.6)
   *  @return
   *    the literal fixed Huffman codes
   */
  def fixedLiteralTable(): Array[(Int, Int)] = {
    val array = new Array[(Int, Int)](Constants.LITERAL_TABLE_LENGTH)

    // 0 - 143
    for (symbol <- 0 to 143) {
      array(symbol) = (8, symbol - (0 - 0x30))
    }

    // 144 - 255
    for (symbol <- 144 to 255) {
      array(symbol) = (9, symbol - (144 - 0x190))
    }

    // 256 - 279
    for (symbol <- 256 to 279) {
      array(symbol) = (7, symbol - (256 - 0x0))
    }

    // 280 - 287
    for (symbol <- 280 to 287) {
      array(symbol) = (8, symbol - (280 - 0xc0))
    }

    // Convert every symbol in lsb first
    for (i <- 0 until array.size) {
      array(i) = (array(i)._1, BitReverse(array(i)._2, array(i)._1))
    }
    array
  }

  /** Generate the fixed Huffman tree (table) for distance only. Table RFC1951 compilant (section 3.2.6)
   *  @return
   *    the distance fixed Huffman codes
   */
  def fixedDistanceTable(): Array[(Int, Int)] = {
    val array = new Array[(Int, Int)](Constants.DISTANCE_TABLE_LENGTH)

    // 0 : exception to the rule. Here it means no length needed
    array(0) = (0, 0)

    // 1 - 29
    for (symbol <- 1 to 29) {
      array(symbol) = (5, symbol)
    }

    // Convert every symbol in lsb first
    for (i <- 0 until array.size) {
      array(i) = (array(i)._1, BitReverse(array(i)._2, array(i)._1))
    }
    array
  }

  /** Return the maximal bitwidth use to encode with Huffman codes
   *  @param table
   *    to examine
   *  @return
   *    maximum bitwidth
   */
  def getSymbolMaxWidth(table: Seq[(Int, Int)]): Int = {
    var max = 0
    table.foreach(value => if (value._1 > max) max = value._1)
    max
  }
}

/* Uncomment to test Huffman object testHuf extends App { val table = HuffmanTable.fixedLiteralTable()
 *
 * def bitsToString(bits: Int, size: Int): String = { val result = new StringBuilder() if(size > 0) {
 * result.append(bits.toBinaryString) while(result.size < size) { result.insert(0, "0") } result.toString() } else { ""
 * }
 *
 * }
 *
 * for((elem, index) <- table.zipWithIndex) { println(s"$index (" + index.toChar + ") : " +
 * bitsToString(BitReverse(elem._2, elem._1), elem._1)) } } */
