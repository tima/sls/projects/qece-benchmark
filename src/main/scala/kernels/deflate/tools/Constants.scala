/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.tools

object Constants {

  // BTYPE codes
  val BTYPE_NO_COMPRESSION = 0
  val BTYPE_FIXED_HUFFMAN  = 1
  val BTYPE_DYN_HUFFMAN    = 2

  val EOB = 256 // End of block code

  // Code and extra bits widths
  val LITERAL_CODE_WIDTH        = 9
  val LITERAL_EXTRA_BITS_WIDTH  = 5
  val DISTANCE_CODE_WIDTH       = 5
  val DISTANCE_EXTRA_BITS_WIDTH = 15

  // Table lengths
  val LITERAL_TABLE_LENGTH  = 288
  val DISTANCE_TABLE_LENGTH = 30

  // Boundaries
  val LENGTH_BOUNDARIES        = Seq(3, 11, 19, 35, 67, 131, 258)
  val CODE_LENGTH_BOUNDARIES   = Seq(257, 265, 269, 273, 277, 281)
  val DISTANCE_BOUNDARIES      = Seq(1, 5, 9, 17, 33, 65, 129, 257, 513, 1025, 2049, 4097, 8193, 16385, 32769)
  val CODE_DISTANCE_BOUNDARIES = Seq(0, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28)

}
