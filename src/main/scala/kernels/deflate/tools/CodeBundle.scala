/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.tools

import chisel3._
import chisel3.util.log2Ceil

/** Bundle that define a Deflate symbol (code followed eventually with extra bits)
 *  @param length
 *    bitwidth of the code
 *  @param maxExtraBits
 *    maximal bitwidth used to encode extra bits
 */
class CodeBundle(length: Int, maxExtraBits: Int) extends Bundle {
  val code               = UInt(length.W)
  val extraBits          = UInt(maxExtraBits.W)
  val extraBitsLength    = UInt(log2Ceil(maxExtraBits).W)
  override def cloneType = new CodeBundle(length, maxExtraBits).asInstanceOf[this.type]
}
