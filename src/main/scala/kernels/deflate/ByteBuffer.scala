/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate

import bench.utils.generation.Require
import bench.utils.hardware.Delay

import chisel3._
import chisel3.util.{Cat, PriorityEncoder, ShiftRegister, Valid, log2Ceil}
import dsptools.DspContext
import dsptools.numbers._

/** Used to aggregate bits into a multi byte aggregation
 *  @param nbBytes
 *    Output size (in byte)
 *  @param maxBitwidth
 *    maximal bitwidth of incoming data (must be smaller than the word size (in bits))
 */
class ByteBuffer(nbBytes: Int, maxBitwidth: Int) extends Module {
  val bufferSize = nbBytes * 8
  val delay      = Delay.getAddDelay(UInt(log2Ceil(bufferSize).W))
  require(delay >= 1, "The addition must be pipelined.")
  require(Require.isPow2(bufferSize), "The buffer size muse be a power of 2")
  require(bufferSize >= maxBitwidth, "The buffer size must be bigger than the max word size")

  val io = IO(new Bundle() {
    val bytes    = Output(Valid(UInt(bufferSize.W)))
    val word     = Input(Valid(UInt(maxBitwidth.W)))
    val size     = Input(UInt(log2Ceil(maxBitwidth).W))
    val nextByte = Input(Bool()) // Used to pad to the next byte (if the byte boundary is not reached)
  })

  val getPipelineLevel: Int = {
    delay
  }

  val buffer = RegInit(UInt(bufferSize.W), 0.U)

  val index     = WireInit(UInt(log2Ceil(bufferSize).W), 0.U)
  val nextIndex = Wire(UInt((index.getWidth + 1).W))

  val delayedIndex    = ShiftRegister(index, delay, 0.U, io.word.valid)
  val delayedWord     = ShiftRegister(io.word.bits, delay, io.word.valid)
  val delayedSize     = ShiftRegister(io.size, delay, io.word.valid)
  val delayedNextByte = ShiftRegister(io.nextByte, delay, false.B, true.B)
  val nextByte        = (PriorityEncoder((0 until nbBytes).map(i => delayedIndex <= (i * nbBytes).U)) << log2Ceil(8)).asUInt()
  val output          = WireInit(UInt(bufferSize.W), 0.U)
  io.bytes.valid := false.B

  DspContext.withNumAddPipes(delay) {

    // Compute the next index according to the incoming word size and the byte - padding signal
    nextIndex := Mux(delayedNextByte, nextByte, index context_+ Mux(io.word.valid, io.size, delayedSize))

    when(io.word.valid) {
      when(nextIndex >= bufferSize.U) { // Overflow detected
        val delta = bufferSize.S context_- Cat(0.U, index).asSInt()
        buffer := (delayedWord >> delta.asUInt()).asUInt()
        output := buffer | (delayedWord << delayedIndex).asUInt()
        io.bytes.valid := true.B
      } otherwise { // Bits concatenation
        buffer := buffer | (delayedWord << delayedIndex).asUInt()
        output := buffer
      }
    } otherwise {
      output := buffer
    }

    index := Mux(io.word.valid, nextIndex, delayedIndex)
  }

  // Reverse byte order to get LSByte -> MSByte order
  io.bytes.bits := VecInit(output.asTypeOf(Vec(nbBytes, UInt(8.W))).reverse).asUInt()
}
