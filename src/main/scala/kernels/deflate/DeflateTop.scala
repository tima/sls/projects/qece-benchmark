/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate

import bench.utils.components.Role

import bench.kernels.deflate.huffman.HuffmanEncoder
import bench.kernels.deflate.lz77.LZ77
import bench.kernels.deflate.preencoder.PreEncoder
import bench.kernels.deflate.tools.Constants

import chisel3._
import chisel3.util._

/** Compress input data in Deflate fashion (RFC1951 : https://dl.acm.org/doi/pdf/10.17487/RFC1951) WARNING : As the
 *  output is an aggregation of byte, we must distinct the over-padding (completion of the word) of the regular padding
 *  (byte-padding). To deal with this particularity, we add a 0xFF byte at the end of the byte-padding. It's not a
 *  feature described in the RFC1951 but as the decoder must stop at the EOB (end of block), it might not be an issue to
 *  decode if not removed. BUT if the deflate data is wrapped in another format (like GZIP) the software MUST remove
 *  over padding (including the 0xFF byte) to be compliant.
 *
 *  @param bitWidth
 *    bitwidth used for input on both FIFOs
 *  @param lz77Depth
 *    lz77 dictionary depth (see LZ77 module to know more)
 *  @param literalHuffCodes
 *    Huffman table use to encode literals
 *  @param distanceHuffCodes
 *    Huffman table use to encode distance
 */
class DeflateRole(
    bitWidth: Int,
    lz77Depth: Int,
    literalHuffCodes: Seq[(Int, Int)],
    distanceHuffCodes: Seq[(Int, Int)]
) extends Role(bitWidth) {
  val idle :: writeBFinal :: writeBType :: running :: flushingLZ77 :: flushingPipeline :: writeEOB :: padding :: endOfPadding :: flushingBuffer :: ending :: Nil =
    Enum(11)

  val lz77           = Module(new LZ77(lz77Depth))
  val preEncoder     = Module(new PreEncoder())
  val huffmanEncoder = Module(new HuffmanEncoder(literalHuffCodes, distanceHuffCodes))
  val byteBuffer     = Module(new ByteBuffer(bitWidth / 8, huffmanEncoder.outputWidth))

  val flushDelay = preEncoder.getPipelineLevel + huffmanEncoder.getPipelineLevel - 1
  val stateReg   = RegInit(idle)
  val counter    = RegInit(UInt(log2Ceil(flushDelay).W), 0.U)

  io.in.ready := false.B

  val flush = io.in.bits(bitWidth - 1) // Use the MSB to indicate that it's the last byte

  lz77.io.inputChar.bits := io.in.bits(7, 0) // LZ77 take only 1 byte per cycle. (TODO: parallelize LZ77)
  lz77.io.inputChar.valid := io.in.valid
  lz77.io.flush := false.B

  preEncoder.io.position := lz77.io.position
  preEncoder.io.length := lz77.io.length
  preEncoder.io.char := lz77.io.char
  preEncoder.io.inValid := lz77.io.valid

  huffmanEncoder.io.literalCode := preEncoder.io.literalCode
  huffmanEncoder.io.distanceCode := preEncoder.io.distanceCode
  huffmanEncoder.io.inValid := preEncoder.io.outValid

  byteBuffer.io.word := huffmanEncoder.io.output
  byteBuffer.io.size := huffmanEncoder.io.size
  byteBuffer.io.nextByte := false.B

  io.out.bits := byteBuffer.io.bytes.bits
  io.out.valid := byteBuffer.io.bytes.valid

  switch(stateReg) {
    // ============ Idle state ============
    // This state is essential to guaranty a proper cold-start.
    is(idle) { // Jump immediately to the next state to write deflate headers
      stateReg := writeBFinal
    }
    // ============ Header writing states ============
    is(writeBFinal) {                  // Write BFinal bit
      byteBuffer.io.word.bits := 0x1.U // Only 1 block is generated (TODO : create multiple blocks)
      byteBuffer.io.word.valid := true.B
      byteBuffer.io.size := 1.U
      stateReg := writeBType
    }
    is(writeBType) {                                             // Write BType bit
      byteBuffer.io.word.bits := Constants.BTYPE_FIXED_HUFFMAN.U // Only fixed huffman is handled (TODO : 1. save huffman tree 2. handle dyn huffman)
      byteBuffer.io.word.valid := true.B
      byteBuffer.io.size := 2.U
      stateReg := running
    }
    // ============ Running state ============
    is(running) {
      io.in.ready := true.B
      when(flush) {
        stateReg := flushingLZ77
      }
    }
    // ============ Flushing states ============
    is(flushingLZ77) { // Flush LZ77 to get all bytes
      lz77.io.flush := true.B
      when(lz77.io.flushed) {
        counter := 0.U
        stateReg := flushingPipeline
      }
    }
    is(flushingPipeline) { // Waiting the last byte to pass the pipeline
      lz77.io.flush := true.B
      when(counter === (flushDelay - 1).U) {
        stateReg := writeEOB
      } otherwise {
        counter := counter + 1.U
      }
    }
    is(writeEOB) { // Write End of Block (deflate footer)
      lz77.io.flush := true.B
      byteBuffer.io.word.bits := literalHuffCodes(Constants.EOB)._2.U
      byteBuffer.io.word.valid := true.B
      byteBuffer.io.size := literalHuffCodes(Constants.EOB)._1.U
      stateReg := padding
    }
    // ============ Padding state ============
    is(padding) { // Simple byte padding
      lz77.io.flush := true.B
      byteBuffer.io.nextByte := true.B
      byteBuffer.io.word.bits := 0.U
      byteBuffer.io.word.valid := true.B
      byteBuffer.io.size := 0.U
      stateReg := endOfPadding
    }
    // ============ Over padding states ============
    // Note : Over padding is due to ByteBuffer's size which is bigger than 1 byte.
    // The over-padding must be delete for other purposes than deflate (for instance
    // wrapping in GZIP format). To differentiate the padding (byte padding) with the
    // excess of padding (word padding), we use a magic byte 0xFF.
    is(endOfPadding) { // Mark the beginning of over-padding with 0xFF
      lz77.io.flush := true.B
      byteBuffer.io.word.bits := 0xff.U
      byteBuffer.io.word.valid := true.B
      byteBuffer.io.size := 8.U
      stateReg := flushingBuffer
    }
    is(flushingBuffer) { // Over-padding with 0x00 to fill the buffer until it completes the word
      lz77.io.flush := true.B
      byteBuffer.io.word.bits := 0x00.U
      byteBuffer.io.word.valid := true.B
      byteBuffer.io.size := 8.U
      when(byteBuffer.io.bytes.valid) {
        stateReg := ending
      }
    }
    // ============ End state ============
    // Todo : recover from this state
    is(ending) {
      lz77.io.flush := true.B
    }
  }
}
