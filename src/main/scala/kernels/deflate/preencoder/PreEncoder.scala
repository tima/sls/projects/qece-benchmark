/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.preencoder

import bench.utils.hardware.Delay
import bench.kernels.deflate.tools.{CodeBundle, Constants}

import chisel3._
import chisel3.util.{Cat, PriorityEncoder, ShiftRegister}
import dsptools.DspContext
import dsptools.numbers._

/** Pre-encoder is used to convert LZ77 information (char, distance, length) into Deflate code (cf section 3.2.5 of the
 *  RFC1951) This component must be between the gateway between LZ77 module and the Huffman encoder
 *  @param symbolBitWidth
 *    = Max(position bitwidth, length bitwidth) by default 32 bit is the max size (due to DEFLATE limitations)
 */
class PreEncoder(symbolBitWidth: Int = 32) extends Module {
  // https://dl.acm.org/doi/pdf/10.17487/RFC1951
  // page 12 - 13

  val io = IO(new Bundle {
    val position = Input(UInt(symbolBitWidth.W))
    val length   = Input(UInt(symbolBitWidth.W))
    val char     = Input(UInt(8.W))
    val inValid  = Input(Bool())

    val literalCode =
      Output(new CodeBundle(Constants.LITERAL_CODE_WIDTH, Constants.LITERAL_EXTRA_BITS_WIDTH)) // from 0 -> 285
    val distanceCode =
      Output(new CodeBundle(Constants.DISTANCE_CODE_WIDTH, Constants.DISTANCE_EXTRA_BITS_WIDTH)) // from 0 -> 29
    val outValid = Output(Bool())
  })

  val addDelay = Delay.getAddDelay(UInt(symbolBitWidth.W))

  def getPipelineLevel: Int = {
    2 * addDelay
  }

  // Length and distance codes cf RFC1951 section 3.2.5
  val lengthBoundaries     = VecInit(Constants.LENGTH_BOUNDARIES.map(value => value.U))
  val codeLengthBoundaries = VecInit(Constants.CODE_LENGTH_BOUNDARIES.map(value => value.U))

  val distanceBoundaries     = VecInit(Constants.DISTANCE_BOUNDARIES.map(value => value.U))
  val codeDistanceBoundaries = VecInit(Constants.CODE_DISTANCE_BOUNDARIES.map(value => value.U))

  val lengthExtraBits = PriorityEncoder(
      (1 until lengthBoundaries.size).map(index => io.length < lengthBoundaries(index))
  )
  val distanceExtraBits = PriorityEncoder(
      (1 until distanceBoundaries.size).map(index => io.position < distanceBoundaries(index))
  )

  DspContext.withNumAddPipes(addDelay) {
    when(ShiftRegister(io.length < lengthBoundaries(0), getPipelineLevel)) {
      // Character
      io.literalCode.code := ShiftRegister(io.char, getPipelineLevel)
      io.literalCode.extraBits := 0.U
      io.literalCode.extraBitsLength := 0.U
      io.distanceCode.code := 0.U
      io.distanceCode.extraBits := 0.U
      io.distanceCode.extraBitsLength := 0.U
    } otherwise {
      // Pointer to a word
      // Length
      val delayedLengthExtraBits = ShiftRegister(lengthExtraBits, addDelay)
      val deltaLength            = Cat(0.U, io.length).asSInt() context_- Cat(0.U, lengthBoundaries(lengthExtraBits)).asSInt()
      io.literalCode.extraBits := (ShiftRegister(deltaLength, addDelay) & (Cat(0.U, 1.U << delayedLengthExtraBits)
        .asSInt() context_- 1.S)).asUInt() // modulus a power of 2
      io.literalCode.code := codeLengthBoundaries(
          delayedLengthExtraBits
      ) context_+ (deltaLength >> delayedLengthExtraBits).asUInt()
      io.literalCode.extraBitsLength := ShiftRegister(lengthExtraBits, 2 * addDelay)

      // Distance
      val delayedDistanceExtraBits = ShiftRegister(distanceExtraBits, addDelay)
      val deltaDistance =
        Cat(0.U, io.position).asSInt() context_- Cat(0.U, distanceBoundaries(distanceExtraBits)).asSInt()
      io.distanceCode.extraBits := (ShiftRegister(deltaDistance, addDelay) & (Cat(0.U, 1.U << delayedDistanceExtraBits)
        .asSInt() context_- 1.S)).asUInt()
      io.distanceCode.code := codeDistanceBoundaries(
          delayedDistanceExtraBits
      ) context_+ (deltaDistance >> delayedDistanceExtraBits).asUInt()
      io.distanceCode.extraBitsLength := ShiftRegister(distanceExtraBits, 2 * addDelay)
    }
  }

  io.outValid := ShiftRegister(io.inValid, getPipelineLevel)

}
