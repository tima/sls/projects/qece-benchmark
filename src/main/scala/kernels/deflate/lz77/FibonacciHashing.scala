/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.lz77

import bench.utils.generation.Require
import bench.utils.hardware.Delay

import dsptools.DspContext
import dsptools.numbers._
import chisel3._
import chisel3.util.{Cat, log2Ceil}

/** Fibonacci hashing function (NOTE: it's the 32 bit version)
 *  @param dictDepth
 */
class FibonacciHashing(dictDepth: Int) extends Module {
  require(Require.isPow2(dictDepth), "The dictionary depth must be a power of 2")

  val io = IO(new Bundle() {
    val data = Input(Vec(3, UInt(8.W)))
    val hash = Output(UInt(log2Ceil(dictDepth).W))
  })

  private val factor   = WireInit(UInt(32.W), 2654435769L.U) // = 2^32 / golden_ratio
  private val shift    = 32 - log2Ceil(dictDepth)
  private val elemType = UInt(32.W)

  def getPipelineLevel(): Int = {
    Delay.getMultDelay(elemType)
  }

  val concatenated = WireInit(elemType, Cat(io.data))

  DspContext.withNumMulPipes(Delay.getMultDelay(elemType)) {
    io.hash := ((factor context_* concatenated) >> shift).asUInt()
  }
}
