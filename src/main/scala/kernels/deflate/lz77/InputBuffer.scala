/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.lz77

import chisel3._
import chisel3.util.{RegEnable, ShiftRegister, Valid}

/** Input buffer of the LZ77 module : Take byte from input to generate a sliding window of 3 characters
 *  @param addrBitWidth
 *    Bitwidth used to index characters (must be greater than the byte size of the text to encode)
 */
class InputBuffer(addrBitWidth: Int) extends Module {

  val io = IO(new Bundle() {
    val inputChar  = Input(Valid(UInt(8.W)))
    val outputChar = Output(Valid(Vec(3, UInt(8.W))))
    val addr       = Output(UInt(addrBitWidth.W))
  })

  def generateBuffer(depth: Int, input: UInt, enable: Bool): Array[UInt] = {
    val array = new Array[UInt](depth)
    array(0) = RegEnable(input, 0.U, enable)
    for (i <- 1 until depth) {
      array(i) = RegEnable(array(i - 1), 0.U, enable)
    }
    array
  }

  val buffer  = generateBuffer(3, io.inputChar.bits, io.inputChar.valid)
  val addrReg = RegInit(UInt(addrBitWidth.W), 0.U)
  val valid   = ShiftRegister(io.inputChar.valid, 3, false.B, io.inputChar.valid)

  when(valid) {
    addrReg := addrReg + 1.U
  }

  io.outputChar.bits.zip(buffer).foreach { case (a, b) => a := b }
  io.outputChar.valid := valid
  io.addr := addrReg
}
