/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.lz77

import bench.utils.hardware.Delay

import chisel3._
import chisel3.util._
import dsptools.DspContext
import dsptools.numbers._

/** LZ77 implementation using a hash table (fibonacci hashing function) to detect pattern.
 *  @param dictDepth
 *    Depth of the hash table (increase the size will allow to match longer patterns and much farther away)
 *  @param addrBitWidth
 *    Bitwidth used to index characters (must be greater than the byte size of the text to encode)
 */
class LZ77(dictDepth: Int, addrBitWidth: Int = 32) extends Module {
  // Todo : make a parallelized version
  // Todo : freeze the module when the input is not valid.

  val io = IO(new Bundle() {
    val inputChar = Input(Valid(UInt(8.W)))

    val position = Output(UInt(addrBitWidth.W))
    val length   = Output(UInt(addrBitWidth.W))
    val char     = Output(UInt(8.W))

    val valid   = Output(Bool())
    val flush   = Input(Bool())
    val flushed = Output(Bool())
  })

  val addDelay = Delay.getAddDelay(UInt(addrBitWidth.W))
  val initializing :: noMatch :: matching :: endOfMatch :: flushChar0 :: flushChar1 :: flushChar2 :: flushEnded :: Nil =
    Enum(8)

  val inputBuffer = Module(new InputBuffer(addrBitWidth))
  val hashTable   = Module(new HashTable(dictDepth, addrBitWidth))

  val delayedChars = ShiftRegister(inputBuffer.io.outputChar.bits, hashTable.getPipelineLevel())
  val delayedAddr  = ShiftRegister(inputBuffer.io.addr, hashTable.getPipelineLevel())
  val delayedInit  = ShiftRegister(true.B, hashTable.getPipelineLevel(), false.B, inputBuffer.io.outputChar.valid)
  val delayedFlush = ShiftRegister(io.flush, hashTable.getPipelineLevel(), false.B, true.B)

  val stateReg    = RegInit(initializing)
  val lengthReg   = RegInit(UInt(addrBitWidth.W), 0.U)
  val positionReg = RegInit(UInt(addrBitWidth.W), 0.U)

  inputBuffer.io.inputChar := io.inputChar
  hashTable.io.enable := inputBuffer.io.outputChar.valid
  hashTable.io.dataIn := inputBuffer.io.outputChar.bits
  hashTable.io.addrIn := inputBuffer.io.addr

  io.length := 0.U
  io.position := 0.U
  io.valid := false.B
  io.flushed := false.B
  io.char := ShiftRegister(delayedChars(2), addDelay)

  DspContext.withNumAddPipes(addDelay) {
    // Compute the relative position
    val delta = Cat(0.U, delayedAddr).asSInt() context_- Cat(0.U, hashTable.io.addrOut).asSInt()

    // Verify the continuity of the word
    val matchPosition = WireInit(Bool(), true.B)
    when(stateReg =!= noMatch) {
      matchPosition := delta.asTypeOf(UInt(addrBitWidth.W)) === positionReg
    }

    // Is a correct match (good pattern and good offset) ?
    val matched = (ShiftRegister(delayedChars.asUInt() === hashTable.io.dataOut.asUInt(), addDelay, false.B, true.B)
      && delta > 0.S
      && matchPosition) // Todo : a better offset condition

    switch(stateReg) {
      // ============= Initialization =============
      is(initializing) { //  waiting until the hash table is ready.
        io.valid := false.B
        when(delayedInit) {
          stateReg := noMatch
        }
      }
      // ============= Pattern matching =============
      is(noMatch) { // No pattern matched : "return" the current char
        when(delayedFlush) {
          io.valid := true.B
          stateReg := flushChar0
        } otherwise {
          when(matched) {
            lengthReg := 3.U
            positionReg := delta.asTypeOf(UInt(32.W))
            stateReg := matching
            io.valid := false.B
          } otherwise {
            io.valid := true.B
          }
        }
      }
      is(matching) { // Pattern matched : wait for a potential bigger pattern.
        io.valid := false.B
        when(delayedFlush) {
          stateReg := endOfMatch
        } otherwise {
          when(matched) {
            lengthReg := lengthReg + 1.U
          } otherwise {
            stateReg := endOfMatch
          }
        }
      }
      is(endOfMatch) { // End of matching : "return" position and length
        io.length := lengthReg
        io.position := positionReg
        io.valid := true.B
        when(delayedFlush) {
          stateReg := flushChar0
        } otherwise {
          stateReg := noMatch
        }
      }
      // ============= Flushing procedure =============
      is(flushChar0) { // Outputs char 0
        io.valid := true.B
        stateReg := flushChar1
        io.char := inputBuffer.io.outputChar.bits(2)
      }
      is(flushChar1) { // Outputs char 1
        io.valid := true.B
        stateReg := flushChar2
        io.char := inputBuffer.io.outputChar.bits(1)
      }
      is(flushChar2) { // Outputs char 2
        io.valid := true.B
        stateReg := flushEnded
        io.char := inputBuffer.io.outputChar.bits(0)
      }
      is(flushEnded) { // Wait until flush signal is released
        io.valid := false.B
        io.flushed := true.B
        when(io.flush === false.B) {
          stateReg := noMatch
        }
      }
    }

  }
}
