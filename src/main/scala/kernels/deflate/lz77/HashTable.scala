/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.lz77

import bench.utils.generation.Require

import chisel3._
import chisel3.util.{Cat, ShiftRegister}

/** Hash table using Fibonacci hashing function. Each search will replace the old result by the new one (swap data) It
 *  stores 2 values : address and data. The hash is computed using only the data field.
 *  @param depth
 *    size of the dictionary. Note as the Fibonacci function is 32 bit version, it's useless to have a size bigger than
 *    32 bits.
 *  @param addrBitWidth
 *    size of the address. Note : data is 3 bytes long
 */
class HashTable(depth: Int, addrBitWidth: Int) extends Module {
  require(Require.isPow2(depth), "The hash table depth must be a power of 2")

  val io = IO(new Bundle() {
    val enable  = Input(Bool()) // Enable or disable swap
    val addrIn  = Input(UInt(addrBitWidth.W))
    val dataIn  = Input(Vec(3, UInt(8.W)))
    val addrOut = Output(UInt(addrBitWidth.W))
    val dataOut = Output(Vec(3, UInt(8.W)))
  })

  val dataBitWidth = addrBitWidth + 3 * 8

  def getPipelineLevel(): Int = {
    hashFunction.getPipelineLevel() + 1
  }

  val hashFunction = Module(new FibonacciHashing(depth))
  val mem          = SyncReadMem(depth, UInt(dataBitWidth.W), SyncReadMem.ReadFirst) // Must READ before WRITE !

  val delayedData = ShiftRegister(Cat(io.addrIn, Cat(io.dataIn)), hashFunction.getPipelineLevel())

  val output = mem.read(hashFunction.io.hash).asTypeOf(UInt(dataBitWidth.W))
  when(io.enable) {
    mem.write(hashFunction.io.hash, delayedData)
  }

  hashFunction.io.data := io.dataIn
  io.addrOut := output(dataBitWidth - 1, 3 * 8).asUInt()
  io.dataOut := output(3 * 8 - 1, 0).asTypeOf(Vec(3, UInt(8.W))).reverse
}
