/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.generation.emitter

import bench.kernels.fir._

import bench.utils.generation.CustomEmitter
import bench.utils.hardware.{RingElem}

import chisel3._
import chisel3.experimental._

object FirEmitter extends CustomEmitter {
  val bitWidth  = 128
  val elemType  = RingElem(FixedPoint(32.W, 16.BP))
  val numberTap = 256
  val coeffs    = FirUtil.generate(numberTap, true)

  val static = true

  if (static) {
    emit(new FirGenRole(bitWidth, elemType, numberTap, Some(coeffs)), "fir")
  } else {
    emit(new FirGenRole(bitWidth, elemType, numberTap), "fir")
  }
}
