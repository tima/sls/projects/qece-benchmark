/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.generation.emitter

import bench.kernels.monteCarlo._
import bench.kernels.monteCarlo.optionpricing.EuropeanVanillaFactory

import bench.utils.components.random.TauswortheConfig
import bench.utils.generation.CustomEmitter
import bench.utils.hardware.{RingElem}

import chisel3._
import chisel3.experimental._

object MonteCarloEmitter extends CustomEmitter {
  val elemWidth   = 64
  val nbIteration = 512
  val nbCore      = 4

  val config = MonteCarloUtil.getEuropeanVanillaConfig(
      kind = "payoff",     // kind of core used
      s0 = 100.0,          // Option price
      strikePrice = 100.0, // Strike price
      volatility = 0.2,    // Volatility of the underlying (20%)
      riskFreeRate = 0.05, // Risk-free rate (5%)
      maturity = 1,        // One year until expiry
      n = 4                // Nb iterations of Euler-Maruyama
  )

  val elemType = RingElem(FixedPoint(elemWidth.W, (elemWidth / 2).BP))
  val monteCarloConfig = EuropeanVanillaFactory(
      elemType, // type of the data (must be a fixed point because of the LUT functions)
      //urngConfig iterator of TauswortheConfig (needed to setup random generator)
      TauswortheConfig.randomTupleIterator(123, elemWidth),
      config, // Black Scholes parameters
      256,    // BoxMuller size
      true    // use sharedROM for BoxMuller LUTs
  )

  emit(new MonteCarloRole(elemType, monteCarloConfig, nbIteration, nbCore), "monteCarlo")
}
