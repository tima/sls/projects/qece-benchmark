package bench.generation

import qece.QeceContext

package object explore {
  /** Used to specify the QECE configuration to be used in the project. */
  val baseConfig = QeceContext.copy(
    emission  = _.copy(basePath = "chisel_gen"),
    parallel  = _.copy(numThread = 8)
  )

  /** Synthesis specific configuration. */
  val synthesisConfig = baseConfig.funcCopy(
    /** Keep the number of synthesis low to avoid memory crashes. */
    parallel  = _.copy(numThread = 4),
    /** Target specific configuration. */
    backend   = _.copy(targetBoard = "Zedboard", targetPeriod = 10, vivadoVersion = "2017.3")
  )
}
