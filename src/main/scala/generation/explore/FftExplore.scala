/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.generation.explore

import bench.kernels.fft._

import bench.utils.hardware.RingElem
import bench.utils.components.Role

import chisel3._
import chisel3.experimental._
import dsptools.numbers._

import qece.exploration.strategies.StrategyBuilder
import qece.exploration.utils.WithExplorable
import qece.exploration.annotations._
import qece.estimation.transforms.TransformSeq

object ExplorationHelper {
  val size = 128
  // val size = 512
  // val size = 1024
  // val size = 2048
}

class FftWrapper(
    @pow2(1, 10) parallelism: Int,
    // @enum(32)     elemWidth: Int,
    @pow2(3, 6) elemWidth: Int,
    // @enum(ExplorationHelper.size)  size: Int
    @pow2(7, 11) size: Int
) extends Role(elemWidth * parallelism)
    with WithExplorable {
  val internal =
    Module(
        new FftRole(
            elemWidth * parallelism,
            RingElem(DspComplex(FixedPoint((elemWidth / 2).W, (elemWidth / 4).BP))),
            size
        )
    )

  io.in <> internal.io.in
  io.out <> internal.io.out
}

object FftTransforms {
  val tfs = TransformSeq.postElab(
      "area"       -> (m => Seq(m("%realLUT"), m("%realFF"), m("%realDSP"), m("%realMem")).max),
      "through"    -> (m => m("elemWidth") * m("parallelism") * m("realFreq")),
      "throughput" -> (m => if (m("area") > 1.0) 0.0 else m("through"))
  )
}

object FftSpace extends App {
  val builder = StrategyBuilder(baseConfig)
  val strat = builder.displaySpace[FftWrapper]
}

object FftEstimation extends App {
  val builder = StrategyBuilder(synthesisConfig).withPath("fftEstimation")
  val strat = builder.buildStrategy(
      builder.sort[FftWrapper](
          tfs = TransformSeq.resources ++ TransformSeq.synthesis ++ FftTransforms.tfs,
          func = _("throughput"),
          cmp = (_ > _)
      )
  )
  strat.explore[FftWrapper]
  strat.writeBack(s"csv/fftEstimation.csv")
}

object FftBaseline extends App {
  val builder = StrategyBuilder(synthesisConfig).withPath("fftBaseline")
  val strat = builder.buildStrategy(
      builder.sort[FftWrapper](
          tfs = TransformSeq.synthesis ++ FftTransforms.tfs,
          func = _("throughput"),
          cmp = (_ > _)
      )
  )
  strat.explore[FftWrapper]
  strat.writeBack(s"csv/fftBaseline${ExplorationHelper.size}.csv")
}

object FftExhaustive extends App {
  //reuse previous synthesis results to compare
  val builder = StrategyBuilder(synthesisConfig).withPath("fftBaseline")
  val strat = builder.buildStrategy(
      builder.sort[FftWrapper](
          tfs = TransformSeq.synthesis ++ TransformSeq.resources ++ FftTransforms.tfs,
          func = _("throughput"),
          cmp = (_ > _)
      )
  )
  strat.explore[FftWrapper]
  strat.writeBack(s"csv/fftExhaustive.csv")
}

object FftPruning extends App {
  val builder = StrategyBuilder(baseConfig).withPath("fftPruned")
  val strat = builder.buildStrategy(
      builder.compose(
          builder.prune[FftWrapper](
              tfs = TransformSeq.resources,
              func = m => m("%dsp") > 3.0 || m("%lut") > 6.0
          ),
          builder.sort[FftWrapper](
              tfs = TransformSeq.synthesis ++ FftTransforms.tfs,
              func = _("throughput"),
              cmp = (_ > _),
              numThread = synthesisConfig.parallel.numThread
          )
      )
  )
  strat.explore[FftWrapper]
  strat.writeBack(s"csv/fftPruned${ExplorationHelper.size}.csv")
}

object FftGradient extends App {
  val builder = StrategyBuilder(baseConfig).withPath("fftGradient")
  val strat = builder.buildStrategy(
      builder.compose(
          builder.sort[FftWrapper](
              tfs = TransformSeq.resources,
              func = _("lut"),
              cmp = (_ > _)
          ),
          builder.gradient[FftWrapper](
              tfs = TransformSeq.synthesis ++ FftTransforms.tfs,
              func = _("throughput"),
              cmp = (_ > _),
              numThread = synthesisConfig.parallel.numThread
          )
      )
  )
  strat.explore[FftWrapper]
  strat.writeBack(s"csv/fftGradient${ExplorationHelper.size}.csv")
}
