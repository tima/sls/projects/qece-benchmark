/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.generation.explore

import bench.kernels.monteCarlo._
import bench.kernels.monteCarlo.pi._

import bench.utils.components.random.TauswortheConfig
import bench.utils.hardware.RingElem
import bench.utils.components.Role

import qece.exploration.strategies.StrategyBuilder
import qece.exploration.utils.WithExplorable
import qece.exploration.annotations._
import qece.estimation.transforms.TransformSeq

import chisel3._
import chisel3.experimental._
import dsptools.numbers._

class PiWrapper(
    // @qualityOfResult @linear(32, 64)  elemWidth: Int,
    // @qualityOfResult @linear(16, 48)  binaryPoint: Int,
    // @qualityOfResult @pow2(8, 17)     nbIteration: Int,
    //                  @pow2(2, 10)     nbCore: Int
    @qualityOfResult @linear(2, 32) dynamic: Int,
    @qualityOfResult @linear(2, 32) precision: Int,
    @qualityOfResult @pow2(8, 9) nbIteration: Int,
    @pow2(2, 10) nbCore: Int
) extends Role(dynamic + precision)
    with WithExplorable {
  val elemWidth = dynamic + precision
  val elemType  = RingElem(FixedPoint(elemWidth.W, precision.BP))
  val piFactory = new PiFactory(elemType, TauswortheConfig.randomTupleIterator(123, elemWidth))

  val internal = Module(new MonteCarloRole(elemType, piFactory, nbIteration, nbCore))

  io.in <> internal.io.in
  io.out <> internal.io.out
}

object PiEstimation extends App {
  val builder = StrategyBuilder(synthesisConfig).withPath("piEstimation")
  val strat = builder.buildStrategy(
      builder.sort[PiWrapper](
          tfs = TransformSeq.resources ++ TransformSeq.synthesis ++ BlackScholesTransforms.tfs,
          func = _("%realLUT"),
          cmp = (_ > _)
      )
  )
  strat.explore[PiWrapper].head
  strat.writeBack("csv/piEstimation.csv")
}
