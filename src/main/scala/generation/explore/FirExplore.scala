/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.generation.explore

import bench.kernels.fir._

import bench.utils.hardware.RingElem
import bench.utils.components.Role

import chisel3._
import chisel3.experimental._
import dsptools.numbers._

import qece.exploration.strategies.StrategyBuilder
import qece.exploration.utils.WithExplorable
import qece.exploration.annotations._
import qece.estimation.transforms.TransformSeq

class FirWrapper(
    // @pow2(5, 10)  bitWidth: Int,
    @pow2(5) bitWidth: Int,
    // @enum(32)     elemWidth: Int,
    // @pow2(3, 6)     elemWidth: Int,
    @pow2(3) elemWidth: Int,
    // @pow2(5, 10)  tap: Int
    @pow2(5) tap: Int
) extends Role(bitWidth)
    with WithExplorable {
  val internal = Module(new FirGenRole(bitWidth, RingElem(FixedPoint(elemWidth.W, (elemWidth / 2).BP)), tap))
  // Uncomment for SInt usage
  // val internal = Module(new FirRole(bitWidth, RingElem(SInt(elemWidth.W)), tap))

  io.in <> internal.io.in
  io.out <> internal.io.out
}

object FirTransforms {
  val tfs = TransformSeq.postElab(
      "area"           -> (m => Seq(m("%realLUT"), m("%realFF"), m("%realDSP"), m("%realMem")).max),
      "through"        -> (m => m("bitWidth") * m("realFreq")),
      "throughput"     -> (m => if (m("area") > 1.0) 0.0 else m("through")),
      "elemThrough"    -> (m => m("realFreq") * m("bitWidth") / m("elemWidth")),
      "elemThroughput" -> (m => if (m("area") > 1.0) 0.0 else m("elemThrough"))
  )
}

object FirEstimation extends App {
  val builder = StrategyBuilder(synthesisConfig).withPath("firEstimation")
  val strat = builder.buildStrategy(
      builder.sort[FirWrapper](
          tfs = TransformSeq.resources ++ TransformSeq.synthesis ++ FirTransforms.tfs,
          func = _("%realLUT"),
          cmp = (_ > _)
      )
  )
  strat.explore[FirWrapper]
  strat.writeBack("csv/firEstimation.csv")
}
