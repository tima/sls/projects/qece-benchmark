/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.generation.explore

import bench.kernels.monteCarlo._
import bench.kernels.monteCarlo.optionpricing._

import bench.utils.components.random.TauswortheConfig
import bench.utils.hardware.RingElem
import bench.utils.components.Role

import qece._
import qece.exploration.strategies.StrategyBuilder
import qece.exploration.utils.{Constructor, WithExplorable}
import qece.exploration.annotations._

import qece.estimation.transforms.TransformSeq
import qece.estimation.qor.NormalDistribution

import chisel3._
import chisel3.experimental._
import dsptools.numbers._

object BlackScholesConfig {
  def getConfig(nEuler: Int) = 
    MonteCarloUtil.getEuropeanVanillaConfig(
      kind = "payoff",     // kind of core used
      s0 = 100.0,          // Option price
      strikePrice = 100.0, // Strike price
      volatility = 0.2,    // Volatility of the underlying (20%)
      riskFreeRate = 0.05, // Risk-free rate (5%)
      maturity = 1,        // One year until expiry
      n = nEuler           // Nb iterations of Euler-Maruyama
    )
}

class resource extends ImpactMetric("resource")

class BlackScholesWrapper(
    // @resource @qualityOfResult @enum(8, 18, 28)   dynamic: Int,
    // @resource @qualityOfResult @enum(8, 18, 28)   precision: Int,
    // @qualityOfResult @enum(32, 1024)            nbIteration: Int,
    // @qualityOfResult @enum(2, 8, 64)            nEuler: Int,
    // @resource @enum(4, 128, 1024)               nbCore: Int
    @resource @qualityOfResult @linear(8, 32)   dynamic: Int,
    @resource @qualityOfResult @linear(8, 32)   precision: Int,
    @qualityOfResult @pow2(5, 10)               nbIteration: Int,
    @qualityOfResult @pow2(1, 6)                nEuler: Int,
    @resource @pow2(2, 10)                      nbCore: Int
) extends Role(dynamic+precision) with WithExplorable {
  val config = BlackScholesConfig.getConfig(nEuler)
  val elemWidth = dynamic + precision
  val elemType = RingElem(FixedPoint(elemWidth.W, precision.BP))
  val monteCarloConfig = EuropeanVanillaFactory(
      elemType, // type of the data (must be a fixed point because of the LUT functions)
      //urngConfig iterator of TauswortheConfig (needed to setup random generator)
      TauswortheConfig.randomTupleIterator(123, elemWidth),
      config, // Black Scholes parameters
      256,    // BoxMuller size
      true    // use sharedROM for BoxMuller LUTs
  )

  val internal = Module(new MonteCarloRole(elemType, monteCarloConfig, nbIteration, nbCore))

  io.in <> internal.io.in
  io.out <> internal.io.out
}

object BlackScholesEstimation extends App {
  val builder = StrategyBuilder(synthesisConfig).withPath("blackScholesEstimation")
  val strat = builder.buildStrategy(
      builder.sort[BlackScholesWrapper](
          tfs = TransformSeq.resources ++ TransformSeq.synthesis ++ BlackScholesTransforms.tfs,
          func = _("%realLUT"),
          cmp = (_ > _)
      )
  )
  strat.explore[BlackScholesWrapper].head
  strat.writeBack("csv/blackScholesEstimation.csv")
}

object BlackScholesBaseline extends App {
  val builder = StrategyBuilder(synthesisConfig).withPath("blackScholesBaseline")
  val strat = builder.buildStrategy(
      builder.sort[BlackScholesWrapper](
          tfs = TransformSeq.synthesis ++ BlackScholesTransforms.tfs,
          func = _("realLUT"),
          cmp = (_ > _)
      )
  )
  strat.explore[BlackScholesWrapper].head
  strat.writeBack("csv/blackScholesBaseline.csv")
}

object BlackScholesExhaustive extends App {
  // reuse previous synthesis by using the same target directory
  val builder = StrategyBuilder(synthesisConfig).withPath("blackScholesBaseline")
  val strat = builder.buildStrategy(
      builder.sort[BlackScholesWrapper](
          tfs = TransformSeq.synthesis ++ TransformSeq.resources ++ BlackScholesTransforms.tfs,
          func = _("realLUT"),
          cmp = (_ > _)
      )
  )
  strat.explore[BlackScholesWrapper].head
  strat.writeBack("csv/blackScholesExhaustive.csv")
}

object BlackScholesQoR extends App {
  val builder = StrategyBuilder(baseConfig).withPath("blackScholesQoR")
  
  val c = new Constructor[BlackScholesWrapper]

  val nbSoftIter = 10000
  val nbSimu = 200
  val mean = 0
  val std = 1
  val workload = (new NormalDistribution(mean, std)).seq(nbSoftIter * c.getMax("nEuler"))

  val threshold = 0.05
  val baseDir = "csv/bs/qor/"
  val name = Seq(
    s"t$threshold",
    s"d${c.getMin("dynamic")}-${c.getMax("dynamic")}",
    s"p${c.getMin("precision")}-${c.getMax("precision")}",
    s"n${c.getMin("nbIteration")}-${c.getMax("nbIteration")}",
    s"e${c.getMin("nEuler")}-${c.getMax("nEuler")}",
    s"c${c.getMin("nbCore")}-${c.getMax("nbCore")}"
  ).mkString("")

  val spaceSize     = c.getSpaceSize(Some(new qualityOfResult))

  val exhaustive = builder.buildStrategy(
    builder.prune[BlackScholesWrapper](
      TransformSeq.simulation(m =>
          (
            c =>
              new BlackScholesQoRTest(
                m("dynamic").toInt,
                m("precision").toInt,                
                m("nbIteration").toInt,
                m("nbCore").toInt,
                BlackScholesConfig.getConfig(m("nEuler").toInt),
                nbSoftIter,
                nbSimu,
                workload
              )(c)
            )
          ),
        _.error > threshold,
        metric = Some(new qualityOfResult)
      )
    )

  val exStartTime = Utils.getTime
  exhaustive.explore[BlackScholesWrapper]
  val exEndTime = Utils.getTime
  val exTime    = exEndTime.getTime() - exStartTime.getTime()

  exhaustive.writeBack(s"$baseDir/$name/exhaustivePrune.csv")

  val quick = builder.buildStrategy(
    builder.quickPrune[BlackScholesWrapper](
      TransformSeq.simulation(m =>
          (
            c =>
              new BlackScholesQoRTest(
                m("dynamic").toInt,
                m("precision").toInt,                
                m("nbIteration").toInt,
                m("nbCore").toInt,
                BlackScholesConfig.getConfig(m("nEuler").toInt),
                nbSoftIter,
                nbSimu,
                workload
              )(c)
            )
          ),
        _.error > threshold,
        metric = Some(new qualityOfResult)
      )
    )

  val quStartTime = Utils.getTime
  quick.explore[BlackScholesWrapper]
  val quEndTime = Utils.getTime
  val quTime    = quEndTime.getTime() - quStartTime.getTime()
  val quSize = quick.getResults[BlackScholesWrapper].head.metrics("nbSimu").toInt

  quick.writeBack(s"$baseDir/$name/quickPrune.csv")

  // writing config to file
  val config = s"$baseDir/$name/config.csv"
  (s"name;nbSimu;exhaustiveTime;exhaustiveSize;quickPruningTime;quickPruningSize;mean;std\n").writeTo(config)
  (s"$name;$nbSimu;$exTime;$spaceSize;$quTime;$quSize;$mean;$std").appendTo(config)
}

object BlackScholesTransforms {
  val tfs = TransformSeq.postElab()
  val preElab     = TransformSeq.preElab("latency" -> (m => (m("nbIteration") * m("nEuler")) / m("nbCore")))
  val postSynth   = TransformSeq.postElab(
      "area"       -> (m => Seq(m("%realLUT"), m("%realFF"), m("%realDSP"), m("%realMem")).max),
      "through"    -> (m => m("realFreq") / m("latency")),
      "throughput" -> (m => if (m("area") > 1.0) 0.0 else m("through"))
  )
}


object BlackScholesFullExplo extends App {
  val builder = StrategyBuilder(baseConfig).withPath("blackScholesFull")

  val c = new Constructor[BlackScholesWrapper]

  val nbSoftIter = 10000
  val nbSimu = 200
  val mean = 0
  val std = 1
  val workload = (new NormalDistribution(mean, std)).seq(nbSoftIter * c.getMax("nEuler"))

  val threshold = 0.05
  val baseDir = "csv/bs/full/"
  val strat = builder.buildStrategy(
    builder.quickPrune[BlackScholesWrapper](
      TransformSeq.simulation(m =>
          (
            c =>
              new BlackScholesQoRTest(
                m("dynamic").toInt,
                m("precision").toInt,                
                m("nbIteration").toInt,
                m("nbCore").toInt,
                BlackScholesConfig.getConfig(m("nEuler").toInt),
                nbSoftIter,
                nbSimu,
                workload
              )(c)
            )
          ),
        _.error > threshold,
        metric = Some(new qualityOfResult)
      ),
    builder.reduceDimension[BlackScholesWrapper](new resource, true),
    builder.map[BlackScholesWrapper](BlackScholesTransforms.preElab),
    builder.sort[BlackScholesWrapper](
      TransformSeq.empty,
      m => m("dynamic") + m("precision") + m("nbCore"),
      (_ < _)
    ),
    builder.gradient[BlackScholesWrapper](
      TransformSeq.synthesis ++ BlackScholesTransforms.postSynth,
      func = _("throughput"),
      cmp = (_>_),
      numThread = synthesisConfig.parallel.numThread
    )
  )
  val startTime = Utils.getTime
  strat.explore[BlackScholesWrapper]
  val endTime = Utils.getTime
  val time    = endTime.getTime() - startTime.getTime()
  
  strat.writeBack(s"$baseDir/explo.csv")
  time.toString.writeTo(s"$baseDir/time.log")
}
