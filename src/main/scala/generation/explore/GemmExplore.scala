/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.generation.explore

import bench.kernels.gemm._

import bench.utils.hardware.RingElem
import bench.utils.components.Role

import qece._
import qece.exploration.strategies.StrategyBuilder
import qece.exploration.annotations._
import qece.exploration.utils.{Constructor, WithExplorable}
import qece.estimation.transforms.TransformSeq

import chisel3._
import dsptools.numbers._

import scala.math._

class GemmWrapper(
    // @pow2(5, 10)  bitWidth: Int,
    // @pow2(3, 6)   elemWidth: Int,
    // @pow2(4, 10)  dim: Int
    @pow2(5) bitWidth: Int,
    @pow2(3) elemWidth: Int,
    @pow2(4) dim: Int
) extends Role(bitWidth)
    with WithExplorable {
  val internal = Module(new GemmRole(bitWidth)(RingElem(UInt(elemWidth.W)), dim))

  io.in <> internal.io.in
  io.out <> internal.io.out
}

object GemmTransforms {
  val tfs = TransformSeq.postElab(
      "latency"    -> (m => 3 * pow(m("dim"), 2) * m("elemWidth") / m("bitWidth")),
      "area"       -> (m => Seq(m("%realLUT"), m("%realFF"), m("%realDSP"), m("%realMem")).max),
      "through"    -> (m => 2 * pow(m("dim"), 3) * m("realFreq") / m("latency")),
      "throughput" -> (m => if (m("area") > 1.0) 0.0 else m("through")),
      "efficiency" -> (m => m("throughput") / m("area"))
  )
}

/** Compare estimation and synthesis on a large set of implementations. */
object GemmEstimation extends App {
  val builder = StrategyBuilder(synthesisConfig).withPath("gemmEstimation")
  val strat = builder.buildStrategy(
      builder.sort[GemmWrapper](
          tfs = TransformSeq.synthesis ++ TransformSeq.resources ++ GemmTransforms.tfs,
          func = _("throughput"),
          cmp = (_ > _)
      )
  )
  strat.explore[GemmWrapper]
  strat.writeBack("csv/gemmEstimation.csv")
}

/** Baseline for experiments, only run synthesis on toplevels. */
object GemmBaseline extends App {
  val builder = StrategyBuilder(synthesisConfig).withPath("gemmBaseline32")
  val strat = builder.buildStrategy(
      builder.sort[GemmWrapper](
          tfs = TransformSeq.synthesis ++ GemmTransforms.tfs,
          func = _("throughput"),
          cmp = (_ > _)
      )
  )
  strat.explore[GemmWrapper]
  strat.writeBack("csv/gemmBaseline32.csv")
}

/** Experiments for estimator validation. */
object GemmExhaustive extends App {
  val builder = StrategyBuilder(synthesisConfig).withPath("gemmExhaustive")
  val strat = builder.buildStrategy(
      builder.sort[GemmWrapper](
          tfs = TransformSeq.resources ++ TransformSeq.synthesis ++ GemmTransforms.tfs,
          func = _("throughput"),
          cmp = (_ > _)
      )
  )
  strat.explore[GemmWrapper]
  strat.writeBack("csv/gemmExhaustive.csv")
}

/** Experiment on pruning strategy */
object GemmPruning extends App {
  val builder = StrategyBuilder(baseConfig).withPath("gemmPruned")
  val strat = builder.buildStrategy(
      builder.compose(
          builder.prune[GemmWrapper](
              tfs = TransformSeq.resources,
              func = m => m("%dsp") > 1.0 || m("%lut") > 2.0
          ),
          builder.sort[GemmWrapper](
              tfs = TransformSeq.synthesis ++ GemmTransforms.tfs,
              func = _("throughput"),
              cmp = (_ > _),
              numThread = synthesisConfig.parallel.numThread
          )
      )
  )
  strat.explore[GemmWrapper]
  strat.writeBack("csv/gemmPruned.csv")
}

/** Experiment on gradient strategy */
object GemmGradient extends App {
  val builder = StrategyBuilder(baseConfig).withPath("gemmGradient")
  val strat = builder.buildStrategy(
      builder.compose(
          builder.prune[GemmWrapper](
              tfs = TransformSeq.resources,
              func = m => m("%dsp") > 1.0 || m("%lut") > 2.0
          ),
          builder.sort[GemmWrapper](
              tfs = TransformSeq.empty,
              func = _("lut"),
              cmp = (_ > _)
          ),
          builder.gradient[GemmWrapper](
              tfs = TransformSeq.synthesis ++ GemmTransforms.tfs,
              func = _("throughput"),
              cmp = (_ > _),
              numThread = synthesisConfig.parallel.numThread
          )
      )
  )
  strat.explore[GemmWrapper]
  strat.writeBack("csv/gemmGradient.csv")
}

object GemmTiming extends App {
  val c = new Constructor[GemmWrapper]
  val builder = StrategyBuilder(synthesisConfig).withPath("gemmTiming")
  val params = Seq(
      s"b${c.getMin("bitWidth")}-${c.getMax("bitWidth")}",
      s"e${c.getMin("elemWidth")}-${c.getMax("elemWidth")}",
      s"d${c.getMin("dim")}-${c.getMax("dim")}",
      s"sTimeout${synthesisConfig.timeout.synthesis}",
      s"mTimeout${synthesisConfig.timeout.replaceMacro}",
      s"eTimeout${synthesisConfig.timeout.endpointReport}"
  ).mkString("", "_", "")
  val baseDir = s"csv/gemm/timing/$params"
  val info    = s"$baseDir/gemmTimingInfo.csv"

  val initialSize = c.getSpaceSize()

  val synthesis = builder.buildStrategy(
      builder.sort[GemmWrapper](
          tfs = TransformSeq.synthesis,
          func = _("realFreq"),
          cmp = (_ > _)
      )
  )

  val estimation = builder.buildStrategy(
      builder.sort[GemmWrapper](
          tfs = TransformSeq.synthesis ++ TransformSeq.timing,
          func = _("realFreq"),
          cmp = (_ > _)
      )
  )

  val synthStartTime = Utils.getTime
  synthesis.explore[GemmWrapper]
  val synthEndTime = Utils.getTime
  val synthTime    = synthEndTime.getTime() - synthStartTime.getTime()

  val estimationStartTime = Utils.getTime
  estimation.explore[GemmWrapper]
  val estimationEndTime = Utils.getTime
  val estimationTime    = estimationEndTime.getTime() - estimationStartTime.getTime()

  synthesis.writeBack(s"$baseDir/synthesis.csv")
  val synthesisComplete = synthesis.getResults[GemmWrapper].size

  estimation.writeBack(s"$baseDir/estimation.csv")
  val estimationComplete = estimation.getResults[GemmWrapper].size

  ("params;numThread;sTimeout;mTimeout;eTimeout;spaceSize;synthTime;synthComplete;estimationTime;estimationComplete\n" +
    s"$params;${synthesisConfig.parallel.numThread};${synthesisConfig.timeout.synthesis};"+
    s"${synthesisConfig.timeout.replaceMacro};${synthesisConfig.timeout.endpointReport};" +
    s"$initialSize;$synthTime;$synthesisComplete;$estimationTime;$estimationComplete").writeTo(info)
}
