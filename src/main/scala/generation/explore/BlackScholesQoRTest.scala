/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.generation.explore

import bench.kernels.monteCarlo.optionpricing._
import bench.utils.software.BitVector

import qece.estimation.qor.ErrorFeaturedPeekPokeTester
import qece.Utils

import scala.collection.mutable.ArrayBuffer

import chisel3._
import chisel3.experimental._

/* Testing the design */
class BlackScholesQoRTest(
    dynamic: Int,
    precision: Int,
    nbIter: Int,
    nbCore: Int,
    config: EuropeanVanillaConfig,
    nbSoftIterations: Int,
    nbSimu: Int,
    workload: Seq[Double]
)(mut: BlackScholesWrapper) extends ErrorFeaturedPeekPokeTester(mut) {
  val elemWidth = dynamic + precision
  implicit val signal = FixedPoint(elemWidth.W, precision.BP)

  Console.println(s"[${Utils.getTime}] --- Running $nbSimu simulations of ${this.getClass.getSimpleName} "+
                  s"with d$dynamic-p$precision ($nbIter iterations, ${config.N} euler) on $nbCore core(s)")

  val sw        = 
    new bench.kernels.monteCarlo.software.EuropeanOptionPricing(config, nbSoftIterations, workload=workload)
  val hwResults = ArrayBuffer[Double]()

  def waitForValue(): Unit = {
    while (peek(mut.io.out.valid) == 0) {
      step(1)
    }
  }

  for (_ <- 0 until nbSimu) {
    waitForValue()
    val hwValue = BitVector.deserialize(peek(mut.io.out.bits), elemWidth, elemWidth)
    hwResults.append(hwValue(0).toDouble)

    step(1)
  }

  // compute oracle value, using a large number of soft iterations
  val swResult = sw.monteCarlo()

  setNormalizedRootMeanSquareError(hwResults.toArray, Array.tabulate(hwResults.size)(_ => swResult._1))

  Console.println(s"[${Utils.getTime}] --- Ended $nbSimu simulations of ${this.getClass.getSimpleName} "+
                  s"with d$dynamic-p$precision ($nbIter iterations, ${config.N} euler) on $nbCore core(s)")
}
