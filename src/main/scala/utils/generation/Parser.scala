/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.generation

import bench.utils.hardware.RingElem

import collection.mutable.Map

import chisel3._
import chisel3.experimental._
import dsptools.numbers._

/** Scala parser to build designs from command line. */
object Parser {

  /** Parse the arguments
   *
   *  @param args
   *    an Array of strings, formated as "key=value"
   *  @return
   *    a Map of arguments
   */
  def parseArgs(args: Array[String]): Map[String, String] = {
    val resultMap = Map[String, String]()
    val Pattern   = "(.+)=(.+)".r
    for (arg <- args) {
      arg match {
        case Pattern(name, entry) => resultMap(name) = entry
        case _                    => ;
      }
    }
    resultMap
  }

  /* Get element type name to build from type from it.
   * Need to explicitly create the underlying type after
   *
   * @param argumentMap args after parsing
   * @return the type of element used */
  def getElementTypeName(argumentMap: Map[String, String]): String = {
    if (!(argumentMap contains "type"))
      throw new IllegalArgumentException("'type' key was not found in arguments")
    if (!(argumentMap contains "elementWidth"))
      throw new IllegalArgumentException("'elementWidth' key was not found in arguments")
    argumentMap("type")
  }

  /** Explicit builders are needed for instanciation */
  /** Explicitly build UInt type. */
  def buildUInt(argumentMap: Map[String, String]): RingElem[UInt] = {
    RingElem(UInt(argumentMap("elementWidth").toInt.W))
  }

  /** Explicitly build DspComplex[UInt] type. */
  def buildComplexUInt(argumentMap: Map[String, String]): RingElem[DspComplex[UInt]] = {
    RingElem(DspComplex(UInt(argumentMap("elementWidth").toInt.W)))
  }

  /** Explicitly build SInt type. */
  def buildSInt(argumentMap: Map[String, String]): RingElem[SInt] = {
    RingElem(SInt(argumentMap("elementWidth").toInt.W))
  }

  /** Explicitly build DspComplex[SInt] type. */
  def buildComplexSInt(argumentMap: Map[String, String]): RingElem[DspComplex[SInt]] = {
    RingElem(DspComplex(SInt(argumentMap("elementWidth").toInt.W)))
  }

  /** Explicitly build a FixedPoint type. */
  def buildFixedPoint(argumentMap: Map[String, String]): RingElem[FixedPoint] = {
    if (!(argumentMap contains "binaryPoint")) {
      throw new IllegalArgumentException("'binaryPoint' key was not found in arguments")
    }
    RingElem(FixedPoint(argumentMap("elementWidth").toInt.W, argumentMap("binaryPoint").toInt.BP))
  }

  /** Explicitly build a DspComplex[FixedPoint] type. */
  def buildComplexFixedPoint(argumentMap: Map[String, String]): RingElem[DspComplex[FixedPoint]] = {
    if (!(argumentMap contains "binaryPoint")) {
      throw new IllegalArgumentException("'binaryPoint' key was not found in arguments")
    }
    RingElem(DspComplex(FixedPoint(argumentMap("elementWidth").toInt.W, argumentMap("binaryPoint").toInt.BP)))
  }
}
