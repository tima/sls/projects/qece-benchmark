/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.generation

/** Scala object for requirement on module building. */
object Require {

  /** Checks if a value divides another
   *
   *  @param divider
   *    divider value
   *  @param value
   *    value to be divided
   *  @return
   *    weither divider divides value or not
   */
  def divide(divider: Int, value: Int): Boolean = {
    ((divider <= value) & ((value % divider) == 0))
  }

  /** Checks if a value is a power of two
   *
   *  @param value
   *    value to be checked
   *  @return
   *    weither the value is a power of two or not
   */
  def isPow2(value: Int): Boolean = {
    ((value & -value) == value)
  }

}
