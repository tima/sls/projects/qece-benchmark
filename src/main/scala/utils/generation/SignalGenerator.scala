/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.generation

import bench.utils.software.Numeric

import chisel3.Data
import dsptools.numbers.Ring

import scala.collection.AbstractIterator

abstract class SignalGenerator extends AbstractIterator[Numeric]

class RandomSignalGenerator[T <: Data: Ring](elemType: T) extends SignalGenerator {
  def hasNext(): Boolean = { true }
  def next(): Numeric = {
    Numeric.random(elemType)
  }
}

class DiracSignalGenerator(delay: Int, diracValue: BigInt) extends SignalGenerator {
  var counter = 0
  def hasNext(): Boolean = { true }
  def next(): Numeric = {
    val signal = Numeric(if (counter == delay) diracValue else BigInt(0))
    counter = counter + 1
    signal
  }
}

class StepSignalGenerator(delay: Int, stepValue: BigInt) extends SignalGenerator {
  var counter = 0
  def hasNext(): Boolean = { true }
  def next(): Numeric = {
    val signal = Numeric(if (counter >= delay) stepValue else BigInt(0))
    counter = counter + 1
    signal
  }
}

class SinusSignalGenerator(
    frequency: Int,
    amplitude: Int,
    clockFrequency: Int,
    isFixedPoint: Boolean = false
) extends SignalGenerator {
  var tick_counter = 0
  def hasNext(): Boolean = { true }
  def next(): Numeric = {
    val t     = tick_counter.toDouble / clockFrequency
    val value = Math.sin(2 * Math.PI * frequency * t)
    val signal = if (isFixedPoint) {
      Numeric(amplitude * value)
    } else {
      Numeric(BigDecimal(amplitude * value).setScale(0, BigDecimal.RoundingMode.HALF_UP).toBigInt())
    }
    tick_counter = tick_counter + 1
    signal
  }
}

class SumSignalsGenerator(array: Array[SignalGenerator], isFixedPoint: Boolean = false) extends SignalGenerator {
  def hasNext(): Boolean = { true }
  def next(): Numeric = {
    var result = if (isFixedPoint) Numeric(0.0) else Numeric(0)
    array.map(signal => result = result + signal.next())
    result
  }
}

class IncreasingSignalGenerator(startValue: BigInt) extends SignalGenerator {
  var counter = startValue
  def hasNext(): Boolean = { true }
  def next(): Numeric = {
    val signal = Numeric(counter)
    counter = counter + 1
    signal
  }
}
