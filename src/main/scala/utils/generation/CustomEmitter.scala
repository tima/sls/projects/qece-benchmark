/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.generation

import chisel3.RawModule
import chisel3.stage.ChiselStage

trait CustomEmitter extends App {
  lazy val newArgs = args.isEmpty match {
    case false =>
      if (args.contains("--target-dir")) {
        args
      } else {
        args ++ Array("--target-dir", "chisel_gen")
      }
    case true => Array("--target-dir", "chisel_gen")
  }

  private def appendDir(post: String): Array[String] = {
    val index = newArgs.indexOf("--target-dir") + 1
    newArgs.updated(index, s"${newArgs(index)}/$post")
  }

  def emit(m: => RawModule, endPath: String = ""): Unit = {
    (new ChiselStage).emitVerilog(m, appendDir(endPath))
  }
}
