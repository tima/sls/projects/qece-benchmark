/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.components

import chisel3.{Data, _}
import chisel3.experimental.FixedPoint
import chisel3.util.{Cat, ShiftRegister, log2Ceil}

import dsptools.DspContext
import dsptools.numbers._

import bench.utils.generation.Require
import bench.utils.hardware.{Delay, RingElem}
import bench.utils.software.Numeric
import bench.utils.components.memory.PresetMemory

import breeze.linalg.linspace

/** Fast evaluation of non linear function using look up table with piecewise linear approximation in a specific
 *  interval [a, b]
 *
 *  @param elemType
 *    Data type (WARNING: Only fixed point are supported.)
 *  @param config
 *    Configuration of the LUT
 */
class LUTFunction[T <: Data: Ring](elemType: RingElem[T], config: LUTFunctionConfig[T]) extends Module {
  val io = IO(new Bundle() {
    val x   = Input(elemType)
    val y   = Output(elemType)
    val rom = Input(Vec(config.size + 1, elemType))
  })

  var pipelineLevel = 0
  def getPipelineLevel(): Int = {
    pipelineLevel
  }

  val signal = elemType.value

  // Compute the LUT index corresponding to the input
  val centeredX = (if (config.a == 0) {
                     io.x
                   } else {
                     pipelineLevel += 1
                     io.x - Numeric(config.a.toDouble).toRingElem(elemType)
                   }).asTypeOf(Bits(elemType.getWidth.W))
  val index = (centeredX >> config.shift).asUInt()

  // Get the extreme values of the interval
  // Todo : verify that index + 1 is not a critical path.
  val y0 = io.rom(index).asTypeOf(elemType)
  val y1 = io.rom(Mux(index === config.size.U, index, index + 1.U)).asTypeOf(elemType)

  // Use linear interpolation to get a better approximation
  val dy = y1 - y0
  val dx = ShiftRegister(Cat(0.U, centeredX(config.shift - 1, 0)).asTypeOf(elemType), Delay.getAddDelay(elemType))

  DspContext.withNumMulPipes(Delay.getMultDelay(elemType)) {
    val y = ((dy << log2Ceil(config.size / (config.b - config.a))).asTypeOf(signal) context_* dx.asTypeOf(signal))
      .asTypeOf(elemType) + ShiftRegister(y0, Delay.getAddDelay(elemType) + Delay.getMultDelay(elemType))
    pipelineLevel += 2 * Delay.getAddDelay(elemType) + Delay.getMultDelay(elemType)
    io.y := y
  }
}

object LUTFunction {
  def apply[T <: Data: Ring](elemType: RingElem[T], config: LUTFunctionConfig[T]): LUTFunction[T] = {
    val m   = Module(new LUTFunction(elemType, config))
    val rom = config.getROM()
    m.io.rom := rom.values
    m
  }

  def connectROM[T <: Data: Ring](lut: LUTFunction[T], rom: Vec[RingElem[T]]): Unit = {
    lut.io.rom := rom
  }
}

/** Configuration of the LUT Function by giving the function to evaluate and the range. NOTE : the size of the range
 *  must be a power of 2
 *
 *  @param elemType
 *    Data type (WARNING: Only fixed point are supported.)
 *  @param fct
 *    Function to evaluate
 *  @param size
 *    Number of samples registered in the ROM (it must be a power of 2)
 *  @param a
 *    Beginning of the range
 *  @param b
 *    Ending of the range
 */
class LUTFunctionConfig[T <: Data: Ring](
    elemType: RingElem[T],
    val fct: Double => Double,
    val size: Int,
    val a: Int,
    val b: Int
) {
  require(elemType.value.isInstanceOf[FixedPoint], "Only fixed points are supported.")
  require(Require.isPow2(size), "The size must be a power of 2")

  val n = elemType.getWidth
  val k = log2Ceil(size)
  require(Require.isPow2(b - a), "The interval size must be a power of 2")

  val fixedPoint = elemType.value.asInstanceOf[FixedPoint]
  val shift      = (fixedPoint.binaryPoint.get - k) + log2Ceil(b - a)
  require(shift >= 0, "The size must be compatible with the current fixed point precision")

  val fctValues = linspace(a, b, size + 1).map(value => fct(value))

  def getROM(): PresetMemory[T] = {
    PresetMemory(elemType, fctValues.map(value => Numeric(value)).toArray)
  }

  def debugIndex(x: Double): Unit = {
    val xvalues = linspace(a, b, size).toArray
    for (i <- 1 until xvalues.size) {
      if (xvalues(i) > x) {
        println(
            "Index: " + (i - 1) + "; x: " + xvalues(i - 1) + "; y: " + fct(xvalues(i - 1)) + " dx: " + (xvalues(
                i - 1
            ) - x)
        )
        return
      }
    }
  }
}
