/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.components.random

import bench.utils.components.{LUTFunction, LUTFunctionConfig}
import bench.utils.hardware.{Delay, RingElem}

import chisel3._
import dsptools.DspContext
import dsptools.numbers._

/** Box Muller transformation to generate a single (but can be easily generate a pair) of independent, standard, normaly
 *  distributed random numbers from a source of UNIFORMLY distributed random numbers. WARNING : the input random numbers
 *  MUST be in [0, 1] NOTE : It's recommended to generate sin2piConfig & sqrtlnConfig from BoxMuller.defaultConfig
 *
 *  @param elemType
 *    Data type (must be a fixed point because of the LUT functions)
 *  @param sin2piConfig
 *    f(x) = Sin(2*Pi*x) configuration
 *  @param sqrtlnConfig
 *    f(x) = Sqrt(-2*ln(x)) configuration
 *  @param sharedROM
 *    Do we use a shared ROM ?
 */
class BoxMuller[T <: Data: Ring](
    elemType: RingElem[T],
    sin2piConfig: LUTFunctionConfig[T],
    sqrtlnConfig: LUTFunctionConfig[T],
    sharedROM: Boolean
) extends MultiIOModule {
  val io = IO(new Bundle() {
    val urng0 = Input(elemType)
    val urng1 = Input(elemType)
    val gauss = Output(elemType)
  })

  val rom = IO(new Bundle {
    val sin2pi = Input(Vec(sin2piConfig.size + 1, elemType))
    val sqrtln = Input(Vec(sqrtlnConfig.size + 1, elemType))
  })

  def getPipelineLevel(): Int = {
    sin2pi.getPipelineLevel() + Delay.getMultDelay(elemType)
  }

  val sin2pi = Module(new LUTFunction(elemType, sin2piConfig))
  val sqrtln = Module(new LUTFunction(elemType, sqrtlnConfig))

  if (sharedROM) {
    sin2pi.io.rom := rom.sin2pi
    sqrtln.io.rom := rom.sqrtln
  } else {
    sin2pi.io.rom := sin2piConfig.getROM().values
    sqrtln.io.rom := sqrtlnConfig.getROM().values
  }

  assert(
      sin2pi.getPipelineLevel() == sqrtln.getPipelineLevel(),
      "Something goes wrong : the pipeline levels are different"
  )

  sqrtln.io.x := io.urng0
  sin2pi.io.x := io.urng1

  val signal = elemType.value
  DspContext.withNumMulPipes(Delay.getMultDelay(elemType)) {
    io.gauss := (sqrtln.io.y.asTypeOf(signal) context_* sin2pi.io.y.asTypeOf(signal)).asTypeOf(elemType)
  }

}

object BoxMuller {
  def apply[T <: Data: Ring](elemType: RingElem[T], size: Int): BoxMuller[T] = {
    val config = defaultConfig(elemType, size)
    new BoxMuller(elemType, config._1, config._2, false)
  }

  def defaultConfig[T <: Data: Ring](elemType: RingElem[T], size: Int): (LUTFunctionConfig[T], LUTFunctionConfig[T]) = {
    val sin2piConfig = new LUTFunctionConfig(elemType, x => Math.sin(2 * Math.PI * x), size, 0, 1)
    val sqrtlnConfig =
      new LUTFunctionConfig(elemType, x => Math.sqrt(-2 * Math.log(if (x == 0) 0.000000001 else x)), size, 0, 1)
    (sin2piConfig, sqrtlnConfig)
  }
}
