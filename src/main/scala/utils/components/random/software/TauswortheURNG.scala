/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.components.random.software

import bench.utils.components.random.TauswortheConfig

import plotly._
import plotly.element._
import plotly.layout._
import spire.math.UInt

class TauswortheURNG(configs: Array[TauswortheConfig]) {
  private val size  = 32
  private val masks = configs.map(config => UInt(-math.pow(2, size - config.k).toInt))
  val xs            = configs.map(config => UInt(config.x0.toInt))

  def random(): UInt = {
    var result = UInt(0)
    for (i <- 0 until configs.size) {
      val config = configs(i)
      val b      = ((xs(i) << config.q) ^ xs(i)) >> (config.k - config.s)
      xs(i) = ((xs(i) & masks(i)) << config.s) ^ b
      result ^= xs(i)
    }
    result
  }

  def randomNormalized(): Double = {
    random().toDouble / UInt.MaxValue.toLong
  }
}

object TauswortheURNGPlot extends App {
  val n    = 100000
  val urng = new TauswortheURNG(TauswortheConfig.default(123456, 7891011, 121314))

  val array = Array.fill[Double](n)(urng.randomNormalized())

  Plotly.plot(
      "plots/TausworthePDF",
      Seq(Histogram(array.toSeq, histnorm = HistNorm.ProbabilityDensity)),
      Layout(title = "Discrete approximation of the uniform distribution with Tausworthe generation")
  )
}
