/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.components.random.software

import bench.utils.components.random.TauswortheConfig

import plotly.element.HistNorm
import plotly.layout.Layout
import plotly.{Histogram, Plotly, Scatter}

class BoxMuller(urng1: TauswortheURNG, urng2: TauswortheURNG) {
  val r = scala.util.Random
  def gauss(): (Double, Double) = {
    val u1 = urng1.randomNormalized()
    val u2 = urng2.randomNormalized()
    BoxMuller.gauss(u1, u2)
  }
}

object BoxMuller {
  def gauss(u1: Double, u2: Double): (Double, Double) = {
    val f = Math.sqrt(-2 * Math.log(u1))
    (f * Math.cos(2 * Math.PI * u2), f * Math.sin(2 * Math.PI * u2))
  }
}

object BoxMullerTest extends App() {

  def generateSamples(n: Int): Array[Double] = {
    val boxMuller = new BoxMuller(
        new TauswortheURNG(TauswortheConfig.default(242642, 156416254, 543242)),
        new TauswortheURNG(TauswortheConfig.default(132456, 5727251, 2431))
    )
    Array.fill(n)(boxMuller.gauss()._1)
  }

  def generatePDFDistribution(n: Int, max: Int): (Array[Double], Array[Double]) = {
    val xValues = breeze.linalg.linspace(-max, max, n).toArray
    val yValues = new Array[Double](n)
    for ((value, index) <- xValues.zipWithIndex) {
      yValues(index) = 1 / Math.sqrt(2 * Math.PI) * Math.exp(-Math.pow(value, 2) / 2)
    }
    (xValues, yValues)
  }

  val n    = 1000000
  val hist = generateSamples(n)
  val pdf  = generatePDFDistribution(n, 5)

  Plotly.plot(
      "plots/BoxMullerPDF",
      Seq(Histogram(hist.toSeq, histnorm = HistNorm.ProbabilityDensity), Scatter(pdf._1.toSeq, pdf._2.toSeq)),
      Layout(title = "Discrete approximation of the normal distribution with Box-Muller transform")
  )
}
