/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.components.random

import bench.utils.components.LUTFunctionConfig
import bench.utils.hardware.RingElem

import chisel3._
import chisel3.experimental.FixedPoint
import chisel3.util._
import dsptools.numbers.Ring

/** Generate independent, standard, normaly distributed random numbers using Box Muller transform with Tausworthe
 *  generators
 *  @param elemType
 *    type of data (must be a fixed point because of the LUT functions)
 *  @param urngConfig
 *    configuration of Tausworthe generators
 *  @param boxMullerConfig
 *    configuration of Box Muller's LUT function
 *  @param sharedROM
 *    tells to the generator if Box Muller's LUT function use a shared ROM
 */
class GaussGenerator[T <: Data: Ring](
    elemType: RingElem[T],
    urngConfig: (Array[TauswortheConfig], Array[TauswortheConfig]),
    boxMullerConfig: (LUTFunctionConfig[T], LUTFunctionConfig[T]),
    sharedROM: Boolean
) extends MultiIOModule {

  val io = IO(new Bundle {
    val gauss = Output(Valid(elemType))
  })

  val rom = IO(new Bundle {
    val sin2pi = Input(Vec(boxMullerConfig._1.size + 1, elemType))
    val sqrtln = Input(Vec(boxMullerConfig._2.size + 1, elemType))
  })

  def getPipelineLevel(): Int = {
    boxMuller.getPipelineLevel()
  }

  val fixedPoint = elemType.value.asInstanceOf[FixedPoint]

  val urng0     = Module(new TauswortheURNG(elemType.getWidth, urngConfig._1))
  val urng1     = Module(new TauswortheURNG(elemType.getWidth, urngConfig._2))
  val boxMuller = Module(new BoxMuller(elemType, boxMullerConfig._1, boxMullerConfig._2, sharedROM))

  if (sharedROM) {
    boxMuller.rom <> rom
  } else {
    boxMuller.rom.sin2pi := DontCare
    boxMuller.rom.sqrtln := DontCare
  }

  // Connect and normalized Tausworthe output to the Box-Muller transformation module
  boxMuller.io.urng0 := Cat(
      0.U(fixedPoint.getWidth - fixedPoint.binaryPoint.get),
      urng0.io.urng(fixedPoint.binaryPoint.get - 1, 0)
  ).asTypeOf(elemType)
  boxMuller.io.urng1 := Cat(
      0.U(fixedPoint.getWidth - fixedPoint.binaryPoint.get),
      urng1.io.urng(fixedPoint.binaryPoint.get - 1, 0)
  ).asTypeOf(elemType)

  io.gauss.bits := boxMuller.io.gauss
  io.gauss.valid := ShiftRegister(true.B, boxMuller.getPipelineLevel(), false.B, true.B)
}
