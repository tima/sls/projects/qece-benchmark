/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.components.random

import scala.util.Random

/** Configuration of a single Tausworthe generator. For further details and conditions on the parameters :
 *  https://www.rocq.inria.fr/mathfi/Premia/free-version/doc/premia-doc/pdf_html/common/math/random_doc/index.html#x1-100002.7
 *
 *  @param k
 *    @param q
 *  @param s
 *    @param x0 seed of the generator (must be > 0)
 */
class TauswortheConfig(val k: Int, val q: Int, val s: Int, val x0: BigInt) {
  assert(0 < 2 * q && 2 * q < k, "Parameters must verify 0 < 2q < k")
  assert(0 <= k - q && k - q <= k, "Parameters must verify s < k - q < k")
  assert(BigInt(s).gcd(BigInt(2).pow(k) - 1) == 1, "Parameters must verify gcd(s, 2^k - 1) == 1")
  // WARNING : P(x) = x^k - ax^q - 1 MUST BE A PRIMITIVE TRINOMIAL : this condition is not checked.
}

object TauswortheConfig {

  /** Default configuration with 3 generators preconfigured (this configuration is often used as example)
   *  @param x0
   *    seed of the first generator
   *  @param x1
   *    seed of the second generator
   *  @param x2
   *    seed of the third generator
   *  @return
   *    the configuration of the generators
   */
  def default(x0: BigInt, x1: BigInt, x2: BigInt): Array[TauswortheConfig] = {
    val p1 = new TauswortheConfig(31, 13, 12, x0)
    val p2 = new TauswortheConfig(29, 2, 4, x1)
    val p3 = new TauswortheConfig(28, 3, 17, x2)
    Array(p1, p2, p3)
  }

  def tupleDefault(
      x00: BigInt,
      x01: BigInt,
      x02: BigInt,
      x10: BigInt,
      x11: BigInt,
      x12: BigInt
  ): (Array[TauswortheConfig], Array[TauswortheConfig]) = {
    (default(x00, x01, x02), default(x10, x11, x12))
  }

  def randomTupleIterator(seed: Long, bitWidth: Int): Iterator[(Array[TauswortheConfig], Array[TauswortheConfig])] = {
    Random.setSeed(seed)

    def nextRandom(): BigInt = {
      BigInt(bitWidth, Random)
    }

    new Iterator[(Array[TauswortheConfig], Array[TauswortheConfig])] {
      override def hasNext: Boolean = true

      override def next(): (Array[TauswortheConfig], Array[TauswortheConfig]) = {
        tupleDefault(
            nextRandom(),
            nextRandom(),
            nextRandom(),
            nextRandom(),
            nextRandom(),
            nextRandom()
        )
      }
    }
  }
}
