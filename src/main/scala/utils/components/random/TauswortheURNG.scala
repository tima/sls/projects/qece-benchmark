/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.components.random

import chisel3._

/** Generator of uniformly distributed random numbers using the Tausworthe method
 *  @param bitWidth
 *    Size of the number
 *  @param configs
 *    Configuration of the generators (cf TauswortheConfig)
 */
class TauswortheURNG(bitWidth: Int, configs: Array[TauswortheConfig]) extends Module {
  val io = IO(new Bundle() {
    val urng = Output(UInt(bitWidth.W))
  })

  configs.foreach(config => require(config.k < bitWidth, "k must be < bitwidth"))

  val masks =
    configs.map(config => ((BigInt(2).pow(bitWidth) - 1) ^ (BigInt(2).pow(bitWidth - config.k) - 1)).U(bitWidth.W))
  val xs = configs.map(config => RegInit(UInt(bitWidth.W), config.x0.U))

  for (i <- 0 until configs.size) {
    val config = configs(i)
    val mask   = masks(i)
    val x      = xs(i)
    x := ((x & mask) << config.s)
      .asTypeOf(UInt(bitWidth.W)) ^ (((x << config.q).asTypeOf(UInt(bitWidth.W)) ^ x) >> (config.k - config.s))
      .asTypeOf(UInt(bitWidth.W))
  }

  io.urng := xs.reduce(_ ^ _)
}
