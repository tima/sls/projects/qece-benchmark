/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.components

import bench.utils.convert._
import bench.utils.generation.Require
import bench.utils.hardware.{RingElem, Delay}

import chisel3._
import chisel3.util._
import dsptools.numbers.Ring

/** Basic Multiply and Accumulate implementation
 *  @param gen
 *    type of elements to multiply
 *  @param nbElem
 *    defining the number of elements
 */
class Mac[T <: Data: Ring](val gen: RingElem[T], val nbElem: Int) extends MultiIOModule with DelayKnown {

  /* Constants for generation */
  val elemWidth = gen.getWidth

  val io = IO(new Bundle {
    val op1    = Input(Vec(nbElem, gen))
    val op2    = Input(Vec(nbElem, gen))
    val result = Output(gen.cloneType)
  })

  val pipelineLevel         = (log2Ceil(nbElem)) * Delay.getAddDelay(gen) + Delay.getMultDelay(gen)
  def getPipelineLevel: Int = pipelineLevel

  val buffer = Wire(Vec(nbElem, gen))
  buffer := (io.op1 zip io.op2).map { case (a, b) => a * b } // Compute multiplication
  io.result := buffer.reduceTree(_ + _, _.delay(Delay.getAddDelay(gen))) // Compute addition
}

/** Basic operator for matrix multiplication TODO : integrate in GEMM Basic Mac with accu possibility
 *  => need to deserialize in Top and to poke vectors for Mac tester
 *
 *  @param gen
 *    type of elements to multiply
 *  @param bitWidth
 *    bitWidth of I/O, defining the number of element to multiply by cycle
 */
class DeserMac[T <: Data: Ring](val gen: RingElem[T], val bitWidth: Int) extends Module with DelayKnown {
  require(Require.divide(gen.getWidth, bitWidth), "Input bitwidth must be a multiple of elem width")

  val io = IO(new Bundle {
    val a_line = Input(UInt(bitWidth.W)) /* subblock of a line    (a(i, k) to a(i, k+m')) AS UINT  */
    val b_col  = Input(UInt(bitWidth.W)) /* subblock of b column  (b(k, j) to b(k+m', j)) AS UINT  */
    val reset  = Input(Bool()) /* control signal for reseting Accu                       */
    val result = Output(gen.cloneType) /* compute c(i, j) in (N/m') cycle                        */
  })

  /* Constants for generation */
  val elemWidth = gen.getWidth
  val nbElem    = bitWidth / elemWidth

  val mac = Module(new Mac(gen, nbElem))

  def getPipelineLevel: Int = mac.getPipelineLevel

  mac.io.op1 := io.a_line.deserialize(gen)
  mac.io.op2 := io.b_col.deserialize(gen)
  val macResult = mac.io.result

  val accu = RegInit(gen.cloneType, gen.zero)

  /* Delay the reset signal to fit the pipeline level */
  val resetReg = ShiftRegister(io.reset, getPipelineLevel)

  /* Reset can be done while accumulating */
  Delay.preventDelay {
    accu := Mux(resetReg, macResult, accu + macResult)
  }
  io.result := accu
}
