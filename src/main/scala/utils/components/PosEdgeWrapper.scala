/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.components

import bench.utils.interface.DecoupledStreamBundle

import chisel3._

/** Wrapper used in test. Since PeekPokeTesters poke at neg edge, need to register every input to avoid combinatorial
 *  problems on decoupled logic.
 */
class PosEdgeWrapper(dut: => Role) extends MultiIOModule {
  /* passing top design by-name and not by-value see https://groups.google.com/forum/#!topic/chisel-users/ZT911SE6Q_w */

  /* Instantiate top design */
  val mut      = Module(dut) // Need to be done before instantiating IO, since we need to retrieve bitwidth from top
  val bitWidth = mut.bitWidth

  /* Instantiating IO */
  val io = IO(new DecoupledStreamBundle(bitWidth))

  /* Decoupled input management */

  private val regbits  = RegInit(0.U.asTypeOf(chiselTypeOf(io.in.bits)))
  private val regvalid = RegInit(false.B)

  regbits := io.in.bits
  regvalid := io.in.valid

  mut.io.in.bits := regbits
  mut.io.in.valid := regvalid
  io.in.ready := mut.io.in.ready

  /* Decoupled output management */

  io.out.bits := mut.io.out.bits
  io.out.valid := mut.io.out.valid

  private val regready = RegInit(false.B)
  regready := io.out.ready
  mut.io.out.ready := regready
}
