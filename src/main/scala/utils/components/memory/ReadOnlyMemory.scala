/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.components.memory

import bench.utils.hardware.RingElem
import bench.utils.software._

import logger.LazyLogging

import chisel3._
import dsptools.numbers.Ring

/** Read Only Memory component. */
class ReadOnlyMemory[T <: Data: Ring](
    val gen: RingElem[T],
    val initData: Array[Numeric]
) extends LazyLogging {
  class InnerMemory extends MultiIOModule {
    private val nValues = initData.size
    val values          = IO(Output(Vec(nValues, gen)))

    val inner = WireInit(VecInit.tabulate(nValues)(i => initData(i).toRingElem(gen)))

    (0 until nValues).foreach(i => values(i) := inner(i))
  }

  val inner = Module(new InnerMemory)

  val values: Vec[RingElem[T]] = inner.values
}

object ReadOnlyMemory {
  def apply[T <: Data: Ring](gen: RingElem[T], initData: Array[Numeric]): ReadOnlyMemory[T] = {
    new ReadOnlyMemory(gen, initData)
  }
}
