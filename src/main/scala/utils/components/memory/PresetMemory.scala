/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.components.memory

import bench.utils.hardware.RingElem
import bench.utils.software._
import bench.utils.generation.Require

import logger.LazyLogging

import chisel3._
import dsptools.numbers.Ring

/** A memory component with a pre defined configuration
 *
 *  If set as ROM, will be a constant reading component. If not, will be able to be set as a memory component on its own
 *  Either way, every values can be accessed at the same time
 *
 *  @param gen
 *    type of elements used
 *  @param initData
 *    initial datas to be found in memory
 *  @param nInputs
 *    define the width of input bus, as a number of element, or None if ROM mode
 */
class PresetMemory[T <: Data: Ring](
    val gen: RingElem[T],
    val initData: Array[Numeric],
    val nInputs: Option[Int] = None
) extends LazyLogging {
  class InnerMemory extends MultiIOModule {
    private val nValues = initData.length

    require(
        nInputs.getOrElse(0) <= nValues,
        s"Number of inputs by cycle ${nInputs.getOrElse(0)} must be lesser than total amount of value $nValues"
    )
    require(
        Require.divide(nInputs.getOrElse(1), nValues),
        s"Number of inputs by cycle ${nInputs.getOrElse(0)} must divide number of values $nValues"
    )

    // define input ports iff not ROM
    val inPort = nInputs match {
      case Some(x) => Some(IO(new InputMemoryPort(nValues / x - 1, gen.getWidth * x)))
      case None    => None
    }
    val values = IO(Output(Vec(nValues, gen)))

    (inPort, nInputs) match {
      case (Some(p), Some(n)) =>
        val memory = SyncReadMem(nValues / n, UInt((gen.getWidth * n).W))
        (0 until nValues / n).foreach {
          case i =>
            val outVec = Wire(Vec(n, gen))
            (outVec zip memory.read(i.U).asTypeOf(Vec(n, gen))).foreach { case (a, b) => a := b }
            (0 until n).foreach(j => values(i * n + j) := outVec(j))
        }
        when(p.en) {
          for (i <- 0 to nValues - 1) {
            when(p.addr === i.U) {
              memory(p.addr) := p.data
            }
          }
        }
      case _ =>
        val memory = WireInit(VecInit.tabulate(nValues)(i => initData(i).toRingElem(gen)))
        (0 until nValues).foreach(i => values(i) := memory(i))
    }
  }

  val inner = Module(new InnerMemory)
  inner.inPort match {
    case Some(p) =>
      p.en := false.B
      p.data := DontCare
      p.addr := DontCare
    case None =>
  }

  val values: Vec[RingElem[T]] = inner.values

  def write(baseAddr: UInt, data: UInt, enable: Bool = true.B): Unit = {
    inner.inPort match {
      case Some(p) =>
        p.addr := baseAddr
        p.data := data
        p.en := enable
      case None => logger.warn(s"Can't write to ROM")
    }
  }
}

object PresetMemory {
  def apply[T <: Data: Ring](
      gen: RingElem[T],
      initData: Array[Numeric],
      nInputs: Option[Int] = None
  ): PresetMemory[T] = {
    new PresetMemory(gen, initData, nInputs)
  }
}
