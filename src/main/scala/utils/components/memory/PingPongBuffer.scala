/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.components.memory

import bench.utils.hardware.RingElem

import chisel3._
import chisel3.util.log2Ceil
import dsptools.numbers.Ring

class PingPongBuffer[T <: Data: Ring](elemType: RingElem[T], vecSize: Int, nbVec: Int, nbBanks: Int) extends Module {

  val io = IO(new Bundle {
    val in      = Input(Vec(nbBanks, Vec(vecSize, elemType)))
    val out     = Output(Vec(nbBanks, Vec(vecSize, elemType)))
    val inAddr  = Input(Vec(nbBanks, UInt(log2Ceil(nbVec).W)))
    val outAddr = Input(Vec(nbBanks, UInt(log2Ceil(nbVec).W)))

    val inValid  = Input(Bool())
    val outValid = Output(Bool())

    val enable = Input(Bool())
    val swap   = Input(Bool())
  })

  val memories = Array.fill(2)(BankedMemory(nbBanks, nbVec, elemType.getWidth * vecSize))

  val pingValid = RegInit(Bool(), false.B)
  val pongValid = RegInit(Bool(), false.B)

  val delayedSwap    = RegNext(io.swap, false.B)
  val delayedInAddr  = RegNext(io.inAddr)
  val delayedIn      = RegNext(io.in)
  val delayedEnable  = RegNext(io.enable)
  val delayedInValid = RegNext(io.inValid)

  // Write end:
  when(delayedEnable) {
    when(delayedSwap) {
      pongValid := delayedInValid
      for (indexBank <- 0 until nbBanks) {
        memories(1).write(indexBank.U, delayedInAddr(indexBank), delayedIn(indexBank).asUInt())
      }
    } otherwise {
      pingValid := delayedInValid
      for (indexBank <- 0 until nbBanks) {
        memories(0).write(indexBank.U, delayedInAddr(indexBank), delayedIn(indexBank).asUInt())
      }
    }
  }

  // Read end:
  var output = Wire(Vec(nbBanks, Vec(vecSize, elemType)))

  when(delayedSwap) {
    for (indexBank <- 0 until nbBanks) {
      val result = memories(0).read(indexBank.U, io.outAddr(indexBank)).asTypeOf(Vec(vecSize, elemType))
      output(indexBank).zip(result).foreach { case (a, b) => a := b }
    }
    io.outValid := pingValid
  } otherwise {
    for (indexBank <- 0 until nbBanks) {
      val result = memories(1).read(indexBank.U, io.outAddr(indexBank)).asTypeOf(Vec(vecSize, elemType))
      output(indexBank).zip(result).foreach { case (a, b) => a := b }
    }
    io.outValid := pongValid
  }

  io.out := output
}
