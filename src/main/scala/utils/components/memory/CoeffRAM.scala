/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.components.memory

import bench.utils.hardware.RingElem
import bench.utils.generation.Require

import logger.LazyLogging

import chisel3._
import dsptools.numbers.Ring

/** A memory component with all element accessed at the same time
 *
 *  @param gen
 *    type of elements used
 *  @param size
 *    number of elements in the memory
 *  @param nInputs
 *    define the width of input bus, as a number of element
 */
class CoeffRAM[T <: Data: Ring](
    val gen: RingElem[T],
    val size: Int,
    val nInputs: Int
) extends LazyLogging {
  class InnerMemory extends MultiIOModule {
    require(nInputs <= size, s"Number of inputs by cycle $nInputs must be lesser than total amount of value $size")
    require(Require.divide(nInputs, size), s"Number of inputs by cycle $nInputs must divide number of values $size")

    // define ports
    val inPort = IO(new InputMemoryPort(size / nInputs - 1, gen.getWidth * nInputs))
    val values = IO(Output(Vec(size, gen)))

    val memory = SyncReadMem(size / nInputs, UInt((gen.getWidth * nInputs).W))
    (0 until size / nInputs).foreach {
      case i =>
        val outVec = Wire(Vec(nInputs, gen))
        (outVec zip memory.read(i.U).asTypeOf(Vec(nInputs, gen))).foreach { case (a, b) => a := b }
        (0 until nInputs).foreach(j => values(i * nInputs + j) := outVec(j))
    }

    when(inPort.en) {
      for (i <- 0 to size - 1) {
        when(inPort.addr === i.U) {
          memory(inPort.addr) := inPort.data
        }
      }
    }
  }

  val inner = Module(new InnerMemory)
  inner.inPort.en := false.B
  inner.inPort.data := DontCare
  inner.inPort.addr := DontCare

  def values: Vec[RingElem[T]] = inner.values

  def write(baseAddr: UInt, data: UInt, enable: Bool = true.B): Unit = {
    inner.inPort.addr := baseAddr
    inner.inPort.data := data
    inner.inPort.en := enable
  }
}

object CoeffRAM {
  def apply[T <: Data: Ring](gen: RingElem[T], size: Int, nInputs: Int): CoeffRAM[T] = {
    new CoeffRAM(gen, size, nInputs)
  }
}
