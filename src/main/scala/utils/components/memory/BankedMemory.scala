/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.components.memory

import chisel3._
import chisel3.util._

/** Banked memory hardware implementation.
 *
 *  WARNING : No dual port is currently supported, as a single address bus is used for both reads and writes.
 */
class Banks(val nBanks: Int, val nElem: Int, val elemW: Int) extends Module {
  val io = IO(new Bundle {
    val dataIn  = Input(Vec(nBanks, UInt(elemW.W)))
    val addr    = Input(Vec(nBanks, UInt(log2Up(nElem).W)))
    val ren     = Input(Vec(nBanks, Bool()))
    val wen     = Input(Vec(nBanks, Bool()))
    val dataOut = Output(Vec(nBanks, UInt(elemW.W)))
  })

  val memory = Seq.fill(nBanks)(SyncReadMem(nElem, UInt(elemW.W)))

  /* Using enable signals allows synthesis tool to infer BRAM See
   * https://groups.google.com/forum/?utm_medium=email&utm_source=footer#!msg/chisel-users/za7b9FMu_yY/E9J0xH8NAQAJ */
  for (i <- 0 until nBanks) {
    val bank = memory(i)
    when(io.wen(i.U)) { bank.write(io.addr(i.U), io.dataIn(i.U)) }
    io.dataOut(i.U) := bank.read(io.addr(i.U), io.ren(i.U))
  }

}

/** Banked memory accessors.
 *
 *  @constructor
 *    create a BankedMemory
 *  @param nBanks
 *    number of banks available
 *  @param nElem
 *    number of elements in each bank
 *  @param elemW
 *    width of each element
 */
class BankedMemory(val nBanks: Int, val nElem: Int, val elemW: Int) {
  val memory = Module(new Banks(nBanks, nElem, elemW))
  for (i <- 0 until nBanks) {
    memory.io.dataIn(i.U) := 0.U
    memory.io.addr(i.U) := 0.U
    memory.io.ren(i.U) := false.B
    memory.io.wen(i.U) := false.B
  }

  /** Read access to the BankedMemory instantiated (with explicit read enable)
   *
   *  @param bank
   *    identifiant of bank to be accessed
   *  @param addr
   *    address to be accessed in the bank
   *  @param en
   *    read enable
   *  @return
   *    memory content of bank {{{bank}}} with given address
   */
  def read(bank: UInt, addr: UInt, en: Bool): UInt = {
    memory.io.addr(bank) := addr
    memory.io.ren(bank) := en
    memory.io.dataOut(bank)
  }

  /** Read access to the BankedMemory instantiated (with implicit read enable)
   *
   *  @param bank
   *    identifiant of bank to be accessed
   *  @param addr
   *    address to be accessed in the bank
   *  @return
   *    memory content of bank {{{bank}}} with given address
   */
  def read(bank: UInt, addr: UInt): UInt = {
    memory.io.addr(bank) := addr
    memory.io.ren(bank) := true.B
    memory.io.dataOut(bank)
  }

  /** Write access to the BankedMemory instantiated (with implicit write enable)
   *
   *  @param bank
   *    identifiant of bank to be accessed
   *  @param addr
   *    address to be accessed in the bank
   *  @param data
   *    data to be writen
   */
  def write(bank: UInt, addr: UInt, data: UInt): Unit = {
    memory.io.addr(bank) := addr
    memory.io.dataIn(bank) := data
    memory.io.wen(bank) := true.B
  }
}

/** SyncReadMemory companion object. */
object BankedMemory {

  /** Create a new [[BankedMemory]] instance. */
  def apply(nBanks: Int, nElems: Int, elemW: Int): BankedMemory = {
    new BankedMemory(nBanks, nElems, elemW)
  }
}
