/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils

import bench.utils.hardware._

import chisel3._
import chisel3.experimental._
import chisel3.internal.firrtl.KnownBinaryPoint

import dsptools.numbers._

package object convert {
  implicit class IntToRingElemClass(val myVal: Int) {

    /** Get the targeted Int as the right RingElem type. */
    def toRingElem[T <: Data: Ring](signal: RingElem[T]): RingElem[T] = {
      signal.value match {
        case _: UInt => RingElem.toWire(myVal.asUInt.asInstanceOf[T])
        case _: SInt => RingElem.toWire(myVal.asSInt.asInstanceOf[T])
        case fp: FixedPoint =>
          (fp.binaryPoint, fp.getWidth) match {
            case (KnownBinaryPoint(bp), w) =>
              RingElem.toWire(FixedPoint(FixedPoint.toBigInt(myVal.toDouble, bp), w.W, bp.BP).asInstanceOf[T])
            case _ => throw new UnsupportedOperationException("Unset binary point for this Fixed Point instance")
          }
        case _ => throw new UnsupportedOperationException("Unrecognized hardware type")
      }
    }
  }

  implicit class UIntToRingElemVec(val myVal: UInt) {
    def deserialize[T <: Data: Ring](myType: RingElem[T]) = {
      val aw = myVal.getWidth
      val tw = myType.getWidth
      val w  = Wire(Vec(aw / tw, myType.cloneType))
      for (i <- 0 until aw / tw) w(i) := myVal((i + 1) * tw - 1, i * tw).asTypeOf(myType)
      w
    }
  }

  /** Allow to test compatibility between two hardware types */
  implicit class CompareDataTypes[T <: Data](val left: T) {
    def =:=[R <: Data](right: R): Boolean = {
      (left, left.getWidth, right, right.getWidth) match {
        case (_: UInt, lw: Int, _: UInt, rw: Int)                   => (lw == rw)
        case (_: SInt, lw: Int, _: SInt, rw: Int)                   => (lw == rw)
        case (_: FixedPoint, lw: Int, _: FixedPoint, rw: Int)       => (lw == rw)
        case (_: DspComplex[_], lw: Int, _: DspComplex[_], rw: Int) => (lw == rw)
        case _                                                      => throw new UnsupportedOperationException(s"Can't compare unknown types $left and $right")
      }
    }
  }

  /** Implicit conversion from BigInt to Boolean, to avoid laborious comparison when peeking on valid/ready signals */
  implicit def bigIntToBoolean(bi: BigInt): Boolean = (bi == 1)
}
