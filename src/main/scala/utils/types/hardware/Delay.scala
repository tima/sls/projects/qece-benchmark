/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.hardware

import scala.util.DynamicVariable

import chisel3._
import dsptools.numbers.DspComplex

case class Configuration(
    val DELAY_UNARY_OP: Int = 0,  // Never pipeline unary op
    val DELAY_BINARY_OP: Int = 1, // Always pipeline simple binary op
    val DELAY_DSP: Int = 4,       // Xilinx DSPs able to absorb for level of pipeline to increase frequency
    val DSP_USAGE_WIDTH: Int = 16 // DSPs enabled for bitwidth ge 16, by default
)

object Configuration {
  def getNull = Configuration(0, 0, 0)
}

/** Object to define custom pipeline level on operations. */
object Delay {
  private val configContextVar = new DynamicVariable[Configuration](Configuration())
  private def configContext: Configuration = {
    configContextVar.value
  }

  /** Allows some block to be executed in a combinatorial fashion.
   *
   *  @param block
   *    the execution block that won't get pipelined
   */
  def preventDelay[T](block: => T) {
    configContextVar.withValue(Configuration.getNull) {
      block
    }
  }

  /** Define addition pipeline level depending on the type of data. Define also substraction pipeline level
   *
   *  @param signal
   *    type of the data we're adding
   *  @return
   *    delay level of such addition
   */
  def getAddDelay[T <: Data](signal: T): Int = configContext.DELAY_BINARY_OP

  /** Define multiplication pipeline level depending on the type of data.
   *
   *  @param signal
   *    type of the data we're adding
   *  @return
   *    delay level of such mutiplication
   */
  def getMultDelay[T <: Data](signal: T): Int = {
    signal match {
      case r: RingElem[_] =>
        r.value match {
          case c: DspComplex[_] => getMultDelay(c.real) + getAddDelay(c.real)
          case _                => getMultDelay(r.value)
        }
      case c: DspComplex[_] => getMultDelay(c.real) + getAddDelay(c.real)
      case _ =>
        if (signal.getWidth < configContext.DSP_USAGE_WIDTH) configContext.DELAY_BINARY_OP else configContext.DELAY_DSP
    }
  }

  /** Define unary operation pipeline level depending on the type of data.
   *
   *  @param signal
   *    type of the data we're using
   *  @return
   *    delay level of such operation
   */
  def getUnaryDelay[T <: Data](signal: T): Int = configContext.DELAY_UNARY_OP
}
