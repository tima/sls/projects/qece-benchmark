/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.hardware

import bench.utils.convert._

import chisel3._
import chisel3.experimental._
import dsptools.DspContext
import dsptools.numbers._

object RingElem {

  /** Used for type constructor */
  def toWire[T <: Data: Ring](value: T): RingElem[T] = {
    val w = Wire(new RingElem(value.cloneType))
    w := value.asTypeOf(w); w
  }
}

case class RingElem[T <: Data: Ring](val value: T) extends Bundle {

  def cloneType[newT <: Data: Ring](bitWidth: Int): RingElem[newT] = {
    value match {
      case _: UInt       => RingElem(UInt(bitWidth.W).asInstanceOf[newT])
      case _: SInt       => RingElem(SInt(bitWidth.W).asInstanceOf[newT])
      case f: FixedPoint => RingElem(FixedPoint(bitWidth.W, f.binaryPoint).asInstanceOf[newT])
      case cp: DspComplex[_] => {
        cp.real match {
          case _: UInt       => RingElem(DspComplex(UInt(bitWidth.W)).asInstanceOf[newT])
          case _: SInt       => RingElem(DspComplex(SInt(bitWidth.W)).asInstanceOf[newT])
          case f: FixedPoint => RingElem(DspComplex(FixedPoint(bitWidth.W, f.binaryPoint)).asInstanceOf[newT])
        }
      }
      case _ => throw new UnsupportedOperationException(s"Can't set bit width on type ${value}")
    }
  }

  def isPositive: Bool = {
    value match {
      case _: UInt       => true.B
      case s: SInt       => s(s.getWidth - 1) === 0.U
      case f: FixedPoint => f(f.getWidth - 1) === 0.U
      case _             => throw new UnsupportedOperationException(s"Can't test sign on type ${this.value}")
    }
  }

  /** Classic addition between two compatible RingElem. */
  def +(that: RingElem[T]) = {
    delayBinaryOp(that, Delay.getAddDelay(value))(_ + _)
  }

  /** Classic mult between two compatible RingElem. */
  def *(that: RingElem[T]): RingElem[T] = {
    value match {
      case dp: DspComplex[_] => {
        DspContext.alter(
            DspContext.current.copy(
                numAddPipes = Delay.getAddDelay(dp.real),
                numMulPipes = Delay.getMultDelay(dp.real),
                complexUse4Muls = true
            )
        ) {
          dp.real match {
            case x: UInt => {
              RingElem.toWire((dp.asTypeOf(DspComplex(x)) context_* that.value.asTypeOf(DspComplex(x))).asInstanceOf[T])
            }
            case s: SInt => {
              RingElem.toWire((dp.asTypeOf(DspComplex(s)) context_* that.value.asTypeOf(DspComplex(s))).asInstanceOf[T])
            }
            case fp: FixedPoint => {
              RingElem.toWire(
                  (dp.asTypeOf(DspComplex(fp)) context_* that.value.asTypeOf(DspComplex(fp))).asInstanceOf[T]
              )
            }
            case _ => throw new UnsupportedOperationException(s"Unsupported complex type ${dp.real}")
          }

        }
      }
      case _ => {
        delayBinaryOp(that, Delay.getMultDelay(value))(_ * _)
      }
    }
  }

  /** Static left shifting. Can only be defined on type T inheriting from Bits */
  def <<(shift: Int) = {
    value match {
      case x: Bits => RingElem.toWire((x << shift).asInstanceOf[T])
      case _       => throw new UnsupportedOperationException(s"Can't shl values with type ${this.value}")
    }
  }

  /** Dynamic left shifting. Can only be defined on type T inheriting from Bits */
  def <<(shift: UInt) = {
    value match {
      case x: Bits => RingElem.toWire((x << shift).asInstanceOf[T])
      case _       => throw new UnsupportedOperationException(s"Can't shl values with type ${this.value}")
    }
  }

  /** Static right shifting. Can only be defined on type T inheriting from Bits */
  def >>(shift: Int) = {
    value match {
      case x: Bits => RingElem.toWire((x >> shift).asInstanceOf[T])
      case _       => throw new UnsupportedOperationException(s"Can't shr values with type ${this.value}")
    }
  }

  /** Dynamic right shifting. Can only be defined on type T inheriting from Bits */
  def >>(shift: UInt) = {
    value match {
      case x: Bits => RingElem.toWire((x >> shift).asInstanceOf[T])
      case _       => throw new UnsupportedOperationException(s"Can't shr values with type ${this.value}")
    }
  }

  /** Multiply this RingElem with a vector of compatible RingElem.
   *
   *  Vectors are represented as UInt bit vectors for simplicity.
   *
   *  @param that
   *    a vector of compatible RingElem represented as a bit vector (concatenated elements)
   */
  def *(that: UInt): UInt = {
    val vector = that.deserialize(this)
    val result = Wire(Vec(vector.length, this.cloneType))
    for (i <- 0 until vector.length) {
      result(i) := delayBinaryOp(vector(i), Delay.getMultDelay(value))(_ * _)
    }
    result.asUInt
  }

  /** Classic substraction between two compatible RingElem. */
  def -(that: RingElem[T]) = {
    delayBinaryOp(that, Delay.getAddDelay(value))(_ - _)
  }

  def unary_- = delayUnaryOp(Delay.getUnaryDelay(value))(-_)

  /** Used to delay output of d cycle
   *
   *  @param d
   *    number of cycle to delay output with
   */
  def delay(d: Int = 1) = {
    delayUnaryOp(d)(a => a)
  }

  /* Used to pipeline any binary operation on RingElem[T]
   *
   * @param that other RingElem to operate with
   * @param op function to apply on the 2 Ringelem inputs
   * @param delay pipeline level to use
   * @return the result of [[this]] [[op]] [[that]], delayed by [[delay]] */
  private def delayBinaryOp(that: RingElem[T], delay: Int)(op: (T, T) => T): RingElem[T] = {
    delay match {
      case 0 => RingElem.toWire(op(this.value, that.value))
      case x if x > 0 =>
        val regs = Reg(Vec(x, this.value.cloneType))
        regs(0) := op(this.value, that.value)
        for (i <- 1 until x) regs(i) := regs(i - 1)
        RingElem.toWire(regs(x - 1))
      case _ => throw new UnsupportedOperationException("Can't delay binary operation with negative delay")
    }
  }

  /* Used to pipeline any binary operation on RingElem[T]
   *
   * @param that other RingElem to operate with
   * @param op function to apply on the 2 Ringelem inputs
   * @param delay pipeline level to use
   * @return the result of [[this]] [[op]] [[that]], delayed by [[delay]] */
  private def delayUnaryOp(delay: Int)(op: T => T): RingElem[T] = {
    delay match {
      case 0 => RingElem.toWire(op(this.value))
      case x if x > 0 =>
        val regs = Reg(Vec(x, this.value.cloneType))
        regs(0) := op(this.value)
        for (i <- 1 until x) regs(i) := regs(i - 1)
        RingElem.toWire(regs(x - 1))
      case _ => throw new UnsupportedOperationException("Can't delay binary operation with negative delay")
    }
  }

  /** Type method to get neutral element for addition. */
  def zero: RingElem[T] = RingElem.toWire(Ring[T].zero)

  /** Type method to get neutral element for multiplication. */
  def one: RingElem[T] = RingElem.toWire(Ring[T].one)

  override def toString: String = s"${this.value.toString}"
  override def cloneType        = (RingElem(this.value.cloneType)).asInstanceOf[this.type]

  override def typeEquivalent(d: Data): Boolean = {
    d.isInstanceOf[RingElem[T]]
  }
}
