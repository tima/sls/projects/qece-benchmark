/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.software

import bench.utils.hardware.RingElem

import chisel3._
import chisel3.experimental._
import chisel3.internal.firrtl.KnownBinaryPoint
import dsptools.numbers._

/** Numeric class, representing a numeric value on the software side. Used for simulation. Value can either be BigInt or
 *  Double.
 *
 *  @param value
 *    the value to use, either BigInt or Double
 */
class Numeric(val intValue: Option[BigInt], val doubleValue: Option[Double], val complexValue: Option[Complex]) {

  /** Define add behavior for Numeric type
   *
   *  @param that
   *    second operand
   *  @return
   *    a new Numeric representing the result of the addition
   */
  def +(that: Numeric): Numeric = {
    (intValue, that.intValue, doubleValue, that.doubleValue, complexValue, that.complexValue) match {
      case (Some(thisVal), Some(thatVal), _, _, _, _) => Numeric(thisVal + thatVal)
      case (_, _, Some(thisVal), Some(thatVal), _, _) => Numeric(thisVal + thatVal)
      case (_, _, _, _, Some(thisVal), Some(thatVal)) => Numeric(thisVal + thatVal)
      case _                                          => throw new UnsupportedOperationException(s"Addition not possible between $this and $that")
    }
  }

  /** Define add behavior for Numeric type
   *
   *  @param that
   *    second operand as Int
   *  @return
   *    a new Numeric representing the result of the addition
   */
  def +(that: Int): Numeric = {
    (intValue, doubleValue) match {
      case (Some(_), _) => this + Numeric(that)
      case (_, Some(_)) => this + Numeric(that.toDouble)
      case _            => throw new UnsupportedOperationException(s"Addition not possible between $this and $that")
    }
  }

  /** Define sub behavior for Numeric type
   *
   *  @param that
   *    second operand
   *  @return
   *    a new Numeric representing the result of the substraction
   */
  def -(that: Numeric): Numeric = {
    (intValue, that.intValue, doubleValue, that.doubleValue, complexValue, that.complexValue) match {
      case (Some(thisVal), Some(thatVal), _, _, _, _) => Numeric(thisVal - thatVal)
      case (_, _, Some(thisVal), Some(thatVal), _, _) => Numeric(thisVal - thatVal)
      case (_, _, _, _, Some(thisVal), Some(thatVal)) => Numeric(thisVal - thatVal)
      case _                                          => throw new UnsupportedOperationException(s"Substraction not possible between $this and $that")
    }
  }

  /** Define sub behavior for Numeric type
   *
   *  @param that
   *    second operand as Int
   *  @return
   *    a new Numeric representing the result of the addition
   */
  def -(that: Int): Numeric = {
    (intValue, doubleValue) match {
      case (Some(_), _) => this - Numeric(that)
      case (_, Some(_)) => this - Numeric(that.toDouble)
      case _            => throw new UnsupportedOperationException(s"Substraction not possible between $this and $that")
    }
  }

  /** Define mult behavior for Numeric type
   *
   *  @param that
   *    second operand
   *  @return
   *    a new Numeric representing the result of the multiplication
   */
  def *(that: Numeric): Numeric = {
    (intValue, that.intValue, doubleValue, that.doubleValue, complexValue, that.complexValue) match {
      case (Some(thisVal), Some(thatVal), _, _, _, _) => Numeric(thisVal * thatVal)
      case (_, _, Some(thisVal), Some(thatVal), _, _) => Numeric(thisVal * thatVal)
      case (_, _, _, _, Some(thisVal), Some(thatVal)) => Numeric(thisVal * thatVal)
      case _                                          => throw new UnsupportedOperationException(s"Multiplication not possible between $this and $that")
    }
  }

  /** Define mult behavior for Numeric type
   *
   *  @param that
   *    second operand as Int
   *  @return
   *    a new Numeric representing the result of the multiplication
   */
  def *(that: Int): Numeric = {
    (intValue, doubleValue) match {
      case (Some(_), _) => this * Numeric(that)
      case (_, Some(_)) => this * Numeric(that.toDouble)
      case _            => throw new UnsupportedOperationException(s"Multiplication not possible between $this and $that")
    }
  }

  /** Override toString method for printing. */
  override def toString: String = {
    (intValue, doubleValue, complexValue) match {
      case (Some(thisVal), _, _) => s"$thisVal"
      case (_, Some(thisVal), _) => s"$thisVal"
      case (_, _, Some(thisVal)) => s"$thisVal"
      case _                     => throw new UnsupportedOperationException(s"Can not find a value in $this")
    }
  }

  /** Retrieve double value of that numeric, if available. Throws an exception if not. */
  private def getDouble: Double = {
    doubleValue match {
      case Some(v) => v
      case _       => throw new UnsupportedOperationException(s"Can't find Double value in $this")
    }
  }

  /** Retrieve BigInt value of that numeric, if available. Throws an exception if not. */
  private def getBigInt: BigInt = {
    intValue match {
      case Some(v) => v
      case _       => throw new UnsupportedOperationException(s"Can't find BigInt value in $this")
    }
  }

  /** Retrieve Complex value of that numeric, if available. Throws an exception if not. */
  private def getComplex: Complex = {
    complexValue match {
      case Some(v) => v
      case _       => throw new UnsupportedOperationException(s"Can't find BigInt value in $this")
    }
  }

  /** Define maxValue that can be represented on a BigInt, if the signal is an integer. Throws an exception if it is
   *  not.
   *
   *  @param signal
   *    type of hardware Data used
   *  @return
   *    maximum value that can be represented on signal.
   */
  private def maxValue(signal: Data): Numeric = {
    val s = getInternalSignal(signal)
    s match {
      case _: UInt => Numeric(BigInt((scala.math.pow(2, signal.getWidth) - 1).toInt))
      case _: SInt => Numeric(BigInt((scala.math.pow(2, signal.getWidth - 1) - 1).toInt))
      case fp: FixedPoint =>
        fp.binaryPoint match {
          case KnownBinaryPoint(bp) => Numeric(scala.math.pow(2, signal.getWidth - bp - 1) - 1)
          case _                    => throw new UnsupportedOperationException(s"Can't find binary point in $fp")
        }
      case _ => throw new UnsupportedOperationException(s"No max on $signal")
    }
  }

  /** Define minValue that can be represented on a BigInt, if the signal is an integer. Throws an exception if it is
   *  not.
   *
   *  @param signal
   *    type of hardware Data used
   *  @return
   *    minimum value that can be represented on signal.
   */
  private def minValue(signal: Data): Numeric = {
    val s = getInternalSignal(signal)
    s match {
      case _: UInt => Numeric(BigInt(0))
      case _: SInt => Numeric(BigInt(-scala.math.pow(2, signal.getWidth - 1).toInt))
      case fp: FixedPoint =>
        fp.binaryPoint match {
          case KnownBinaryPoint(bp) => Numeric(-scala.math.pow(2, signal.getWidth - bp - 1))
          case _                    => throw new UnsupportedOperationException(s"Can't find binary point in $fp")
        }
      case _ => throw new UnsupportedOperationException(s"No min on $signal")
    }
  }

  /** Detect an overflow on the Numeric computations
   *
   *  @param signal
   *    type of hardware Data used.
   *  @return
   *    weither this has overflowed during computation or not.
   */
  def detectOverflow(signal: Data): Boolean = {
    val s = getInternalSignal(signal)
    s match {
      case _: UInt | _: SInt =>
        this.intValue match {
          case Some(bi) => !((bi >= minValue(signal).getBigInt) && (bi <= maxValue(signal).getBigInt))
          case _        => throw new UnsupportedOperationException(s"Can't find overflow on $signal from $this")
        }
      case _: FixedPoint =>
        this.doubleValue match {
          case Some(d) => (d.isNaN || d.isInfinite || d > maxValue(signal).getDouble || d < minValue(signal).getDouble)
          case _       => throw new UnsupportedOperationException(s"Can't find overflow on $signal from $this")
        }
      case hc: DspComplex[_] =>
        this.complexValue match {
          case Some(c) => c.real.detectOverflow(hc.real) || c.imag.detectOverflow(hc.imag)
          case _       => throw new UnsupportedOperationException(s"Can't find overflow on $signal from $this")
        }
      case _ => throw new UnsupportedOperationException(s"Can't find overflow on $signal : type not supported")
    }
  }

  /** Convert this to a BigInt representing the bit vector
   *
   *  @param signal
   *    type of hardware Data used
   *  @return
   *    a BigInt which can be poked to any design using signal as data Type
   */
  def toBits(signal: Data): BitVector = {
    val s = getInternalSignal(signal)
    s match {
      case _: UInt | _: SInt =>
        this.intValue match {
          case Some(bi) => BitVector(bi)(signal)
          case _ =>
            throw new UnsupportedOperationException(s"Can't cast $this to BitVector with underlying type $signal")
        }
      case fp: FixedPoint =>
        this.doubleValue match {
          case Some(d) =>
            fp.binaryPoint match {
              case KnownBinaryPoint(bp) => BitVector(FixedPoint.toBigInt(d, bp))(signal)
              case _                    => throw new UnsupportedOperationException(s"Can't find binary point in $fp")
            }
          case _ =>
            throw new UnsupportedOperationException(s"Can't cast $this to BitVector with underlying type $signal")
        }
      case hc: DspComplex[_] =>
        this.complexValue match {
          case Some(c) =>
            BitComplex(
                BitVector(c.real.toBits(hc.real))(hc.real),
                BitVector(c.imag.toBits(hc.imag))(hc.imag)
            ).toBitVector
          case _ =>
            throw new UnsupportedOperationException(s"Can't cast $this to BitVector with underlying type $signal")
        }
      case _ => throw new UnsupportedOperationException(s"Unrecognized type $signal")
    }
  }

  private def getInternalSignal(signal: Data): Data = {
    signal match {
      case c: RingElem[_] => c.value
      case _              => signal
    }
  }

  /** Check equality between this Numeric (soft representation) and the BitVector representation Comparison can be bit
   *  true (integer operations) or threshold sensitive
   *
   *  @param other
   *    the hardware representation to check against
   *  @return
   *    weither the two values are equals or not
   */
  def ==(other: BitVector)(implicit c: Comparator): Boolean = {
    c.compare(this, other)
  }

  def toRingElem[T <: Data: Ring](signal: RingElem[T]): RingElem[T] = {
    val w = signal.getWidth.W
    signal.value match {
      case _: UInt => RingElem.toWire(getBigInt.asUInt(w).asInstanceOf[T])
      case _: SInt => RingElem.toWire(getBigInt.asSInt(w).asInstanceOf[T])
      case fp: FixedPoint =>
        (fp.binaryPoint, fp.getWidth) match {
          case (KnownBinaryPoint(bp), w) =>
            RingElem.toWire(FixedPoint(FixedPoint.toBigInt(getDouble, bp), w.W, bp.BP).asInstanceOf[T])
          case _ => throw new UnsupportedOperationException(s"Unset binary point for signal $signal")
        }
      case hc: DspComplex[_] =>
        val sc = getComplex
        (hc.real, hc.imag) match {
          case (r: UInt, i: UInt) =>
            RingElem.toWire(
                DspComplex(
                    sc.real.getBigInt.asUInt(r.getWidth.W),
                    sc.real.getBigInt.asUInt(i.getWidth.W)
                ).asInstanceOf[T]
            )
          case (r: SInt, i: SInt) =>
            RingElem.toWire(
                DspComplex(
                    sc.real.getBigInt.asSInt(r.getWidth.W),
                    sc.real.getBigInt.asSInt(i.getWidth.W)
                ).asInstanceOf[T]
            )
          case (rfp: FixedPoint, _: FixedPoint) =>
            (rfp.binaryPoint, rfp.getWidth) match {
              case (KnownBinaryPoint(bp), w) =>
                RingElem.toWire(
                    DspComplex(
                        FixedPoint(FixedPoint.toBigInt(sc.real.getDouble, bp), w.W, bp.BP),
                        FixedPoint(FixedPoint.toBigInt(sc.imag.getDouble, bp), w.W, bp.BP)
                    ).asInstanceOf[T]
                )
              case _ => throw new UnsupportedOperationException(s"Unset binary point for signal $rfp")
            }
          case _ =>
            throw new UnsupportedOperationException(
                s"Unkown inner type for DspComplex with type (${hc.real}, ${hc.imag})"
            )
        }
      case _ => throw new UnsupportedOperationException(s"Unrecognized hardware type $signal")
    }
  }
}

/** Factory for [[Numeric]] type. */
object Numeric {
  private val rand = scala.util.Random

  /** Create a Numeric using BigInt representation. Shall be used for bit true simulation and validation.
   *
   *  @param value
   *    BigInt to be used in this Numeric
   */
  def apply(value: BigInt) = new Numeric(Some(value), None, None)

  /** Create a Numeric using Int representation. Shall be used for bit true simulation and validation.
   *
   *  @param value
   *    Int to be used in this Numeric
   */
  def apply(value: Int) = new Numeric(Some(BigInt(value)), None, None)

  /** Create a Numeric using Double representation. Shall be used for approximate simulation and validation.
   *
   *  @param value
   *    Double to be used in this Numeric
   */

  def apply(value: Double) = new Numeric(None, Some(value), None)

  def apply(value: Complex) = new Numeric(None, None, Some(value))

  /** Generate a random sample for simulation
   *
   *  @param signal
   *    the hardware type to be used in the design.
   *  @param max
   *    the maximum value that this function can return
   *  @return
   *    a random sample as a Numeric instance
   */
  def random(signal: Data, max: Int = 10): Numeric = {
    signal match {
      case _: UInt          => Numeric(BigInt(rand.nextInt(max)))
      case _: SInt          => Numeric(BigInt(rand.nextInt(max) - rand.nextInt(max)))
      case _: FixedPoint    => Numeric(rand.nextDouble() * max - rand.nextDouble() * max)
      case c: DspComplex[_] => Numeric(Complex(Numeric.random(c.real, max), Numeric.random(c.imag, max)))
      case _                => throw new UnsupportedOperationException(s"Unrecognized type when generating sample : $signal")
    }
  }
}
