/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.software

import chisel3._
import chisel3.experimental._
import dsptools.numbers._

/** A comparator to check software vs hardware representation.
 *
 *  Comparison can be bit true (e.g. integer types) or approximate (e.g. fixed point)
 *
 *  @param t
 *    the Numeric threshold to be used when comparing Double to FixedPoint
 */
case class Comparator(threshold: Numeric) {

  /** Compare softRepr and hardRepr to know if they can be considered equals, weither bit true or w.r.t the threshold
   *  defined.
   *
   *  @param softRepr
   *    Numeric representation used for software computations
   *  @param hardRepr
   *    BitVector representation used for hardware peek/poke
   */
  def compare(softRepr: Numeric, hardRepr: BitVector): Boolean = {
    hardRepr.getType match {
      case _: UInt | _: SInt =>
        softRepr.intValue match {
          case Some(bi) => bi == hardRepr.value
          case _        => throw new UnsupportedOperationException(s"Bit true comparison can only work on integer values.")
        }
      case _: FixedPoint =>
        (softRepr.doubleValue, threshold.doubleValue) match {
          case (Some(d), Some(t)) => scala.math.abs(d - hardRepr.toDouble) <= t
          case _                  => throw new UnsupportedOperationException(s"Can't compare values that are not both double")
        }
      case _: DspComplex[_] =>
        (softRepr.complexValue) match {
          case Some(c) =>
            val bc = BitComplex(hardRepr)
            compare(c.real, bc.real) && compare(c.imag, bc.imag)
          case _ => throw new UnsupportedOperationException(s"Can't compare values that are not both Complex")
        }
      case _ => throw new UnsupportedOperationException("Unknown type used in comparison")
    }
  }
}

object Comparator {
  def apply(t: Double): Comparator = Comparator(Numeric(t))
}
