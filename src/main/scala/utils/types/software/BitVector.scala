/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.software

import bench.utils.convert._

import chisel3._
import chisel3.experimental._
import chisel3.internal.firrtl.KnownBinaryPoint

import dsptools.numbers._

/** A vector of bit, represented as a BigInt
 *
 *  It is a wrapping of the BigInt type used by Chisel to represent bit vectors.
 *
 *  This way, when poking/peeking a BigInt, implicit signal can be used in the representation to know the hardware type
 *  corresponding to the BigInt
 */
class BitVector(val value: BigInt)(implicit val signal: Data) {

  /** Cast this BitVector to a Double representation, for printing and comparison purposes. */
  def toDouble: Double = {
    signal match {
      case fp: FixedPoint =>
        fp.binaryPoint match {
          case KnownBinaryPoint(bp) => value.toDouble / scala.math.pow(2, bp.toDouble)
          case _                    => throw new UnsupportedOperationException("Can't find binary point")
        }
      case _ => throw new UnsupportedOperationException("Can't convert to Fixed Point when signal is not")
    }
  }

  override def toString: String = {
    signal match {
      case _: UInt | _: SInt => s"$value"
      case _: FixedPoint     => s"${this.toDouble}"
      case _: DspComplex[_]  => s"${BitComplex(this)} ($value)"
      case _                 => throw new UnsupportedOperationException(s"Unknown signal ${signal} type for BitVector")
    }
  }

  def getType = signal
}

/** Factory for [[BitVector]] class. */
object BitVector {
  def apply(value: BigInt)(implicit signal: Data) = new BitVector(value)(signal)

  implicit def BigIntToBitVector(value: BigInt)(implicit signal: Data) = BitVector(value)(signal)
  implicit def BitVectorToBigInt(value: BitVector)                     = value.value

  /** Create a mask
   *
   *  @param width
   *    width of the desired mask
   *  @return
   *    a BigInt on width bits, with all bits set to 1.
   */
  def createMask(width: Int)(implicit signal: Data): BitVector = {
    BitVector(maxUInt(width))
  }

  /** Return the max value (unsigned integer) representable on width bits. */
  private def maxUInt(width: Int): BigInt = {
    BigInt(2).pow(width) - 1
  }

  /** Serialize an Array[BitVector] array to a BitVector. */
  def serialize(elements: Array[BitVector]): BitVector = {
    /* Implcit signal used for implicit conversion to Bigint in computations */
    implicit val signal = getType(elements)
    val elemWidth       = signal.getWidth
    BitVector(getSerializedValue(elements))(UInt((elemWidth * elements.length).W))
  }

  def getSerializedValue(elements: Array[BitVector]): BigInt = {
    implicit val signal = getType(elements)
    val elemWidth       = signal.getWidth
    val mask            = createMask(elemWidth)
    var result          = BigInt(0)
    elements.map { e => result = (result << elemWidth) | (e & mask) }
    result
  }

  /** Find the inside type of an Array[BitVector] */
  private def getType(elements: Array[BitVector]) = {
    /* Check that every element in array is of same type. */
    elements.reduce((a, b) =>
      if (a.signal =:= b.signal) a
      else throw new UnsupportedOperationException(s"Can't serialize an array with equal types")
    )
    elements(0).signal
  }

  /** Small trick when deserializing signed integers and FixedPoint from bit vector (as UInt).
   *
   *  BigInt format considerate a large integer instead of a below zero value when using bitwise operations to
   *  deserialize bit vectors to signed integers or FixedPoint.
   */
  private def reinterpret(value: BigInt)(implicit signal: Data): BitVector = {
    signal match {
      case _: UInt => BitVector(value)
      case fp: FixedPoint =>
        fp.binaryPoint match {
          case KnownBinaryPoint(bp) =>
            val width = signal.getWidth
            // Detect an overflow on FixedPoint representation, and correct it.
            if (BitVector(value).toDouble >= maxUInt(width - bp - 1).toDouble) {
              val doubleValue = -(maxUInt(width - bp) + 1).toDouble + BitVector(value).toDouble
              BitVector(BigDecimal(doubleValue * scala.math.pow(2, bp)).toBigInt)
            } else BitVector(value)
          case _ => throw new UnsupportedOperationException(s"Can't find binary point on signal $signal")
        }
      case _: SInt =>
        val width = signal.getWidth
        // Detect an overflow on SInt representation, and correct it.
        if (value >= maxUInt(width - 1)) BitVector(-(maxUInt(width) + 1 - value)) else BitVector(value)
      case hc: DspComplex[_] =>
        val c = BitComplex(value)
        BitComplex(reinterpret(c.real)(hc.real), reinterpret(c.imag)(hc.imag)).toBitVector
      case _ => throw new UnsupportedOperationException(s"Unknown signal ${signal} type for BitVector")
    }
  }

  def deserialize(data: BitVector, dataWidth: Int, elemWidth: Int)(implicit signal: Data): Array[BitVector] = {
    val mask = createMask(elemWidth)
    (0 until dataWidth / elemWidth).toArray.map { i =>
      reinterpret(((data & (mask << elemWidth * i)) >> elemWidth * i))(signal)
    }.reverse
  }
}
