/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.software

/* Complex representation for software computing
 *
 * We use the Numeric type here, but it should always be double.
 *
 * @param re real part of the complex
 * @param im imaginary part of the complex
 * @return a complex representation with operations defined on it */
class Complex(val real: Numeric, val imag: Numeric) {
  def +(rhs: Complex) = new Complex(real + rhs.real, imag + rhs.imag)
  def -(rhs: Complex) = new Complex(real - rhs.real, imag - rhs.imag)
  def *(rhs: Complex) = new Complex(real * rhs.real - imag * rhs.imag, rhs.real * imag + real * rhs.imag)

  override def toString = s"[$real + i*$imag]"
}

object Complex {

  /* Create a complex from two Numerics
   *
   * @param re real part
   * @param im imaginary part */
  def apply(re: Numeric, im: Numeric): Complex = {
    new Complex(re, im)
  }

  /* Create a complex from two BigInts
   *
   * @param re real part
   * @param im imaginary part */
  def apply(re: BigInt, im: BigInt): Complex = {
    Complex(Numeric(re), Numeric(im))
  }

  /* Create a complex from two Doubles
   *
   * @param re real part
   * @param im imaginary part */
  def apply(re: Double, im: Double): Complex = {
    Complex(Numeric(re), Numeric(im))
  }
}
