/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.software

import bench.utils.convert._

import chisel3._
import chisel3.experimental._
import dsptools.numbers._

class BitComplex(val real: BitVector, val imag: BitVector) {
  require(
      real.signal =:= imag.signal,
      s"Real and imaginary parts should be of same hardware type. Found (${chiselTypeOf(real.signal)}, ${chiselTypeOf(imag.signal)})"
  )
  override def toString: String = s"[$real + i*$imag]"

  def toBitVector: BitVector = {
    real.signal match {
      case u: UInt        => BitVector(BitVector.getSerializedValue(Array(real, imag)))(DspComplex(u))
      case s: SInt        => BitVector(BitVector.getSerializedValue(Array(real, imag)))(DspComplex(s))
      case fp: FixedPoint => BitVector(BitVector.getSerializedValue(Array(real, imag)))(DspComplex(fp))
      case _              => throw new UnsupportedOperationException(s"Signal ${real.signal} of $this is unknown")
    }
  }
}

object BitComplex {
  def apply(real: BitVector, imag: BitVector): BitComplex = new BitComplex(real, imag)

  /* Deserialize a BitVector representing a complex, to create a BitComplex */
  def apply(vector: BitVector): BitComplex = {
    vector.signal match {
      case hc: DspComplex[_] =>
        val asArray = BitVector.deserialize(vector, hc.getWidth, hc.real.getWidth)(hc.real)
        new BitComplex(asArray(0), asArray(1))
      case _ =>
        throw new UnsupportedOperationException(
            s"Can't deserialize a BitVector with underlying type ${vector.signal} - different from DspComplex"
        )
    }
  }
}
