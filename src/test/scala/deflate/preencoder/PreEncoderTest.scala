/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.preencoder

import bench.utils.exceptions.RepresentationOverflowException

import bench.tags._

import logger.LazyLogging

import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, PeekPokeTester}

class PreEncoderTest(dut: PreEncoder) extends PeekPokeTester(dut) {

  val delay = dut.getPipelineLevel

  def lengthVerif(code: Int, extraBits: Int): Int = {
    val ref = Array(
        3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 17, 19, 23, 27, 31, 35, 43, 51, 59, 67, 83, 99, 115, 131, 163, 195, 227,
        258
    )
    if (code >= 257) {
      ref(code - 257) + extraBits
    } else {
      0
    }
  }

  def distanceVerif(code: Int, extraBits: Int): Int = {
    val ref = Array(
        1, 2, 3, 4, 5, 7, 9, 13, 17, 25, 33, 49, 65, 97, 129, 193, 257, 385, 513, 769, 1025, 1537, 2049, 3073, 4097,
        6145, 8193, 12289, 16385, 24577
    )
    if (code > 29) {
      0
    } else {
      ref(code) + extraBits
    }
  }

  // Test chars
  for (char <- 0 to 255 + delay) {
    poke(dut.io.char, char)
    poke(dut.io.length, 0)
    if (char > delay) {
      expect(
          peek(dut.io.literalCode.code).toInt == (char - delay),
          s"Wrong char code (expected '" + (char - delay) +
            "' but received '" + peek(dut.io.literalCode.code).toChar + "')"
      )
    }
    step(1)
  }

  // Test length
  poke(dut.io.position, 5)
  for (length <- 3 to 257 + delay) {
    poke(dut.io.length, length)
    val lengthCode      = peek(dut.io.literalCode.code).toInt
    val lengthExtraBits = peek(dut.io.literalCode.extraBits).toInt
    if (length - 3 > delay) {
      expect(
          lengthVerif(lengthCode, lengthExtraBits) == length - delay,
          s"Wrong code for length " + (length - delay) + s" (code returned $lengthCode)"
      )
    }
    step(1)
  }

  // Test distance
  poke(dut.io.length, 5)
  for (distance <- 0 to 32768 + delay) {
    poke(dut.io.position, distance)
    val distCode      = peek(dut.io.distanceCode.code).toInt
    val distExtraBits = peek(dut.io.distanceCode.extraBits).toInt
    if (distance > delay) {
      expect(
          distanceVerif(distCode, distExtraBits) == (distance - delay),
          s"Wrong code for distance " + (distance - delay) + s" (code returned $distCode)"
      )
    }
    step(1)
  }

  step(1)
}

object PreEncoderHelper extends LazyLogging {
  def verilatorTest(): Boolean = {
    try {
      logger.info(s"${this.getClass.getSimpleName} : Verilator tester")
      iotesters.Driver.execute(
          Array(
              "--backend-name",
              "verilator",
              "--fint-write-vcd",
              "--target-dir",
              "test_run_dir/test_preencoder",
              "--top-name",
              "PreEncoder"
          ),
          () => new PreEncoder(32)
      ) { c =>
        new PreEncoderTest(c)
      }
    } catch {
      case _: RepresentationOverflowException =>
        logger.warn("Overflow detected on design, not checking validity further")
        true
      // case _: Throwable => false
    }
  }
  def firrtlTest(): Boolean = {
    try {
      logger.info(s"${this.getClass.getSimpleName} : Treadle tester")
      iotesters.Driver.execute(
          Array(
              "--backend-name",
              "firrtl",
              "--fint-write-vcd",
              "--target-dir",
              "test_run_dir/test_preencoder",
              "--top-name",
              "PreEncoder"
          ),
          () => new PreEncoder(32)
      ) { c =>
        new PreEncoderTest(c)
      }
    } catch {
      case _: RepresentationOverflowException =>
        logger.warn("Overflow detected on design, not checking validity further")
        true
      //case _: Throwable => false
    }
  }
}

/** Used for implementation and debug. */
class PreEncoderSpecTester extends ChiselFlatSpec {
  "running verilator" should "validate your design" taggedAs (Unit) in {
    PreEncoderHelper.verilatorTest() should be(true)
  }
}
