/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.test.software.huffman

class HuffmanTree(table: Seq[(Int, Int)]) {
  val root = new HuffmanNode(false)
  createTree(table)

  def createTree(table: Seq[(Int, Int)]): Unit = {
    for ((symbol, index) <- table.zipWithIndex) {
      createLeaf(symbol, index)
    }
  }

  def createLeaf(symbol: (Int, Int), code: Int): Unit = {
    var node = root
    for (bitIndex <- 0 until symbol._1) { // (Little endian reading)
      val bit = (symbol._2 >> bitIndex) & 0x1
      node = getBranch(code, bit, node, bitIndex == symbol._1 - 1)
    }
  }

  private def getBranch(code: Int, bit: Int, parent: HuffmanNode, isLeaf: Boolean): HuffmanNode = {
    if (bit == 1) {
      if (parent.left == null) {
        parent.left = if (isLeaf) HuffmanNode(code) else HuffmanNode()
      } else if (parent.left.isLeaf) {
        throw new Exception("Attempt to create a child of a leaf (wrong Huffman construction)")
      }
      parent.left
    } else {
      if (parent.right == null) {
        parent.right = if (isLeaf) HuffmanNode(code) else HuffmanNode()
      } else if (parent.right.isLeaf) {
        throw new Exception("Attempt to create a child of a leaf (wrong Huffman construction)")
      }
      parent.right
    }
  }

  def getSymbol(bitIterator: Iterator[Int]): Int = {
    var node = root
    while (!node.isLeaf) {
      if (!bitIterator.hasNext) return -1
      node = if (bitIterator.next() == 0) node.right else node.left
      if (node == null) return -1
    }
    node.code
  }

}

class HuffmanNode(val isLeaf: Boolean, val code: Int = 0, val size: Int = 0) {
  var left: HuffmanNode  = null
  var right: HuffmanNode = null
}

object HuffmanNode {
  def apply(): HuffmanNode = {
    new HuffmanNode(false, 0)
  }
  def apply(code: Int): HuffmanNode = {
    new HuffmanNode(true, code)
  }
}

object HuffmanTreeTest extends App {
  val array = Array((8, 9), (9, 19))
  val tree  = new HuffmanTree(array)
}
