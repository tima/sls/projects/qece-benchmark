/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.test.software.deflate

import bench.kernels.deflate.huffman.HuffmanTable
import bench.kernels.deflate.test.software.huffman.HuffmanTree
import bench.kernels.deflate.test.software.tools.BitIterator
import bench.kernels.deflate.tools.{BitReverse, Constants}

/** Instantiate a reader that can decode Huffman encode
 *
 *  @param literalTable
 *    @param distanceTable
 *  @param wordIterator
 *    @param wordSize
 */
class DeflateDecode(
    literalTable: Seq[(Int, Int)],
    distanceTable: Seq[(Int, Int)],
    wordIterator: Iterator[BigInt],
    wordSize: Int
) extends logger.LazyLogging {

  val huffLiteral  = new HuffmanTree(literalTable)
  val huffDistance = new HuffmanTree(distanceTable)
  val rawDecoded   = new StringBuilder()
  val dataDecoded  = new StringBuilder()

  val bitIterator = BitIterator(wordIterator, wordSize / 8)

  private def readBits(nbBits: Int, reverse: Boolean = false): Int = {
    var result = 0
    for (i <- 0 until nbBits) {
      if (bitIterator.hasNext == false) return -1
      result = result | (bitIterator.next() << i)
    }
    if (reverse) BitReverse(result, nbBits) else result
  }

  def readHeader(): Unit = {
    val bFinal = readBits(1)
    if (bFinal == 1) logger.info("[LAST BLOCK]") else logger.info("[NEW BLOCK WILL ARRIVE][NOT SUPPORTED]")
    val bType = readBits(2)
    bType match {
      case Constants.BTYPE_NO_COMPRESSION => logger.info("[NO COMPRESSION][NOT SUPPORTED]")
      case Constants.BTYPE_FIXED_HUFFMAN  => logger.info("[FIXED HUFFMAN]")
      case Constants.BTYPE_DYN_HUFFMAN    => logger.info("[DYN HUFFMAN][NOT SUPPORTED]")
    }
    logger.info("")
  }

  def readSymbol(): Boolean = {
    val code = huffLiteral.getSymbol(bitIterator)
    if (code == -1) {
      rawDecoded.append("[Error : symbol not found]")
      dataDecoded.append("[Error : symbol not found]")
    } else if (code == 256) {
      rawDecoded.append("[END OF BLOCK]")
      return true
    } else if (code < 256) {
      rawDecoded.append(code.toChar)
      dataDecoded.append(code.toChar)
    } else if (code > 256) {
      val length =
        DeflateDecode.decodeLiteral(code, readBits(DeflateDecode.getLiteralExtraBitsLength(code), false))
      val distCode = huffDistance.getSymbol(bitIterator)
      val dist =
        DeflateDecode.decodeDistance(
            distCode,
            readBits(DeflateDecode.getDistanceExtraBitsLength(distCode), false)
        )

      if (distCode == -1) {
        rawDecoded.append("[Error : distance not found]")
        dataDecoded.append("[Error : distance not found]")
      } else {
        rawDecoded.append("[@" + dist + ":" + length + "]")
        if (dataDecoded.size - dist < 0) {
          dataDecoded.append("[ERROR]")
        }
        dataDecoded.append(dataDecoded.substring(dataDecoded.size - dist, dataDecoded.size - dist + length))
      }
    }
    false
  }

  def readAll(): Unit = {
    if (bitIterator.hasNext) readHeader()
    while (bitIterator.hasNext) {
      if (readSymbol()) {
        return
      }
    }
  }

  def printDecoded(): Unit = {
    logger.info("RAW  : " + rawDecoded.toString())
    logger.info("DATA : " + dataDecoded.toString())
  }
}

object DeflateDecode {

  private def getExtraBitsLength(code: Int, codeBoundaries: Seq[Int]): Int = {
    for ((boundary, index) <- codeBoundaries.zipWithIndex) {
      if (code < boundary) {
        return if (index == 0) 0 else index - 1
      }
    }
    -1
  }

  private def decode(
      code: Int,
      extraBits: Int,
      extraBitLength: Int,
      codeBoundaries: Seq[Int],
      boundaries: Seq[Int]
  ): Int = {
    if (code < codeBoundaries(0)) return code

    for (index <- 0 until codeBoundaries.size) {
      if (code < codeBoundaries(index)) {
        return boundaries(index - 1) +
          (code - codeBoundaries(index - 1)) * math.pow(2, extraBitLength).toInt + extraBits
      }
    }
    -1
  }

  def getLiteralExtraBitsLength(code: Int): Int = {
    getExtraBitsLength(code, Constants.CODE_LENGTH_BOUNDARIES)
  }

  def getDistanceExtraBitsLength(code: Int): Int = {
    getExtraBitsLength(code, Constants.CODE_DISTANCE_BOUNDARIES)
  }

  def decodeLiteral(code: Int, extraBits: Int): Int = {
    decode(
        code,
        extraBits,
        getLiteralExtraBitsLength(code),
        Constants.CODE_LENGTH_BOUNDARIES,
        Constants.LENGTH_BOUNDARIES
    )
  }

  def decodeDistance(code: Int, extraBits: Int): Int = {
    decode(
        code,
        extraBits,
        getDistanceExtraBitsLength(code),
        Constants.CODE_DISTANCE_BOUNDARIES,
        Constants.DISTANCE_BOUNDARIES
    )
  }
}

object HuffmanReaderTest extends App {
  val literalTable = HuffmanTable.fixedLiteralTable()

  val sentence = Array[Int]('a', 'b', 'c', 258, 1)

  val reader = new DeflateDecode(
      literalTable,
      HuffmanTable.fixedDistanceTable(),
      sentence.map(value => BigInt(literalTable(value)._2)).toIterator,
      8
  )
  reader.readAll()
}
