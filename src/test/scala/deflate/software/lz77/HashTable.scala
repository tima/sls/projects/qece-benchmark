/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.test.software.lz77

class HashTable(size: Int, hashFunction: HashFunction) {

  private val table = new Array[(Int, Array[Byte])](size)

  def hashData(data: Array[Byte]): Int = {
    hashFunction.hash(BigInt(data).toLong)
  }

  def put(addr: Int, data: Array[Byte]): Unit = {
    table(hashData(data)) = (addr, data)
  }

  def get(data: Array[Byte]): (Int, Array[Byte]) = {
    table(hashData(data))
  }

  def swap(addr: Int, data: Array[Byte]): (Int, Array[Byte]) = {
    val result = get(data)
    put(addr, data)
    result
  }

  def print(): Unit = {
    for (i <- 0 until size) {
      val value = table(i)
      if (value != null) {
        println("entry " + i + " :\taddr " + value._1 + " :\t" + new String(value._2))
      }
    }
  }
}

object HashTableTest extends App with logger.LazyLogging {
  val table = new HashTable(256, new FibonacciHashing32(256))
  table.put(42, Array(1, 2, 3))
  table.put(10, Array(4, 5, 6))
  logger.info("Table : " + table.get(Array(1, 2, 3)))
}
