/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.test.software.lz77

class InputBuffer(byteSize: Int) {
  private val buffer     = new Array[Byte](byteSize)
  private var writeIndex = 0
  private var readIndex  = 0

  def put(byte: Byte): Unit = {
    buffer(writeIndex) = byte
    writeIndex = (writeIndex + 1) % byteSize
  }

  def get3Bytes(addr: Int): Array[Byte] = {
    Array(buffer(addr), buffer((addr + 1) % byteSize), buffer((addr + 2) % byteSize))
  }

  def currentIndex: Int = readIndex

  def canRead(length: Int): Boolean = {
    if (readIndex > writeIndex) {
      byteSize - readIndex + writeIndex >= length
    } else {
      writeIndex - readIndex >= length
    }
  }

  def read3Bytes(): Array[Byte] = {
    val result = get3Bytes(readIndex)
    readIndex = (readIndex + 1) % byteSize
    result
  }

  def skip(length: Int): Unit = {
    if (canRead(length)) {
      readIndex += length
    }
  }
}
