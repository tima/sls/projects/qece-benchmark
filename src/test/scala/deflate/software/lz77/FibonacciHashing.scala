/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.test.software.lz77

import chisel3.util.log2Ceil

import spire.math.UInt

trait HashFunction {
  def hash(value: Long): Int
}

class FibonacciHashing32(HTSize: Int) extends HashFunction {
  private val log2HTSize = log2Ceil(HTSize)
  private val factor     = UInt(2654435769L) // = 2^32 / golden_ratio
  private val shift      = 32 - log2HTSize

  def hash(value: Long): Int = {
    (factor * UInt(value) >> shift).toInt
  }
}

object FibonacciHashingTest extends App with logger.LazyLogging {
  val hashFunction = new FibonacciHashing32(256)
  for (i <- 0 until 10) {
    logger.info("Hashing " + i + " : " + hashFunction.hash(i))
  }
}
