/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.test.software.lz77

class LZ77 extends logger.LazyLogging {
  val buffer   = new InputBuffer(16384)
  val dict     = new HashTable(256, new FibonacciHashing32(256))
  var matching = false
  var position = 0
  var length   = 0

  def put(string: String): Unit = {
    string.foreach(char => buffer.put(char.toByte))
  }

  def step(): Unit = {
    if (!buffer.canRead(3)) {
      logger.warn("Not enought data to read")
      return
    }

    val index = buffer.currentIndex
    val data  = buffer.read3Bytes()

    val result = dict.swap(index, data)

    if (result != null && data.sameElements(result._2)) {
      logger.debug("Matched word : " + new String(data) + " (" + result._1 + ")")
      if (matching) {
        length += 1
      } else {
        matching = true
        position = index - result._1
        length = 3
      }
    } else {
      if (matching) {
        print("@(" + position + ", " + length + ")")
        buffer.skip(1)
      } else {
        print(new String(Array(data(0))))
      }
      matching = false
    }
  }
}

object LZ77Test extends App with logger.LazyLogging {
  val lz77 = new LZ77()
  lz77.put(
      "That apple is our best apple isn't it ? Do you have any idea how much it costs ? Yes I have an idea : it will cost about 5€ and it's too much !"
  )
  while (lz77.buffer.canRead(3)) {
    lz77.step()
  }

  //lz77.dict.print()
  logger.debug("hash value " + lz77.dict.hashData(Array[Byte](' ', 'i', 's')))
}
