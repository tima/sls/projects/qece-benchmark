/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.test.software.tools

class CRC32 {
  // Using RFC1952 implementation
  // https://dl.acm.org/doi/pdf/10.17487/RFC1952
  val crc32Table = makeCRC32Table()
  var crc: Long  = 0

  def makeCRC32Table(): Array[Long] = {
    val table = new Array[Long](256)
    for (n <- 0 until table.size) {
      var c: Long = n
      for (_ <- 0 until 8) {
        if ((c & 1) == 1) {
          c = 0xedb88320L ^ (c >> 1)
        } else {
          c = c >> 1
        }
      }
      table(n) = c
    }
    table
  }

  def update(byte: Byte): Unit = {
    var c: Long = crc ^ 0xffffffffL
    c = crc32Table(((c ^ byte) & 0xff).toInt) ^ (c >> 8)
    crc = c ^ 0xffffffffL
  }

  def update(array: Array[Byte]): Unit = {
    for (byte <- array) {
      update(byte)
    }
  }

  def update(value: String): Unit = {
    for (char <- value) {
      update(char.toByte)
    }
  }

  def toByteArray: Array[Byte] = {
    val array = new Array[Byte](4)
    for (i <- 0 until array.size) {
      array(i) = ((crc >> (8 * i)) & 0xff).toByte
    }
    array
  }

}
