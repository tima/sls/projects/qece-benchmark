/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.test.software.tools

class ByteIterator(wordIterator: Iterator[BigInt], nbBytes: Int) extends Iterator[Byte] {
  var bytes: Iterator[Byte] = getNextBytes()

  def getNextBytes(): Iterator[Byte] = {
    var array = wordIterator.next().toByteArray
    if (array.size > nbBytes) {
      array = array.slice(1, array.size)
    } else if (array.size < nbBytes) {
      array = Array.concat(Array.fill(nbBytes - array.size)(0x00), array)
    }
    if (wordIterator.hasNext == false) {
      for (i <- array.size - 1 to 0 by -1) {
        if (array(i) == -1) {
          return array.slice(0, i).iterator
        }
      }
      throw new Exception("The over-padding pattern was not found on the last word.")
    }
    array.toIterator
  }

  override def hasNext: Boolean = {
    if (bytes.hasNext == false) {
      if (wordIterator.hasNext == false) {
        return false
      } else {
        bytes = getNextBytes()
      }
    }
    bytes.hasNext
  }

  override def next(): Byte = {
    if (bytes.hasNext == false && wordIterator.hasNext) {
      bytes = getNextBytes()
    }
    bytes.next()
  }
}
