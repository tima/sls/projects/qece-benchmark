/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.test.software.tools

class BitIterator(byteIterator: Iterator[Byte]) extends Iterator[Int] {
  var bitIndex = 8
  var byte     = 0

  override def hasNext: Boolean = {
    !(bitIndex == 8 && byteIterator.hasNext == false)
  }

  override def next(): Int = { // (LSB first reading)
    if (bitIndex == 8) {
      byte = byteIterator.next()
      bitIndex = 0
    }
    val bit = (byte >> bitIndex) & 0x1
    bitIndex += 1
    bit.toInt
  }
}

object BitIterator {
  def apply(wordIterator: Iterator[BigInt], nbBytes: Int): BitIterator =
    new BitIterator(new ByteIterator(wordIterator, nbBytes))
}
