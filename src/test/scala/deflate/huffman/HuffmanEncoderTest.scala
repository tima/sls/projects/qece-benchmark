/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.huffman

import bench.utils.tests.LoggedTester
import bench.utils.exceptions.RepresentationOverflowException

import bench.tags._

import logger.LazyLogging

import chisel3._
import chisel3.iotesters.ChiselFlatSpec

class HuffmanEncoderTest(
    dut: HuffmanEncoder,
    literalHuffCode: Array[(Int, Int)],
    distanceHuffCode: Array[(Int, Int)]
) extends LoggedTester(dut) {

  def bitsToString(bits: Int, size: Int): String = {
    val result = new StringBuilder()
    if (size > 0) {
      result.append(bits.toBinaryString)
      while (result.size < size) {
        result.insert(0, "0")
      }
      result.toString()
    } else {
      ""
    }

  }

  def huffmanCode(code: Int, extraBits: Int, extraBitsLength: Int): String = {
    val huff = literalHuffCode(code)
    bitsToString(huff._2, huff._1) + bitsToString(extraBits, extraBitsLength)
  }

  poke(dut.io.literalCode.code, 143)
  poke(dut.io.literalCode.extraBits, 5)
  poke(dut.io.literalCode.extraBitsLength, 4)
  step(2)

  debug("Ref : " + huffmanCode(143, 5, 4))
}

object HuffmanEncoderHelper extends LazyLogging {
  def verilatorTest(literalHuffCode: Array[(Int, Int)], distanceHuffCode: Array[(Int, Int)]): Boolean = {
    try {
      logger.info(s"${this.getClass.getSimpleName} : Verilator tester")
      iotesters.Driver.execute(
          Array(
              "--backend-name",
              "verilator",
              "--fint-write-vcd",
              "--target-dir",
              "test_run_dir/test_huffman_encoder",
              "--top-name",
              "HuffmanEncoder"
          ),
          () => new HuffmanEncoder(literalHuffCode, distanceHuffCode)
      ) { c =>
        new HuffmanEncoderTest(c, literalHuffCode, distanceHuffCode)
      }
    } catch {
      case _: RepresentationOverflowException =>
        logger.warn("Overflow detected on design, not checking validity further")
        true
      // case _: Throwable => false
    }
  }
}

/** Used for implementation and debug. */
class HuffmanEncoderSpecTester extends ChiselFlatSpec {

  "running verilator" should "validate your design" taggedAs (Unit) in {
    HuffmanEncoderHelper.verilatorTest(
        HuffmanTable.fixedLiteralTable(),
        HuffmanTable.fixedDistanceTable()
    ) should be(true)
  }
}
