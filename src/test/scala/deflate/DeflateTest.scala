/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate

import bench.kernels.deflate.huffman.HuffmanTable
import bench.kernels.deflate.tester.{DecodeAndPrint, GzipWrapper, StreamReader}

import bench.utils.hardware.RingElem
import bench.utils.tests.LatencyExtractTester
import bench.utils.components.PosEdgeWrapper
import bench.utils.exceptions._

import bench.tags._

import logger.LazyLogging

import chisel3._
import chisel3.iotesters
import chisel3.iotesters._
import dsptools.numbers._

object DeflateHelper extends LazyLogging {
  def verilatorDecodeAndPrint[T <: Data: Ring](
      gen: T,
      lz77Depth: Int,
      literalHuffCodes: Seq[(Int, Int)],
      distanceHuffCodes: Seq[(Int, Int)],
      sentence: String
  ): Boolean = {
    try {
      logger.info(s"${this.getClass.getSimpleName} : Verilator tester with elements of type $gen")
      iotesters.Driver.execute(
          Array(
              "--backend-name",
              "verilator",
              "--fint-write-vcd",
              "--target-dir",
              "test_run_dir/test_deflate",
              "--top-name",
              "Deflate"
          ),
          () => new PosEdgeWrapper(new DeflateRole(gen.getWidth, lz77Depth, literalHuffCodes, distanceHuffCodes))
      ) { c =>
        new DecodeAndPrint(c, gen, literalHuffCodes, distanceHuffCodes, sentence)
      }
    } catch {
      case _: RepresentationOverflowException =>
        logger.warn("Overflow detected on design, not checking validity further")
        true
      //case _: Throwable => false
    }
  }

  def verilatorGzipWrapper[T <: Data: Ring](
      gen: T,
      lz77Depth: Int,
      literalHuffCodes: Seq[(Int, Int)],
      distanceHuffCodes: Seq[(Int, Int)],
      sentence: String,
      name: String
  ): Boolean = {
    try {
      logger.info(s"${this.getClass.getSimpleName} : Verilator tester with elements of type $gen")
      iotesters.Driver.execute(
          Array(
              "--backend-name",
              "verilator",
              "--fint-write-vcd",
              "--target-dir",
              "test_run_dir/test_deflate",
              "--top-name",
              "Deflate"
          ),
          () => new PosEdgeWrapper(new DeflateRole(gen.getWidth, lz77Depth, literalHuffCodes, distanceHuffCodes))
      ) { c =>
        new GzipWrapper(c, gen, sentence, name)
      }
    } catch {
      case _: RepresentationOverflowException =>
        logger.warn("Overflow detected on design, not checking validity further")
        true
      //case _: Throwable => false
    }
  }

  def tester[T <: Data: Ring](
      genArray: RingElem[T],
      sentence: String,
      mode: String,
      name: String
  ): (PosEdgeWrapper => LatencyExtractTester) = {
    mode match {
      case "gzip" => { c =>
        new GzipWrapper(c, genArray.value, sentence, name)
      }
      case "decode" => { c =>
        new DecodeAndPrint(
            c,
            genArray.value,
            HuffmanTable.fixedLiteralTable(),
            HuffmanTable.fixedDistanceTable(),
            sentence
        )
      }
      case _ => {
        throw new UnsupportedOperationException("Wrong mode. Only gzip and decode are accepted.")
      }
    }
  }
}

class DeflateTester extends ChiselFlatSpec {
  val elem      = UInt((8 * 8).W)
  val lz77Depth = 256

  for (filename <- Array("18juin.txt", "moon.txt")) {
    val sentence = StreamReader.readFile("src/test/scala/deflate/texts/" + filename)
    "running verilator" should s"decode the $filename" taggedAs (Kernel) in {
      DeflateHelper.verilatorDecodeAndPrint(
          elem,
          lz77Depth,
          HuffmanTable.fixedLiteralTable(),
          HuffmanTable.fixedDistanceTable(),
          sentence
      ) should be(true)
    }
  }
}

/** Used for implementation and debug. */
class DeflateSpecTester extends ChiselFlatSpec {
  val elem      = UInt((8 * 8).W)
  val lz77Depth = 256

  val filename = "moon.txt"
  val sentence = StreamReader.readFile("src/test/scala/deflate/texts/" + filename)

  "running verilator" should "decode the stream" taggedAs (Kernel) in {
    DeflateHelper.verilatorDecodeAndPrint(
        elem,
        lz77Depth,
        HuffmanTable.fixedLiteralTable(),
        HuffmanTable.fixedDistanceTable(),
        sentence
    ) should be(true)
  }

  //TODO: fix test by adding source file
  "running verilator" should "wrap data in GZIP format" taggedAs (Kernel, Deprecated) in {
    DeflateHelper.verilatorGzipWrapper(
        elem,
        lz77Depth,
        HuffmanTable.fixedLiteralTable(),
        HuffmanTable.fixedDistanceTable(),
        sentence,
        filename + ".gz"
    ) should be(true)
  }
}
