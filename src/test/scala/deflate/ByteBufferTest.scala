/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate

import bench.utils.exceptions.RepresentationOverflowException

import chisel3._
import chisel3.iotesters.{ChiselFlatSpec, PeekPokeTester}

import logger.LazyLogging

class ByteBufferTest(dut: ByteBuffer) extends PeekPokeTester(dut) {
  val words  = Array(0x123, 0x4, 0x56, 0x789, 0xabc, 0xd, 0xe, 0xf)
  val sizes  = Array(12, 4, 8, 12, 12, 4, 4, 4)
  val result = Array(0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc)

  for ((word, size) <- words.zip(sizes)) {
    if (size >= 0) {
      poke(dut.io.word.valid, true)
    } else {
      poke(dut.io.word.valid, false)
    }
    poke(dut.io.word.bits, word)
    poke(dut.io.size, size)
    step(1)
  }
  step(2)
}

object ByteBufferHelper extends LazyLogging {
  def verilatorTest(nbBytes: Int, maxWordSize: Int): Boolean = {
    try {
      logger.info(s"${this.getClass.getSimpleName} : Verilator tester")
      iotesters.Driver.execute(
          Array(
              "--backend-name",
              "verilator",
              "--fint-write-vcd",
              "--target-dir",
              "test_run_dir/test_byte_buffer",
              "--top-name",
              "ByteBuffer"
          ),
          () => new ByteBuffer(nbBytes, maxWordSize)
      ) { c =>
        new ByteBufferTest(c)
      }
    } catch {
      case _: RepresentationOverflowException =>
        logger.warn("Overflow detected on design, not checking validity further")
        true
      // case _: Throwable => false
    }
  }
}

/** Used for implementation and debug. */
class ByteBufferSpecTester extends ChiselFlatSpec {

  "running verilator" should "validate your design" in {
    ByteBufferHelper.verilatorTest(
        4,
        16
    ) should be(true)
  }
}
