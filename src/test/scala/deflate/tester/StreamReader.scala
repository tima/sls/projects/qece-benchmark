/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.tester

import bench.utils.components.PosEdgeWrapper
import bench.utils.software._
import bench.utils.tests._

import java.io.{BufferedInputStream, FileInputStream}

import scala.collection.mutable.ListBuffer

import chisel3._
import dsptools.numbers._

abstract class StreamReader[T <: Data: Ring](
    mut: PosEdgeWrapper,
    gen: T,
    sentence: String
) extends LatencyExtractTester(mut) {
  implicit val signal     = gen
  implicit val comparator = Comparator(0.25)

  val buffer = ListBuffer[BigInt]()

  def postSimulation(): Unit

  def stepReader(stepValue: Int): Unit = {
    for (_ <- 0 until stepValue) {
      if (peek(mut.io.out.valid) == 1) {
        val value = peek(mut.io.out.bits)
        buffer.append(value)
      }
      step(1)
    }
  }

  def pokeWaiter(value: BigInt): Unit = {
    var i = 100
    while (i > 0 && peek(mut.io.in.ready) == 0) {
      stepReader(1)
      i = i - 1
    }
    if (i == 0) throw new Exception("Timeout.")
    poke(mut.io.in.bits, value)
  }

  // Simu
  initSimu
  poke(mut.io.in.valid, true)
  for ((char, index) <- sentence.zipWithIndex) {
    pokeWaiter(if (index == sentence.size - 1) {
      BigInt(char.toInt) | (BigInt(1) << (gen.getWidth - 1)) // Last byte
    } else {
      BigInt(char.toInt)
    })
    stepReader(1)
  }
  poke(mut.io.in.valid, false)
  stepReader(41)
  endSimu

  postSimulation()
}

object StreamReader {
  def readFile(path: String): String = {
    val file     = new FileInputStream(path)
    val buff     = new BufferedInputStream(file)
    val sentence = buff.readAllBytes()
    buff.close()
    new String(sentence)
  }
}
