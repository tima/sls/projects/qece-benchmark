/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.tester

import bench.kernels.deflate.test.software.deflate.DeflateDecode

import bench.utils.components.PosEdgeWrapper

import chisel3._
import dsptools.numbers._

class DecodeAndPrint[T <: Data: Ring](
    mut: PosEdgeWrapper,
    gen: T,
    literalTable: Seq[(Int, Int)],
    distanceTable: Seq[(Int, Int)],
    sentence: String
) extends StreamReader(mut, gen, sentence) {

  override def postSimulation(): Unit = {
    // Decode
    val reader = new DeflateDecode(
        literalTable,
        distanceTable,
        buffer.iterator,
        gen.getWidth
    )
    reader.readAll()
    reader.printDecoded()
    debug("ORIG : " + sentence)
    debug("Original size : " + sentence.size)
    debug("Decoded size : " + reader.dataDecoded.toString().size)
    expect(reader.dataDecoded.toString().equals(sentence), "Integrity failed.")
  }
}
