/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.tester

import bench.kernels.deflate.test.software.tools.{ByteIterator, CRC32, Constants}

import bench.utils.components.PosEdgeWrapper

import java.io.{BufferedOutputStream, FileOutputStream}
import java.io.ByteArrayOutputStream

import chisel3._
import dsptools.numbers._

class GzipWrapper[T <: Data: Ring](mut: PosEdgeWrapper, gen: T, sentence: String, name: String)
    extends StreamReader(mut, gen, sentence) {

  def writeToFile(path: String): Unit = {
    val file         = new FileOutputStream(path)
    val buff         = new BufferedOutputStream(file)
    val byteIterator = new ByteIterator(buffer.iterator, gen.getWidth / 8)

    // =========== Header ===========
    val header = new ByteArrayOutputStream(10)
    header.write(Constants.GZIP_MAGIC_NUMBER_1)
    header.write(Constants.GZIP_MAGIC_NUMBER_2)
    header.write(Constants.GZIP_DEFLATE_COMPRESSION)
    header.write(Constants.GZIP_NO_FLAGS)
    header.write(0x00)
    header.write(0x00)
    header.write(0x00)
    header.write(0x00)
    header.write(Constants.GZIP_EXTRA_FLAGS)
    header.write(Constants.GZIP_OS)
    buff.write(header.toByteArray)

    // =========== Compressed data ===========
    while (byteIterator.hasNext) {
      val byte = byteIterator.next()
      buff.write(byte)
    }

    // =========== Footer ===========
    val crc32 = new CRC32()
    crc32.update(sentence)
    buff.write(crc32.toByteArray)

    for (i <- 0 until 4) {
      buff.write(((sentence.size >> (i * 8)) & 0xff).toByte)
    }

    buff.close()
    file.close()
  }

  override def postSimulation(): Unit = {
    writeToFile("generated/" + name)
  }

}
