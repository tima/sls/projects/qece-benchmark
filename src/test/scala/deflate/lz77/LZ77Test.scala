/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.deflate.lz77

import bench.utils.tests.LoggedTester
import bench.utils.exceptions.RepresentationOverflowException

import bench.tags._

import logger.LazyLogging

import chisel3._
import chisel3.iotesters._

class LZ77Test(dut: LZ77, depth: Int) extends LoggedTester(dut) {
  val sentence      = "That apple is our best apple. Is'n it ?"
  val reconstructed = new StringBuilder()
  val interpreted   = new StringBuilder()

  def check(): Unit = {
    if (peek(dut.io.valid) == 1) {
      val length   = peek(dut.io.length).toInt
      val position = peek(dut.io.position).toInt
      if (length > 0) {
        val start = reconstructed.size - position
        if (start < 0 || start + length > reconstructed.size) {
          reconstructed.append("[error]")
        } else {
          reconstructed.append(reconstructed.substring(start, start + length))
        }

        interpreted.append("@(" + position + ", " + length + ")")
      } else {
        val char = new String(Array(peek(dut.io.char).toByte))
        reconstructed.append(char)
        interpreted.append(char)
      }
    }
  }

  poke(dut.io.inputChar.valid, true)
  poke(dut.io.flush, false)

  for (c <- sentence) {
    poke(dut.io.inputChar.bits, c.toInt)
    check()
    step(1)
  }
  poke(dut.io.inputChar.valid, false)
  poke(dut.io.flush, true)
  for (_ <- 0 until 15) {
    check()
    step(1)
  }

  debug("Original sentence      " + sentence)
  debug("Reconstructed sentence " + reconstructed)
  debug("Interpreted sentence   " + interpreted)

}

object LZ77Helper extends LazyLogging {
  def verilatorTest(depth: Int): Boolean = {
    try {
      logger.info(s"${this.getClass.getSimpleName} : Verilator tester a depth = $depth")
      iotesters.Driver.execute(
          Array(
              "--backend-name",
              "verilator",
              "--fint-write-vcd",
              "--target-dir",
              "test_run_dir/test_lz77",
              "--top-name",
              "LZ77"
          ),
          () => new LZ77(depth)
      ) { c =>
        new LZ77Test(c, depth)
      }
    } catch {
      case _: RepresentationOverflowException =>
        logger.warn("Overflow detected on design, not checking validity further")
        true
      // case _: Throwable => false
    }
  }
  def firrtlTest(depth: Int): Boolean = {
    try {
      logger.info(s"${this.getClass.getSimpleName} : Verilator tester a depth = $depth")
      iotesters.Driver.execute(
          Array(
              "--backend-name",
              "firrtl",
              "--fint-write-vcd",
              "--target-dir",
              "test_run_dir/test_lz77",
              "--top-name",
              "LZ77"
          ),
          () => new LZ77(depth)
      ) { c =>
        new LZ77Test(c, depth)
      }
    } catch {
      case _: RepresentationOverflowException =>
        logger.warn("Overflow detected on design, not checking validity further")
        true
      //case _: Throwable => false
    }
  }
}

/** Used for implementation and debug. */
class LZ77SpecTester extends ChiselFlatSpec {

  val depth = 256

  "running verilator" should "validate your design" taggedAs (Unit) in {
    LZ77Helper.verilatorTest(
        depth
    ) should be(true)
  }
}
