/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.hardware

import bench.utils.software.Numeric
import bench.utils.tests.CustomTester

import bench.utils.exceptions._

import bench.tags._

import chisel3._
import chisel3.experimental._
import chisel3.iotesters
import chisel3.iotesters.ChiselFlatSpec
import dsptools.numbers._

class SimpleRingAdder[T <: Data: Ring](myType: RingElem[T]) extends Module {
  val io = IO(new Bundle {
    val in1 = Input(myType.cloneType)
    val in2 = Input(myType.cloneType)
    val out = Output(myType.cloneType)
  })
  /* Should add io.in1 to io.in2, and delay the result by another one cycle. */
  io.out := (io.in1 + io.in2).delay(5)
}

class RingElemUnitTester[T <: Data: Ring](
    signal: T,
    mut: SimpleRingAdder[T],
    size: Int = 1
) extends CustomTester(mut, signal, 0.25) {
  val aArray = Array.ofDim[Numeric](size)
  val bArray = Array.ofDim[Numeric](size)
  for (i <- 0 until size) {
    aArray(i) = Numeric.random(signal, 50)
    bArray(i) = Numeric.random(signal, 50)
  }

  for {
    a <- aArray
    b <- bArray
  } yield {
    poke(mut.io.in1, a)
    poke(mut.io.in2, b)
    step(Delay.getAddDelay(signal) + 5) // test addition (pipelined or not) and delay func
    val output = peek(mut.io.out)
    debug(s"$a + $b should give ${a + b}, found ${output}")
    expect(a + b == output, s"Simple Ring Adder failed. $a + $b should return ${a + b} and not ${output}")
  }
}

object RingHelper extends logger.LazyLogging {
  def verilatorTest[T <: Data: Ring](myType: T, nbTest: Int = 2): Boolean = {
    try {
      logger.info(s"Ring Simple tester : verilator tester with elements of type $myType")
      iotesters.Driver.execute(Array("--backend-name", "verilator"), () => new SimpleRingAdder(RingElem(myType))) { c =>
        new RingElemUnitTester(myType, c, nbTest)
      }
    } catch {
      case _: RepresentationOverflowException =>
        logger.warn("Overflow detected on design, not checking validity further")
        true
      case _: Throwable => false
    }
  }
}

class RingTester extends ChiselFlatSpec {
  val myFixedPoint = FixedPoint(24.W, 6.BP)
  s"Verilator test with type = $myFixedPoint using verilator backend" should
    "be used as an alternative way to run using verilator" taggedAs (Unit) in {
    RingHelper.verilatorTest(myFixedPoint) should be(true)
  }
  val myUInt = UInt(14.W)
  s"Verilator test with type = $myUInt using verilator backend" should
    "be used as an alternative way to run using verilator" taggedAs (Unit) in {
    RingHelper.verilatorTest(myUInt) should be(true)
  }
}
