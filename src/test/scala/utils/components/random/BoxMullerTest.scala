/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.components.random

import bench.utils.exceptions.RepresentationOverflowException
import bench.utils.hardware.RingElem
import bench.utils.software.Numeric
import bench.utils.tests.CustomTester

import bench.tags._

import scala.util.Random

import logger.LazyLogging

import chisel3._
import chisel3.experimental.FixedPoint
import chisel3.iotesters.ChiselFlatSpec
import dsptools.numbers.Ring

class BoxMullerTest[T <: Data: Ring](c: BoxMuller[T], gen: T) extends CustomTester(c, gen, 0.25) {
  val r  = new Random()
  val n  = 10
  val u1 = Array.fill(n)(r.nextDouble())
  val u2 = Array.fill(n)(r.nextDouble())

  for (i <- 0 until n + c.getPipelineLevel()) {
    if (i < n) {
      poke(c.io.urng0, Numeric(u1(i)))
      poke(c.io.urng1, Numeric(u2(i)))
    }
    if (i >= c.getPipelineLevel()) {
      val index   = i - c.getPipelineLevel()
      val hwValue = peek(c.io.gauss)
      val swValue = Numeric(software.BoxMuller.gauss(u1(index), u2(index))._2)
      debug("Output : " + hwValue + " (ref : " + swValue + ")")
      expect(this.myComparator.compare(swValue, hwValue), "The value does not match with the reference")
    }
    step(1)
  }
}

object BoxMullerHelper extends LazyLogging {
  def verilatorTest[T <: Data: Ring](gen: T, boxMullerLUTsize: Int): Boolean = {
    try {
      logger.info(s"${this.getClass.getSimpleName} : Verilator tester with elements of type $gen")
      iotesters.Driver.execute(
          Array(
              "--backend-name",
              "verilator",
              "--target-dir",
              "test_run_dir/test_box_muller",
              "--top-name",
              "BoxMuller"
          ),
          () => BoxMuller(RingElem(gen), boxMullerLUTsize)
      ) { c =>
        new BoxMullerTest(c, gen)
      }
    } catch {
      case _: RepresentationOverflowException =>
        logger.warn("Overflow detected on design, not checking validity further")
        true
      case _: Throwable => false
    }
  }
}

class BoxMullerTester extends ChiselFlatSpec {
  val elemTypes = Array(FixedPoint(32.W, 16.BP), FixedPoint(64.W, 32.BP))
  val lutSizes  = Array(16, 128, 256)

  for (elemType <- elemTypes) {
    for (lutSize <- lutSizes) {
      s"Verilator test with params (elemType=$elemType, lutSize=$lutSize)" should
        "validate your design" taggedAs (Probabilistic, Unit) in {
        BoxMullerHelper.verilatorTest(
            elemType,
            lutSize
        ) should be(true)
      }
    }
  }
}

/** Used for implementation and debug. */
class BoxMullerSpecTester extends ChiselFlatSpec {
  val boxMullerLUTsize = 256

  "running verilator" should "validate your design" taggedAs (Probabilistic, Unit) in {
    BoxMullerHelper.verilatorTest(
        FixedPoint(32.W, 16.BP),
        boxMullerLUTsize
    ) should be(true)
  }
}
