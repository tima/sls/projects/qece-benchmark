/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.components.random

import bench.utils.tests.LoggedTester
import bench.utils.exceptions.RepresentationOverflowException

import bench.tags._

import plotly.element.HistNorm
import plotly.layout.Layout
import plotly.{Histogram, Plotly}

import logger.LazyLogging

import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, PeekPokeTester}

class TauswortheURNGTest(
    c: TauswortheURNG,
    bitWidth: Int,
    config: Array[TauswortheConfig]
) extends LoggedTester(c) {
  require(bitWidth == 32, "The software reference needs a 32 bit integer")
  val STEPS = 100
  val swRef = new software.TauswortheURNG(config)
  for (_ <- 0 until STEPS) {
    step(1)
    val swValue = swRef.random().toLong
    val hwValue = peek(c.io.urng)
    debug("Output : " + hwValue.toString + " (ref : " + swValue.toString + ")")
    expect(swValue == hwValue, "The value does not match with the reference")
  }
}

class TausworthePDFPlot(c: TauswortheURNG, bitWidth: Int) extends PeekPokeTester(c) {
  val n     = 100000
  val array = new Array[Double](n)

  for (i <- 0 until n) {
    step(1)
    val hwValue = peek(c.io.urng)
    array(i) = hwValue.toDouble / ((BigInt(2).pow(bitWidth) - 1).toDouble)
  }

  Plotly.plot(
      "plots/TausworthePDF",
      Seq(Histogram(array.toSeq, histnorm = HistNorm.ProbabilityDensity)),
      Layout(title = "Discrete approximation of the uniform distribution with Tausworthe generation")
  )
}

object TauswortheURNGHelper extends LazyLogging {
  def verilatorTest(bitWidth: Int, config: Array[TauswortheConfig]): Boolean = {
    try {
      logger.info(s"${this.getClass.getSimpleName} : Verilator tester")
      iotesters.Driver.execute(
          Array(
              "--backend-name",
              "verilator",
              "--target-dir",
              "test_run_dir/test_urng",
              "--top-name",
              "TauswortheURNG"
          ),
          () => new TauswortheURNG(bitWidth, config)
      ) { c =>
        new TauswortheURNGTest(c, bitWidth, config)
      }
    } catch {
      case _: RepresentationOverflowException =>
        logger.warn("Overflow detected on design, not checking validity further")
        true
      case _: Throwable => false
    }
  }

  def plotPDF(bitWidth: Int, config: Array[TauswortheConfig]): Boolean = {
    try {
      logger.info(s"${this.getClass.getSimpleName} : Plot viewer")
      iotesters.Driver.execute(
          Array(
              "--backend-name",
              "verilator",
              "--target-dir",
              "test_run_dir/test_urng_plot",
              "--top-name",
              "TauswortheURNG"
          ),
          () => new TauswortheURNG(bitWidth, config)
      ) { c =>
        new TausworthePDFPlot(c, bitWidth)
      }
    } catch {
      case _: RepresentationOverflowException =>
        logger.warn("Overflow detected on design, not checking validity further")
        true
      case _: Throwable => false
    }
  }
}

class TauswortheURNGTester extends ChiselFlatSpec {
  // The software version of TauswortheURNG only support 32 bits
  val rndConfig = TauswortheConfig.randomTupleIterator(123, 32)

  for (i <- 0 until 5) {
    val config = rndConfig.next()._1
    s"running verilator with default Tausworthe with random seed (test $i)" should "validate your design" in {
      TauswortheURNGHelper.verilatorTest(
          32,
          config
      ) should be(true)
    }
  }
}

/** Used for implementation and debug. */
class TauswortheURNGSpecTester extends ChiselFlatSpec {
  "running verilator" should "validate your design" taggedAs (Unit, Probabilistic) in {
    TauswortheURNGHelper.verilatorTest(
        32,
        TauswortheConfig.default(123456, 7891011, 121314)
    ) should be(true)
  }

  "running for PDF plot" should "plot the PDF" taggedAs (Deprecated, Probabilistic) in {
    TauswortheURNGHelper.plotPDF(
        64,
        TauswortheConfig.randomTupleIterator(123, 64).next()._1
    ) should be(true)
  }
}
