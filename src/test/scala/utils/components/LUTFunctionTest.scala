/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.components

import bench.utils.software._
import bench.utils.hardware._
import bench.utils.exceptions._
import bench.utils.tests.CustomTester

import bench.tags._

import logger.LazyLogging

import chisel3._
import chisel3.iotesters
import chisel3.iotesters._
import chisel3.experimental._
import dsptools.numbers.Ring

class LUTTestWrapper[T <: Data: Ring](
    elemType: RingElem[T],
    fct: Double => Double,
    size: Int,
    a: Int,
    b: Int
) extends Module {
  val io = IO(new Bundle {
    val x = Input(elemType)
    val y = Output(elemType)
  })
  val config = new LUTFunctionConfig(elemType, fct, size, a, b)
  val lut    = LUTFunction(elemType, config)
  lut.io.x := io.x
  io.y := lut.io.y
}

/* Testing the design */
class LUTFunctionTest[T <: Data: Ring](
    c: LUTTestWrapper[T],
    gen: T,
    fct: Double => Double,
    a: Int,
    b: Int,
    threshold: Double
) extends CustomTester(c, gen, threshold) {
  // Take samples between a et b and test the values.
  // WARNING : pay attention to the comparator threshold !

  val nbSamples = 10
  val x         = breeze.linalg.linspace(a, b, nbSamples)

  for (i <- 0 until nbSamples + c.lut.getPipelineLevel()) {
    if (i < nbSamples) {
      poke(c.io.x, Numeric(x(i)))
    }
    if (i >= c.lut.getPipelineLevel()) {
      val index   = i - c.lut.getPipelineLevel()
      val hwValue = BitVector(peek(c.io.y))
      val swValue = Numeric(fct(x(index)))
      debug("Output : " + hwValue.toString + " (ref : " + swValue.toString + ")")
      expect(this.myComparator.compare(swValue, hwValue), "The value does not match with the reference")
    }
    step(1)
  }
}

object LUTFunctionHelper extends LazyLogging {
  def verilatorTest[T <: Data: Ring](
      gen: T,
      fct: Double => Double,
      size: Int,
      a: Int,
      b: Int,
      threshold: Double = 0.25
  ): Boolean = {
    try {
      logger.info(s"${this.getClass.getSimpleName} : Verilator tester with elements of type $gen")
      iotesters.Driver.execute(
          Array(
              "--backend-name",
              "verilator",
              "--target-dir",
              "test_run_dir/test_lutf",
              "--top-name",
              "LUTFunction"
          ),
          () => new LUTTestWrapper(RingElem(gen), fct, size, a, b)
      ) { c =>
        new LUTFunctionTest(c, gen, fct, a, b, threshold)
      }
    } catch {
      case _: RepresentationOverflowException =>
        logger.warn("Overflow detected on design, not checking validity further")
        true
      case _: Throwable => false
    }
  }
}

class LUTFunctionTester extends ChiselFlatSpec {
  val elemTypes = Array(FixedPoint(32.W, 16.BP), FixedPoint(64.W, 32.BP))
  val lutSizes  = Array(16, 128, 256)

  def doSin(elemType: FixedPoint, size: Int): Unit = {
    s"Verilator test with Sinus function (elemType=$elemType, size=$size)" should
      "validate your design" taggedAs (Unit) in {
      LUTFunctionHelper.verilatorTest(
          elemType,
          i => Math.sin(i),
          size,
          -1,
          1
      ) should be(true)
    }
  }

  def doSqrt(elemType: FixedPoint, size: Int): Unit = {
    if (size == 16) return
    s"Verilator test with Sqrt function (elemType=$elemType, size=$size)" should
      "validate your design" taggedAs (Unit) in {
      LUTFunctionHelper.verilatorTest(
          elemType,
          i => Math.sqrt(i),
          size,
          0,
          1
      ) should be(true)
    }
  }

  def doExp(elemType: FixedPoint, size: Int): Unit = {
    // This test use specific range unlike the other tests
    s"Verilator test with Exponential function (elemType=$elemType, size=$size)" should
      "validate your design" taggedAs (Deprecated, Unit) in {
      LUTFunctionHelper.verilatorTest(
          elemType,
          i => Math.exp(i),
          size,
          -1,
          3
      ) should be(true)
    }
  }

  for (elemType <- elemTypes) {
    for (lutSize <- lutSizes) {
      doSin(elemType, lutSize)
      doSqrt(elemType, lutSize)
      doExp(elemType, lutSize)
    }
  }
}

/** Used for implementation and debug. */
class LUTFunctionSpecTester extends ChiselFlatSpec {
  "running verilator" should "validate your design" taggedAs (Unit) in {
    LUTFunctionHelper.verilatorTest(
        FixedPoint(32.W, 16.BP),
        i => Math.sin(i),
        256,
        -1,
        1
    ) should be(true)
  }
}
