/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.components

import bench.kernels.matrixRef.{Matrix, BitMatrix}

import bench.utils.software.BitVector
import bench.utils.hardware.RingElem
import bench.utils.tests.CustomTester
import bench.utils.exceptions._

import bench.tags._

import logger.LazyLogging

import chisel3._
import chisel3.experimental._
import chisel3.iotesters
import chisel3.iotesters.ChiselFlatSpec
import dsptools.numbers._

class MacUnitTester[T <: Data: Ring](mut: DeserMac[T], size: Int, signal: T) extends CustomTester(mut, signal, 0.25) {

  /** Perform matrix multiplication using a MAC Unit
   *
   *  @param a
   *    first operand
   *  @param b
   *    second operand
   *  @param c
   *    design under test
   *  @param gen
   *    signal type for the design
   *  @return
   *    the result of a*b (as of the matrix multiplication meaning)
   */
  def testHw(a: BitMatrix, b: BitMatrix): BitMatrix = {

    def serializeArray(a: Array[BitVector], bitWidth: Int, elemWidth: Int): Array[BitVector] = {
      val length       = a.length
      val nbElemCycle  = bitWidth / elemWidth
      val nbElemResult = length / nbElemCycle
      val mask         = BitVector.createMask(elemWidth)
      val result       = Array.ofDim[BitVector](nbElemResult)
      for (i <- 0 until nbElemResult) {
        var tmp = BigInt(0)
        for (j <- 0 until nbElemCycle) {
          tmp = (tmp << elemWidth) | (a(i * nbElemCycle + j) & mask)
        }
        result(i) = tmp
      }
      result
    }

    val matA = a.getContent
    val matB = b.getContent.transpose

    val pipelineLevel = mut.getPipelineLevel

    /* Tmp 1D array to store results. */
    val tmpResult   = Array.ofDim[BitVector](size * size)
    var tmpIndex    = 0
    var tmp         = 0
    val nbCycleLine = size / (mut.bitWidth / signal.getWidth)

    for (i <- 0 until size) {
      for (j <- 0 until size) {
        val a_vec = serializeArray(matA(i), mut.bitWidth, signal.getWidth)
        val b_vec = serializeArray(matB(j), mut.bitWidth, signal.getWidth)
        for (k <- 0 until a_vec.length) {
          if (tmp == pipelineLevel + nbCycleLine) {
            tmpResult(tmpIndex) = peek(mut.io.result)
            tmpIndex += 1
            tmp = pipelineLevel
          }
          tmp += 1
          if (k == 0) { poke(mut.io.reset, true) }
          else { poke(mut.io.reset, false) }
          poke(mut.io.a_line, a_vec(k))
          poke(mut.io.b_col, b_vec(k))
          step(1)
        }
      }
    }
    step(pipelineLevel + nbCycleLine - tmp)

    /* Fill the rest of the results */
    while (tmpIndex < tmpResult.length) {
      tmpResult(tmpIndex) = peek(mut.io.result)
      tmpIndex += 1
      step(nbCycleLine)
    }

    val result = BitMatrix.arrayToMatrix(tmpResult, size, 0)

    debug(result.prettyToString("Result"))
    result
  }

  def testSw(a: Matrix, b: Matrix): Matrix = {
    val result = a * b
    debug(result.prettyToString("GoldenReference"))
    result
  }

  def compareResult(a: Matrix, b: Matrix): Unit = {
    expect(
        testSw(a, b) == testHw(BitMatrix(signal, a), BitMatrix(signal, b)),
        "MAC unit design not conform to Golden reference"
    )
  }

  /* Test values */
  val softA = Matrix(size, signal, 5)
  val softB = Matrix(size, signal, 5)

  debug(softA.prettyToString("A"))
  debug(softB.prettyToString("B"))
  compareResult(softA, softB)
}

object MacHelper extends LazyLogging {
  def verilatorTest[T <: Data: Ring](signal: T, bitWidth: Int, size: Int): Boolean = {
    try {
      logger.info(
          s"MAC Units : verilator tester with elements of type $signal, IO bitwidth $bitWidth and matrix size $size"
      )
      iotesters.Driver.execute(Array("--backend-name", "verilator"), () => new DeserMac(RingElem(signal), bitWidth)) {
        c => new MacUnitTester(c, size, signal)
      }
    } catch {
      case _: RepresentationOverflowException =>
        logger.warn("Overflow detected on design, not checking validity further")
        true
      case _: Throwable => false
    }
  }
}

class MacTester extends ChiselFlatSpec {
  def doFPTest(bitWidth: Int, size: Int) = {
    val elemType = FixedPoint(32.W, 16.BP)
    if ((bitWidth >= elemType.getWidth) & ((bitWidth / elemType.getWidth) <= size)) {
      s"Verilator test with params (type = $elemType, matrix size = $size, I/O bitWidth = $bitWidth) " +
        s"using verilator backend" should
        "be used as an alternative way to run using verilator" taggedAs (Long, Unit) in {
        MacHelper.verilatorTest(elemType, bitWidth, size) should be(true)
      }
    }
  }
  def doComplexTest(bitWidth: Int, size: Int) = {
    val elemType = DspComplex(FixedPoint(32.W, 16.BP))
    if ((bitWidth >= elemType.getWidth) & ((bitWidth / elemType.getWidth) <= size)) {
      s"Verilator test with params (type = $elemType, matrix size = $size, I/O bitWidth = $bitWidth) " +
        s"using verilator backend" should
        "be used as an alternative way to run using verilator" taggedAs (Long, Unit) in {
        MacHelper.verilatorTest(elemType, bitWidth, size) should be(true)
      }
    }
  }
  def doUIntTest(bitWidth: Int, size: Int) = {
    val elemType = UInt(16.W)
    if ((bitWidth >= elemType.getWidth) & ((bitWidth / elemType.getWidth) <= size)) {
      s"Verilator test with params (type = $elemType, matrix size = $size, I/O bitWidth = $bitWidth) " +
        s"using verilator backend" should
        "be used as an alternative way to run using verilator" taggedAs (Long, Unit) in {
        MacHelper.verilatorTest(elemType, bitWidth, size) should be(true)
      }
    }
  }
  def doSIntTest(bitWidth: Int, size: Int) = {
    val elemType = SInt(64.W)
    if ((bitWidth >= elemType.getWidth) & ((bitWidth / elemType.getWidth) <= size)) {
      s"Verilator test with params (type = $elemType, matrix size = $size, I/O bitWidth = $bitWidth) " +
        s"using verilator backend" should
        "be used as an alternative way to run using verilator" taggedAs (Long, Unit) in {
        MacHelper.verilatorTest(elemType, bitWidth, size) should be(true)
      }
    }
  }
  val bitWidthList = List(64, 128)
  val sizeList     = List(64, 128)
  for (size <- sizeList) {
    for (bitWidth <- bitWidthList) {
      doFPTest(bitWidth, size)
      doUIntTest(bitWidth, size)
      doSIntTest(bitWidth, size)
      doComplexTest(bitWidth, size)
    }
  }
}

class MacSpecTester extends ChiselFlatSpec {
  val bitWidth  = 512
  val myUInt    = UInt(32.W)
  val myComplex = DspComplex(FixedPoint(16.W, 8.BP))
  val matSize   = 64
  s"Verilator test with params (type = $myUInt, matrix size = $matSize, I/O bitWidth = $bitWidth) " +
    "using verilator backend" should "be used as an alternative way to run using verilator" taggedAs (Unit) in {
    MacHelper.verilatorTest(myUInt, bitWidth, matSize) should be(true)
  }
  s"Verilator test with params (type = $myComplex, matrix size = $matSize, I/O bitWidth = $bitWidth) " +
    "using verilator backend" should "be used as an alternative way to run using verilator" taggedAs (Unit) in {
    MacHelper.verilatorTest(myComplex, bitWidth, matSize) should be(true)
  }
}
