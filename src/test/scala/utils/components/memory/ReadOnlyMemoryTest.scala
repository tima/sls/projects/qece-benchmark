/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.components.memory

import bench.utils.software._
import bench.utils.hardware._
import bench.utils.tests.CustomTester

import bench.tags._

import logger.LazyLogging

import chisel3._
import chisel3.iotesters
import chisel3.iotesters._
import chisel3.experimental._
import dsptools.numbers.Ring

class ReadOnlyMemoryWrapper[T <: Data: Ring](
    gen: RingElem[T],
    initData: Array[Numeric]
) extends MultiIOModule {
  val nData   = initData.size
  val outputs = IO(Output(Vec(nData, gen)))

  val memory = ReadOnlyMemory(gen, initData)

  outputs <> memory.values
}

/* Testing the design */
class ReadOnlyMemoryTest[T <: Data: Ring](
    c: ReadOnlyMemoryWrapper[T],
    gen: T,
    initData: Array[Numeric]
) extends CustomTester(c, gen, 0.0) {
  val nbData = initData.size

  step(1)
  val values = peek(c.outputs)
  for (i <- 0 until nbData) {
    expect(values(i) == (initData(i)).toBits(gen).value, s"Found ${values(i)} expected ${initData(i)}")
  }
}

object ReadOnlyMemoryHelper extends LazyLogging {
  def verilatorTest[T <: Data: Ring](
      gen: T,
      initData: Array[Numeric]
  ): Boolean = {
    logger.info(
        s"${this.getClass.getSimpleName} : Verilator tester with ${initData.size} elements of type $gen on"
    )
    try {
      iotesters.Driver.execute(
          Array(
              "--backend-name",
              "verilator",
              "--target-dir",
              "test_run_dir/test_preset_memory",
              "--top-name",
              "PresetMemory"
          ),
          () => new ReadOnlyMemoryWrapper(RingElem(gen), initData)
      ) { c =>
        new ReadOnlyMemoryTest(c, gen, initData)
      }
    } catch {
      case e: java.lang.Exception => println(e); true
      case e: Throwable           => throw e
    }
  }
}

class ReadOnlyMemoryTester extends ChiselFlatSpec {
  // insure reproductability
  val r   = new scala.util.Random(42)
  val max = 100

  val sizeList = Seq(16, 128)

  def test[T <: Bits: Ring](tpe: T) = {
    for (size <- sizeList) {
      s"Testing PresetMemory on tpe $tpe with $size elements" should
        "work properly" taggedAs (Unit) in {
        ReadOnlyMemoryHelper.verilatorTest(
            tpe,
            (0 until size).map(_ => Numeric.random(tpe, max)).toArray
        ) should be(true)
      }
    }
  }

  test(UInt(36.W))
  test(SInt(12.W))
  test(FixedPoint(24.W, 12.BP))

}

/** Used for implementation and debug. */
class ReadOnlyMemorySpecTester extends ChiselFlatSpec {
  // val tpe       = FixedPoint(32.W, 16.BP)
  // val initData  = Array.tabulate(16)(i => Numeric(i.toDouble))
  val tpe      = UInt(36.W)
  val initData = Array.tabulate(1024)(i => Numeric(i))
  "running verilator" should "validate your design" in {
    ReadOnlyMemoryHelper.verilatorTest(
        tpe,
        initData
    ) should be(true)
  }
}
