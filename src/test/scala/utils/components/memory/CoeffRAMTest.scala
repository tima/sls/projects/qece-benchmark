/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.components.memory

import bench.utils.software._
import bench.utils.hardware._
import bench.utils.tests.CustomTester

import bench.tags._

import logger.LazyLogging

import chisel3._
import chisel3.util._
import chisel3.iotesters
import chisel3.iotesters._
import chisel3.experimental._
import dsptools.numbers.Ring

class CoeffRAMWrapper[T <: Data: Ring](
    gen: RingElem[T],
    size: Int,
    nInputs: Int
) extends MultiIOModule {
  val inputs  = IO(Input(Vec(nInputs, gen)))
  val en      = IO(Input(Bool()))
  val outputs = IO(Output(Vec(size, gen)))

  val memory = CoeffRAM(gen, size, nInputs)

  // buffer inputs, as PeekPokeTester use NegEdge poking...
  val buffInputs = Reg(Vec(nInputs, gen))
  val buffEn     = RegInit(Bool(), false.B)

  buffInputs <> inputs
  outputs <> memory.values
  buffEn <> en

  val counter = RegInit(UInt(log2Up(size / nInputs + 1).W), 0.U)
  when(buffEn) {
    memory.write(counter, buffInputs.asUInt)
    counter := counter + 1.U
  }
}

/* Testing the design */
class CoeffRAMTest[T <: Data: Ring](
    c: CoeffRAMWrapper[T],
    gen: T,
    initData: Array[Numeric],
    nInputs: Int
) extends CustomTester(c, gen, 0.0) {
  val nbData   = initData.size
  val realData = initData.map(_ * 2)

  for (i <- 0 until (nbData.toDouble / nInputs).ceil.toInt) {
    for (j <- 0 until nInputs) {
      poke(c.inputs(j), realData(i * nInputs + j))
      poke(c.en, true.B)
    }
    step(1)
  }
  poke(c.en, false.B)
  step(1)

  val values = peek(c.outputs)
  for (i <- 0 until nbData) {
    expect(values(i) == (realData(i)).toBits(gen).value, s"Found ${values(i)} expected ${realData(i)}")
  }
}

object CoeffRAMHelper extends LazyLogging {
  def verilatorTest[T <: Data: Ring](
      gen: T,
      initData: Array[Numeric],
      nInputs: Int
  ): Boolean = {
    logger.info(
        s"${this.getClass.getSimpleName} : Verilator tester with ${initData.size} elements of type $gen  ($nInputs inputs) on"
    )
    try {
      iotesters.Driver.execute(
          Array(
              "--backend-name",
              "verilator",
              "--target-dir",
              "test_run_dir/test_preset_memory",
              "--top-name",
              "PresetMemory"
          ),
          () => new CoeffRAMWrapper(RingElem(gen), initData.size, nInputs)
      ) { c =>
        new CoeffRAMTest(c, gen, initData, nInputs)
      }
    } catch {
      case e: java.lang.Exception => println(e); true
      case e: Throwable           => throw e
    }
  }
}

class CoeffRAMTester extends ChiselFlatSpec {
  // insure reproductability
  val r   = new scala.util.Random(42)
  val max = 100

  def generatePorts(size: Int): Array[Int] = (Seq(2, 4).map(i => size / i)).toArray

  val sizeList = Seq(16, 128)

  def test[T <: Bits: Ring](tpe: T) = {
    for (size <- sizeList) {
      for (portSize <- generatePorts(size)) {
        s"Testing PresetMemory on tpe $tpe with $size elements and $portSize input ports" should
          "work properly" taggedAs (Unit) in {
          CoeffRAMHelper.verilatorTest(
              tpe,
              (0 until size).map(_ => Numeric.random(tpe, max)).toArray,
              portSize
          ) should be(true)
        }
      }
    }
  }

  test(UInt(36.W))
  test(SInt(12.W))
  test(FixedPoint(24.W, 12.BP))

}

/** Used for implementation and debug. */
class CoeffRAMSpecTester extends ChiselFlatSpec {
  // val tpe       = FixedPoint(32.W, 16.BP)
  // val initData  = Array.tabulate(16)(i => Numeric(i.toDouble))
  val tpe      = UInt(36.W)
  val initData = Array.tabulate(1024)(i => Numeric(i))
  val nbInputs = 512
  // val nbInputs  = None
  "running verilator" should "validate your design" in {
    CoeffRAMHelper.verilatorTest(
        tpe,
        initData,
        nbInputs
    ) should be(true)
  }
}
