/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.tests

import bench.utils.components.PosEdgeWrapper

import bench.utils.software.{BitVector}
import bench.utils.exceptions._
import bench.utils.convert._

/** LatencyExtractTester used for counting cycle in simulation. */
abstract class LatencyExtractTester(mut: PosEdgeWrapper) extends LoggedTester(mut) {

  private var __running: Boolean = false
  private var __hasInit: Boolean = false

  /* Counter used for latency estimation in simulation. */
  var counter = new Counter

  /** Override the chisel3.iotesters.PeekPokeTester step method to increment counter before actually updating the
   *  simulation time.
   */
  override def step(value: Int): Unit = {
    if (__running) { counter.step(value) }
    super.step(value)
  }

  /** Update simulation time without updating the Counter instance.
   *
   *  Can be used to add random latencies in the test bench with no influence on the performance evaluation.
   */
  def stepNoCount(value: Int): Unit = {
    super.step(value)
  }

  /** pauseSimu and resumeSimu can be used for same reasons than stepNoCount. */
  /** Pause the simulation, i.e. stop counting cycles when stepping. */
  def pauseSimu = {
    if (!__hasInit)
      throw UninitializedSimulationException("Can't call pauseSimu before calling initSimu or after calling endSimu")
    __running = false
  }

  /** Resume the simulation, i.e. resume counting cycles when stepping. */
  def resumeSimu = {
    if (!__hasInit)
      throw UninitializedSimulationException("Can't call resumeSimu before calling initSimu or after calling endSimu")
    __running = true
  }

  /** Initialize simulation of the given module under test
   *
   *  Before anything is done, wait for the module input to be ready .
   */
  def initSimu: Unit = {
    while (!peek(mut.io.in.ready)) {
      step(1)
    }
    __running = true
    __hasInit = true
    counter.reset()
  }

  /* End simulation as for the counter meaning.
   * Simulation may continue, but cycles won't be taken into account in the counter . */
  def endSimu: Unit = {
    if (!__hasInit)
      throw UninitializedSimulationException("Can't call endSimu before calling initSimu or after calling endSimu")
    __running = false
    __hasInit = false
    counter.lock()
  }

  /* Return the value of the counter associated. */
  def getCounter: Int = {
    counter.getCounter()
  }

  /** Sequentially send initial values at the beginning of the simulation
   *
   *  If output were to be valid during this phase, an exception would be thrown, as this method should only be used for
   *  design initialization.
   *
   *  @param values
   *    Array of values to be sent at the beginning of the simulation (after initSimu)
   */
  def sendInitValues(values: Array[BitVector]) = {
    if (!__running) throw SimulationNotRunningException("Can't sendInitValues when simulation is not running")
    if (!__hasInit) throw UninitializedSimulationException("Can't sendInitValues before initSimu")
    for (value <- values) {
      while (!peek(mut.io.in.ready)) {
        if (peek(mut.io.out.valid)) throw MissedOutputException("Output was valid during initialization of the design")
        step(1)
      }
      if (peek(mut.io.out.valid)) throw MissedOutputException("Output was valid during initialization of the design")
      poke(mut.io.in.valid, true)
      poke(mut.io.in.bits, value)
      step(1)
    }
  }
}

/** Counter class used for counting cycle in simulations. */
class Counter {
  private var __locked = true
  var counter: Int     = 0

  /** Set counter with value. */
  def setCounter(value: Int): Unit = {
    if (__locked) throw LockedCounterException("Can't setCounter while it's locked")
    counter = value
  }

  /** Increment the value of the counter with value. */
  def step(value: Int): Unit = {
    if (__locked) throw LockedCounterException("Can't step while counter is locked")
    counter += value
  }

  /** Return the value of the counter. */
  def getCounter(): Int = {
    return counter
  }

  /** Reset the value of the counter. */
  def reset(): Unit = {
    __locked = false
    counter = 0
  }

  def lock() = {
    __locked = true
  }
}
