/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.tests

import chisel3.iotesters.PeekPokeTester
import chisel3.MultiIOModule

abstract class LoggedTester[T <: MultiIOModule](mut: T) extends PeekPokeTester(mut) {

  /** Change here to allow debug printing in your test benches. */
  val targetDir = "test_run_dir"

  private val __debug: Boolean = false

  def debug(str: String) = {
    __debug match {
      case true  => logger.info(s"${this.getClass.getSimpleName}: $str")
      case false =>
    }
  }
}
