/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.utils.tests

import bench.utils.hardware.{RingElem}
import bench.utils.software.{BitVector, Numeric, Comparator, Complex}

import chisel3._
import chisel3.experimental._

import dsptools.numbers.{Ring, DspComplex}

trait PeekPokeRingElem {
  def poke[T <: Data: Ring](signal: RingElem[T], value: Numeric): Unit

  def peek[T <: Data: Ring](signal: RingElem[T]): BitVector
}

class CustomTester[T <: MultiIOModule, S <: Data: Ring](mut: T, signal: S, threshold: Double)
    extends LoggedTester(mut)
    with PeekPokeRingElem {
  /* CustomTester attributes. */
  implicit val mySignal     = signal                // declare signal as implicit for type conversion
  implicit val myComparator = Comparator(threshold) // declare implicit comparator for threshold based comparison

  /** Peek and Poke methods on RingElem types. */
  private def __poke[T <: Data: Ring](signal: T, value: BigInt): Unit = {
    signal match {
      case i: UInt        => this.poke(i, value)
      case s: SInt        => this.poke(s, value)
      case fp: FixedPoint => this.poke(fp, value)
      case _              => throw new UnsupportedOperationException("Unrecognized hardware type")
    }
  }

  def poke[T <: Data: Ring](signal: RingElem[T], value: Numeric): Unit = {
    __poke(signal.value, value.toBits(signal.value).value)
  }

  def poke[T <: Data: Ring](signal: RingElem[T], value: Complex): Unit = {
    val dspSignal  = signal.value.asInstanceOf[DspComplex[_]]
    val realSignal = dspSignal.real.asInstanceOf[Data]
    val imagSignal = dspSignal.imag.asInstanceOf[Data]
    realSignal match {
      case i: UInt        => this.poke(i, value.real.toBits(realSignal))
      case s: SInt        => this.poke(s, value.real.toBits(realSignal))
      case fp: FixedPoint => this.poke(fp, value.real.toBits(realSignal))
      case _              => throw new UnsupportedOperationException("Unrecognized hardware type")
    }
    imagSignal match {
      case i: UInt        => this.poke(i, value.imag.toBits(realSignal))
      case s: SInt        => this.poke(s, value.imag.toBits(realSignal))
      case fp: FixedPoint => this.poke(fp, value.imag.toBits(realSignal))
      case _              => throw new UnsupportedOperationException("Unrecognized hardware type")
    }
  }

  def poke(signal: UInt, value: BitVector): Unit = {
    poke(signal, value.value)
  }

  private def __peek[T <: Data: Ring](signal: T): BigInt = {
    signal match {
      case i: UInt        => this.peek(i)
      case s: SInt        => this.peek(s)
      case fp: FixedPoint => this.peek(fp)
      /* Need to reinterpret values a first time at peeking, then at BitVector deserialization */
      case hc: DspComplex[_] =>
        (hc.real, hc.imag) match {
          case (r: UInt, i: UInt) =>
            val mask      = BitVector.createMask(r.getWidth)
            val maxValue  = (1 << (r.getWidth - 1))
            val real      = __peek(r)
            val imagPoked = __peek(i)
            val imag = if (imagPoked > maxValue) { -(maxValue * 2 - imagPoked) }
            else imagPoked
            ((real & mask) << r.getWidth) + (imag & mask)
          case (r: SInt, i: SInt) =>
            val mask      = BitVector.createMask(r.getWidth)
            val maxValue  = (1 << (r.getWidth - 1))
            val real      = __peek(r)
            val imagPoked = __peek(i)
            val imag = if (imagPoked > maxValue) { -(maxValue * 2 - imagPoked) }
            else imagPoked
            ((real & mask) << r.getWidth) + (imag & mask)
          case (r: FixedPoint, i: FixedPoint) =>
            val mask      = BitVector.createMask(r.getWidth)
            val maxValue  = (1 << (r.getWidth - 1))
            val real      = __peek(r)
            val imagPoked = __peek(i)
            val imag = if (imagPoked > maxValue) { -(maxValue * 2 - imagPoked) }
            else imagPoked
            ((real & mask) << r.getWidth) + (imag & mask)
          case _ =>
            throw new UnsupportedOperationException(s"Unrecognized hardware type when peeking a DspComplex: ${hc.real}")
        }
      case _ => throw new UnsupportedOperationException(s"Unrecognized hardware type when peeking : $signal")
    }
  }

  def peek[T <: Data: Ring](signal: RingElem[T]): BitVector = {
    BitVector(__peek(signal.value))(signal.value)
  }
}
