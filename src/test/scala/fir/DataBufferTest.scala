/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.fir

import bench.utils.hardware.RingElem
import bench.utils.software._
import bench.utils.generation.Require

import bench.tags._

import logger.LazyLogging

import chisel3._
import chisel3.iotesters.{PeekPokeTester, ChiselFlatSpec}
import dsptools.numbers.Ring

class DataBufferTest[T <: Data: Ring](c: DataBuffer[T], gen: T, bitWidth: Int) extends PeekPokeTester(c) {
  require(Require.divide(gen.getWidth, bitWidth), s"Element width must divide input bitwidth")
  val elemWidth = gen.getWidth;
  val nbElement = bitWidth / elemWidth

  poke(c.io.stall, false)
  for (_ <- 0 to 5) {
    val seq = (0 until nbElement).map(_ => Numeric.random(gen, 255).toBits(gen).value)
    poke(c.io.dataIn, seq)
    step(1)
  }
  poke(c.io.stall, true)
  step(2)
}

object DataBufferHelper extends LazyLogging {

  def verilatorTest[T <: Data: Ring](gen: T, bitWidth: Int, buffSize: Int): Boolean = {
    logger.info(
        s"${this.getClass.getSimpleName}: " +
          s"Verilator tester with elements of type $gen with bitwidth $bitWidth and buffSize $buffSize"
    )
    iotesters.Driver.execute(
        Array("--backend-name", "verilator"),
        () => new DataBuffer(RingElem(gen), bitWidth, buffSize)
    ) { c => new DataBufferTest(c, gen, bitWidth) }
  }

  def FirRTLTest[T <: Data: Ring](gen: T, bitWidth: Int, buffSize: Int): Boolean = {
    logger.info(
        s"${this.getClass.getSimpleName}: " +
          s"FIRRTL tester with elements of type $gen with bitwidth $bitWidth and buffSize $buffSize"
    )
    iotesters.Driver.execute(
        Array(
            "--generate-vcd-output",
            "on",
            "--target-dir",
            "test_run_dir/test_fir/test_databuffer",
            "--top-name",
            "dataBuffer"
        ),
        () => new DataBuffer(RingElem(gen), bitWidth, buffSize)
    ) { c => new DataBufferTest(c, gen, bitWidth) }
  }
}

class DataBufferTester extends ChiselFlatSpec {
  val elemType = UInt(32.W)
  val bitWidth = elemType.getWidth * 2
  var buffSize = 5

  behavior of "DataBuffer"
  it should s"" taggedAs (Unit) in {
    DataBufferHelper.FirRTLTest(elemType, bitWidth, buffSize) should be(true)
  }
}
