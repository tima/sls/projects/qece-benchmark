/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.fir

import bench.utils.tests.LatencyExtractTester
import bench.utils.hardware.RingElem
import bench.utils.software._
import bench.utils.components.PosEdgeWrapper
import bench.utils.convert._
import bench.utils.generation.{SinusSignalGenerator, SignalGenerator, SumSignalsGenerator}

import bench.tags._

import chisel3._
import chisel3.experimental.FixedPoint
import chisel3.iotesters.ChiselFlatSpec
import dsptools.numbers.Ring

/* Testing the design */
class DynamicFIRTest[T <: Data: Ring](
    mut: PosEdgeWrapper,
    bitWidth: Int,
    elemType: T,
    coeffs: Array[Numeric],
    nSample: Int = 1000
) extends LatencyExtractTester(mut) {
  implicit val comparator = Comparator(5.0)
  implicit val signal     = elemType
  val elemWidth           = elemType.getWidth
  val nbElement           = bitWidth / elemWidth

  val sineWaves = Array[SignalGenerator](
      // Generate a sine with a frequency of 10 MHz
      new SinusSignalGenerator(10000000, 255, 100000000, elemType.isInstanceOf[FixedPoint]),
      // Generate a sine with a frequency of 1 MHz
      new SinusSignalGenerator(1000000, 255, 100000000, elemType.isInstanceOf[FixedPoint])
  )

  /* signalGenerator is two instances of a same SignalGenerator.
   * They will be consumed independently by software and hardware. */
  val signalGenerator = new SumSignalsGenerator(sineWaves, elemType.isInstanceOf[FixedPoint]).duplicate
  val swReference     = new FirRef(coeffs, nbElement, signalGenerator._1, elemType.isInstanceOf[FixedPoint])

  /** check the output in agreement with SW */
  def checkOutput(firSW: FirRef): Unit = {
    if (peek(mut.io.out.valid)) {
      val values = BitVector.deserialize(peek(mut.io.out.bits), bitWidth, elemWidth)
      values.map(v => {
        val ref = firSW.compute()
        debug("Output : " + v + " (ref = " + ref + ")")
        expect(ref == v, s"Unexpected value: $v vs $ref")
      })
    }
  }

  initSimu
  poke(mut.io.out.ready, true)

  for (i <- 0 until (coeffs.size / nbElement)) {
    poke(mut.io.in.valid, true)
    val signal = (0 until nbElement).map(j => coeffs(i * nbElement + j).toBits(elemType)).reverse.toArray
    poke(mut.io.in.bits, BitVector.serialize(signal))
    step(1)
    expect(!peek(mut.io.out.valid), s"Loading coefficients, output can't be valid yet")
  }
  poke(mut.io.in.valid, false)

  // stalling for test purpose
  step(50)

  poke(mut.io.in.valid, true)
  for (_ <- 0 until nSample) {
    while (!peek(mut.io.in.ready)) step(1) // Wait if the HW is not ready.
    poke(mut.io.in.valid, true)

    // Generate a vector of elements
    val signal = (0 until nbElement).map(_ => signalGenerator._2.next().toBits(elemType)).toArray
    debug("Poke signal : (" + signal.mkString(", ") + ")")
    poke(mut.io.in.bits, BitVector.serialize(signal))

    step(1)
    checkOutput(swReference) // Check then output in agreement with SW.

    poke(mut.io.in.valid, false)
  }

  endSimu
}

object DynamicFIRHelper extends logger.LazyLogging {
  def verilatorTest[T <: Data: Ring](bitwidth: Int, gen: T, coeffs: Array[Numeric], nSample: Int = 1000): Boolean = {
    logger.info(s"${this.getClass.getSimpleName} : Verilator tester with elements of type $gen with bitwidth $bitwidth")
    try {
      iotesters.Driver.execute(
          Array(
              "--backend-name",
              "verilator",
              "--target-dir",
              "test_run_dir/dynamicFirTester",
              "--top-name",
              "DynamicFIR"
          ),
          () => new PosEdgeWrapper(new FirGenRole(bitwidth, RingElem(gen), coeffs.size))
      ) { c => new DynamicFIRTest(c, bitwidth, gen, coeffs, nSample) }
    } catch {
      case _: java.lang.Exception => true
      case e: Throwable           => throw e
    }
  }
}

class DynamicFIRTester extends ChiselFlatSpec {
  val elem       = FixedPoint(32.W, 10.BP)
  val nbElemList = List(1, 2, 8, 16)
  val nbTapList  = List(16, 128, 256)
  // val coeffs = bench.kernels.fir.FirUtil.defaultCoeffs(true)

  for (nbElem <- nbElemList) {
    for (nbTap <- nbTapList) {
      // Computed parameters
      val coeffs   = bench.kernels.fir.FirUtil.generate(nbTap, true)
      val bitwidth = elem.getWidth * nbElem
      s"Dynamic FIR testing on $nbElem elements of type $elem with $nbTap tap" should
        "validate FIR design" taggedAs (Kernel, Long) in {
        DynamicFIRHelper.verilatorTest(bitwidth, elem, coeffs) should be(true)
      }
    }
  }
}

/** Used for implementation and debug. */
class DynamicFIRSpecTester extends ChiselFlatSpec {
  // val elem      = SInt(32.W)
  val elem      = FixedPoint(32.W, 16.BP)
  val nbElement = 8
  // val coeffs    = bench.kernels.fir.FirUtil.defaultCoeffs(true)
  val coeffs  = bench.kernels.fir.FirUtil.generate(256, true)
  val nSample = 1000

  // Computed parameters
  val bitwidth = elem.getWidth * nbElement

  "using --backend-name verilator" should "be an alternative way to run using verilator" taggedAs (Kernel) in {
    DynamicFIRHelper.verilatorTest(bitwidth, elem, coeffs, nSample) should be(true)
  }
}
