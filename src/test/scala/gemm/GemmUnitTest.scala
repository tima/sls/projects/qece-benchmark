/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.gemm

import bench.utils.components.PosEdgeWrapper
import bench.utils.software.{Numeric, Comparator, BitVector}
import bench.utils.hardware.RingElem
import bench.utils.tests.LatencyExtractTester

import bench.utils.exceptions._
import bench.utils.convert._

import bench.kernels.matrixRef.{Matrix, BitMatrix}

import bench.tags._

import chisel3._
import chisel3.experimental._
import dsptools.numbers._

import chisel3.iotesters
import chisel3.iotesters.ChiselFlatSpec

/** Unit tester for the GEMM algorithm
 *
 *  Implements the LatencyExtractTester for functional simulation and latency extraction
 *
 *  @constructor
 *    create a new Unit tester from a GEMM Role, the size of the kernels and the type of the elements
 *  @param role
 *    GEMM kernel
 *  @param size
 *    size of matrix kernels
 *  @param gen
 *    type of elements in the matrixes
 */
class GemmUnitTester[T <: Data: Ring](mut: PosEdgeWrapper, size: Int, gen: T) extends LatencyExtractTester(mut) {
  implicit val comparator = Comparator(1.0)
  implicit val signal     = gen

  val elemWidth = gen.getWidth

  /** Compute GEMM product using the module under test
   *
   *  @param matA
   *    first matrix operand
   *  @param matB
   *    second matrix operand
   *  @param matC
   *    third matrix operand
   *  @param alpha
   *    first constant
   *  @param beta
   *    second constant
   *  @param mut
   *    module under test to be used for GEMM computation
   *  @return
   *    the result of the GEMM product on the operand, as computed by the module under test
   */
  def testHw(matA: BitMatrix, matB: BitMatrix, matC: BitMatrix, alpha: BitVector, beta: BitVector): BitMatrix = {
    val nbElemCycle = mut.bitWidth / elemWidth
    val result      = BitMatrix(matA.getSize)
    val matBT       = matB.transpose // Transpose b matrix for protocol compliance

    val mode      = BitVector(GemmMode.getMode(true, false, true))
    val constList = Array(mode, alpha, beta)

    initSimu
    /* Sending initial values : mode, alpha and beta. */
    sendInitValues(constList)
    while (!result.isFull) {
      poke(mut.io.out.ready, true)
      if (!matC.isEmpty) {
        if (peek(mut.io.in.ready)) {
          debug("Sending C")
          poke(mut.io.in.valid, true)
          poke(mut.io.in.bits, BitVector.serialize(matC.getTokens(nbElemCycle)))
        }
      } else {
        if (!matBT.isEmpty) {
          if (peek(mut.io.in.ready)) {
            debug("Sending B")
            poke(mut.io.in.valid, true)
            poke(mut.io.in.bits, BitVector.serialize(matBT.getTokens(nbElemCycle)))
          }
        } else {
          if (!matA.isEmpty) {
            if (peek(mut.io.in.ready)) {
              debug("Sending A")
              poke(mut.io.in.valid, true)
              poke(mut.io.in.bits, BitVector.serialize(matA.getTokens(nbElemCycle)))
            }
          }
        }
      }
      if (peek(mut.io.out.valid)) {
        // Retrieve output when valid
        val output = peek(mut.io.out.bits)
        result.pushTokens(BitVector.deserialize(output, mut.bitWidth, elemWidth))
      }
      step(1)
    }
    debug(result.prettyToString("Result is"))
    result
  }

  def testSw(a: Matrix, b: Matrix, c: Matrix, alpha: Numeric, beta: Numeric): Matrix = {
    val gemm = ((a * b) * alpha) + c * beta
    debug(gemm.prettyToString("Golden Reference"))
    gemm
  }

  def compareResult(a: Matrix, b: Matrix, c: Matrix, alpha: Numeric, beta: Numeric): Unit = {
    expect(
        testSw(a, b, c, alpha, beta) == testHw(
            BitMatrix(gen, a),
            BitMatrix(gen, b),
            BitMatrix(gen, c),
            alpha.toBits(gen),
            beta.toBits(gen)
        ),
        s"GEMM design not conform to Golden Reference"
    )
  }

  /* Test values */
  val a     = Matrix(size, gen, 5)
  val b     = Matrix(size, gen, 5)
  val c     = Matrix(size, gen, 5)
  val alpha = Numeric.random(gen, 5)
  val beta  = Numeric.random(gen, 5)
  debug(s"Computing $alpha * A * B + $beta * C")
  debug(a.prettyToString("A"))
  debug(b.prettyToString("B"))
  debug(c.prettyToString("C"))
  compareResult(a, b, c, alpha, beta)

}

object GemmHelper extends logger.LazyLogging {
  def verilatorTest[T <: Data: Ring](genArray: T, bitWidth: Int, size: Int): Boolean = {
    try {
      logger.info(
          s"Gemm : verilator tester with elements of type $genArray, IO bitwidth $bitWidth and matrix size $size"
      )
      iotesters.Driver.execute(
          Array(
              "--backend-name",
              "verilator",
              "--target-dir",
              "test_run_dir/test_gemm",
              "--top-name",
              "GEMM"
          ),
          () => new PosEdgeWrapper(new GemmRole(bitWidth)(RingElem(genArray), size))
      ) { c =>
        new GemmUnitTester(c, size, genArray)
      }
    } catch {
      case _: RepresentationOverflowException =>
        logger.info("Overflow detected on design, not checking validity further")
        true
      case _: Throwable => false
    }
  }
}

class GemmTester extends ChiselFlatSpec {
  def doFPTest(bitWidth: Int, size: Int) = {
    val elemType = FixedPoint(32.W, 16.BP)
    if ((bitWidth >= elemType.getWidth) & ((bitWidth / elemType.getWidth) <= size)) {
      s"Verilator test with params (type = $elemType, matrix size = $size, I/O bitWidth = $bitWidth) " +
        s"using verilator backend" should
        "be used as an alternative way to run using verilator" taggedAs (Kernel, Long) in {
        GemmHelper.verilatorTest(elemType, bitWidth, size) should be(true)
      }
    }
  }
  def doComplexTest(bitWidth: Int, size: Int) = {
    val elemType = DspComplex(FixedPoint(32.W, 16.BP))
    if ((bitWidth >= elemType.getWidth) & ((bitWidth / elemType.getWidth) <= size)) {
      s"Verilator test with params (type = $elemType, matrix size = $size, I/O bitWidth = $bitWidth) " +
        s"using verilator backend" should
        "be used as an alternative way to run using verilator" taggedAs (Kernel, Long) in {
        GemmHelper.verilatorTest(elemType, bitWidth, size) should be(true)
      }
    }
  }
  def doUIntTest(bitWidth: Int, size: Int) = {
    val elemType = UInt(32.W)
    if ((bitWidth >= elemType.getWidth) & ((bitWidth / elemType.getWidth) <= size)) {
      s"Verilator test with params (type = $elemType, matrix size = $size, I/O bitWidth = $bitWidth) " +
        s"using verilator backend" should
        "be used as an alternative way to run using verilator" taggedAs (Kernel, Long) in {
        GemmHelper.verilatorTest(elemType, bitWidth, size) should be(true)
      }
    }
  }
  def doSIntTest(bitWidth: Int, size: Int) = {
    val elemType = SInt(64.W)
    if ((bitWidth >= elemType.getWidth) & ((bitWidth / elemType.getWidth) <= size)) {
      s"Verilator test with params (type = $elemType, matrix size = $size, I/O bitWidth = $bitWidth) " +
        s"using verilator backend" should
        "be used as an alternative way to run using verilator" taggedAs (Kernel, Long) in {
        GemmHelper.verilatorTest(elemType, bitWidth, size) should be(true)
      }
    }
  }
  val bitWidthList = List(64, 512)
  val sizeList     = List(64, 128)
  for (size <- sizeList) {
    for (bitWidth <- bitWidthList) {
      doFPTest(bitWidth, size)
      doUIntTest(bitWidth, size)
      doSIntTest(bitWidth, size)
      doComplexTest(bitWidth, size)
    }
  }
}

class GemmSpecTester extends ChiselFlatSpec {
  val bitWidth = 512
  val elemType = UInt(32.W)
  // val elemType    = FixedPoint(4.W, 2.BP)
  // val elemType    = DspComplex(FixedPoint(64.W, 32.BP))
  val matSize = 128
  s"Verilator test with params (type = $elemType, matrix size = $matSize, I/O bitWidth = $bitWidth) " +
    s"using verilator backend" should
    "be used as an alternative way to run using verilator" taggedAs (Kernel) in {
    GemmHelper.verilatorTest(elemType, bitWidth, matSize) should be(true)
  }
}
