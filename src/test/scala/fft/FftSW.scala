/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.fft

import bench.utils.software.{Complex, Numeric}

import chisel3.util.log2Ceil

import scala.collection.AbstractIterator

abstract class FftSW(size: Int) extends logger.LazyLogging {

  def compute(samples: Array[Numeric]): Array[Numeric]

  def getFrequencies(samplingFrequency: Int): Array[Double] = {
    val deltaF = samplingFrequency.toDouble / size
    val freqs  = new Array[Double](size)
    for (i <- 0 until size) {
      freqs(i) = deltaF * i
    }
    freqs
  }
}

class Radix2DIT(size: Int) extends FftSW(size) {
  val preComputedTwiddleFactors = getTwiddleFactors()

  def getTwiddleFactors(): Array[Array[Numeric]] = {
    val log2size       = (Math.log(size) / Math.log(2)).toInt
    val twiddleFactors = new Array[Array[Numeric]](log2size)

    for (puiss <- 1 to log2size) {
      val n = Math.pow(2, puiss).toInt
      twiddleFactors(puiss - 1) = new Array[Numeric](n / 2)
      for (k <- 0 until n / 2) {
        val phase = -2 * Math.PI * k / n
        twiddleFactors(puiss - 1)(k) = Numeric(Complex(Math.cos(phase), Math.sin(phase)))
      }
    }

    twiddleFactors
  }

  def recursive_fft(samples: Array[Numeric], N: Int): Array[Numeric] = {
    if (samples.length == 1) {
      samples
    } else {
      // Step 1 : split
      val M     = samples.length / 2
      val Xeven = new Array[Numeric](M)
      val Xodd  = new Array[Numeric](M)
      for (k <- 0 until M) {
        Xeven(k) = samples(2 * k)
        Xodd(k) = samples(2 * k + 1)
      }

      // Step 2 : Compute
      val Feven = recursive_fft(Xeven, M)
      val Fodd  = recursive_fft(Xodd, M)

      // Step 3 : combine and compute
      val Fcombined = new Array[Numeric](N)
      for (k <- 0 until M) {
        val r = preComputedTwiddleFactors((Math.log(N) / Math.log(2)).toInt - 1)(k) * Fodd(k)
        Fcombined(k) = Feven(k) + r
        Fcombined(k + M) = Feven(k) - r
      }
      Fcombined
    }
  }

  override def compute(samples: Array[Numeric]): Array[Numeric] = {
    assert(samples.size == size)
    recursive_fft(samples, size)
  }
}

class Radix2DIF(size: Int) extends FftSW(size) {

  def getTwiddleFactor(k: Int, sampleSize: Int): Numeric = {
    val stageLevel   = log2Ceil(size) - log2Ceil(sampleSize) + 1
    val twiddleIndex = (math.pow(2, stageLevel).intValue() * k) / 2
    val phase        = -2 * Math.PI * twiddleIndex / size
    val result       = Numeric(Complex(Math.cos(phase), Math.sin(phase)))
    //logger.debug(s"stage $stageLevel k = $k (twiddleIndex = $twiddleIndex) value = $result")
    result
  }

  def recursive_fft(samples: Array[Numeric], N: Int): Array[Numeric] = {
    if (N == 1) {
      samples
    } else {
      val M      = samples.length / 2
      val Xleft  = new Array[Numeric](M)
      val Xright = new Array[Numeric](M)
      for (k <- 0 until M) {
        Xleft(k) = samples(k) + samples(k + M)
        Xright(k) = (samples(k) - samples(k + M)) * getTwiddleFactor(k, N)
      }
      val FLeft     = recursive_fft(Xleft, M)
      val FRight    = recursive_fft(Xright, M)
      val Fcombined = new Array[Numeric](N)
      for (k <- 0 until M) {
        Fcombined(2 * k) = FLeft(k)
        Fcombined(2 * k + 1) = FRight(k)
      }
      Fcombined
    }
  }

  override def compute(samples: Array[Numeric]): Array[Numeric] = {
    assert(samples.size == size)
    recursive_fft(samples, size)
  }
}

class fftSWIterator(fftSW: FftSW, samples: Array[Array[Numeric]]) extends AbstractIterator[Numeric] {
  var countFFTSample = 0
  var countSample    = 0
  var resultFFT      = fftSW.compute(samples(0))
  var available      = true

  override def hasNext: Boolean = {
    available
  }

  override def next(): Numeric = {
    val result = resultFFT(countSample)
    if (countSample == samples(0).length - 1) {
      countFFTSample += 1
      countSample = 0
      if (countFFTSample == samples.length) {
        available = false
      } else {
        resultFFT = fftSW.compute(samples(countFFTSample))
      }
    } else {
      countSample += 1
    }
    result
  }
}

object FftSWTesting extends App with logger.LazyLogging {
  val fftSize = 8
  val fftSW   = new Radix2DIF(fftSize)
  val input   = new Array[Numeric](fftSize)

  for (i <- 0 until fftSize) {
    input(i) = Numeric(Complex(i, 0))
  }

  val result = fftSW.compute(input)
  (0 until result.size).zip(result).foreach { case (i, r) => logger.info(s"$i => $r") }
}
