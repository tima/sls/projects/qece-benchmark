/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.fft

import bench.utils.hardware.RingElem
import bench.utils.software.{BitVector, Complex, Numeric}

import bench.tags._

import chisel3.experimental.FixedPoint
import chisel3._
import chisel3.iotesters.{ChiselFlatSpec, PeekPokeTester}
import dsptools.numbers.{DspComplex, Ring}

class FFTFIFOInterfaceTest[T <: Data: Ring](c: FFTFIFOInterface[T], elemType: T, size: Int, lanes: Int)
    extends PeekPokeTester(c) {
  for (n <- 0 until 3) {
    poke(c.io.enable, true)
    for (i <- 0 until size by lanes) {
      val array = (0 until lanes).map(j => Numeric(Complex(i + j + 100 * n, 0)).toBits(elemType)).toArray.reverse
      poke(c.io.in, BitVector.serialize(array))
      step(1)
    }
    poke(c.io.enable, false)
    step(5)
  }
  step(1)
}

object FFTFIFOInterfaceTestHelper extends logger.LazyLogging {
  def verilatorTest[T <: Data: Ring](gen: T, size: Int, lanes: Int): Boolean = {
    logger.info(s"${this.getClass.getSimpleName} : Verilator tester with elements of type $gen with size $size")
    iotesters.Driver.execute(
        Array(
            "--backend-name",
            "verilator",
            "--target-dir",
            "test_run_dir/test_fft/test_fft_inteface",
            "--top-name",
            "FFTFIFOInterface"
        ),
        () => new FFTFIFOInterface(RingElem(gen), size, lanes)
    ) { c => new FFTFIFOInterfaceTest(c, gen, size, lanes) }
  }
}

class FFTFIFOInterfaceTester extends ChiselFlatSpec {
  val elemType = DspComplex(FixedPoint(32.W, 16.BP))
  val fftSize  = 32
  val lanes    = 4
  it should s"pass with verilator" taggedAs (Unit) in {
    FFTFIFOInterfaceTestHelper.verilatorTest(elemType, fftSize, lanes) should be(true)
  }
}
