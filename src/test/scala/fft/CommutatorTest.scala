/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.fft

import bench.kernels.fft.R2MDC.Commutator

import bench.utils.hardware.RingElem
import bench.utils.generation.Require

import bench.tags._

import breeze.math

import chisel3._
import chisel3.iotesters.ChiselFlatSpec
import dsptools.numbers._
import dsptools.DspTester

class CommutatorWrapper[T <: Data: Ring](elemType: RingElem[T], delay: Int) extends Module {
  val io = IO(new Bundle() {
    val enable = Input(Bool())
    val in0    = Input(DspComplex(elemType.value))
    val in1    = Input(DspComplex(elemType.value))
    val out0   = Output(DspComplex(elemType.value))
    val out1   = Output(DspComplex(elemType.value))
  })
  val commutator = Module(new Commutator(elemType, delay))
  val in0Reg     = Reg(DspComplex(elemType.value))
  val in1Reg     = Reg(DspComplex(elemType.value))
  val enableReg  = Reg(Bool())
  in0Reg := io.in0
  in1Reg := io.in1
  enableReg := io.enable
  commutator.io.in0 := in0Reg
  commutator.io.in1 := in1Reg
  io.out0 := commutator.io.out0
  io.out1 := commutator.io.out1
  commutator.io.enable := enableReg
}

class CommutatorTest[T <: Data: Ring](c: CommutatorWrapper[T], gen: T, delay: Int, size: Int) extends DspTester(c) {
  require(Require.isPow2(size), s"size must a power of 2")
  poke(c.io.enable, true)

  for (i <- 0 until size) {
    poke(c.io.in0, new math.Complex(i, 0))
    poke(c.io.in1, new math.Complex(i + size, 0))
    step(1)
  }

}

object CommutatorTestHelper extends logger.LazyLogging {
  def verilatorTest[T <: Data: Ring](gen: T, delay: Int, size: Int): Boolean = {
    logger.info(s"${this.getClass.getSimpleName} : Verilator tester with elements of type $gen with size $size")
    iotesters.Driver.execute(
        Array("--backend-name", "verilator"),
        () => new CommutatorWrapper(RingElem(gen), delay)
    ) { c => new CommutatorTest(c, gen, delay, size) }
  }

  def FirRTLTest[T <: Data: Ring](gen: T, delay: Int, size: Int): Boolean = {
    logger.info(s"${this.getClass.getSimpleName} : FIRRTL tester with elements of type $gen with size $size")
    iotesters.Driver.execute(
        Array(
            "--generate-vcd-output",
            "on",
            "--target-dir",
            "test_run_dir/test_fft/test_commutator",
            "--top-name",
            "fftModule"
        ),
        () => new CommutatorWrapper(RingElem(gen), delay)
    ) { c => new CommutatorTest(c, gen, delay, size) }
  }
}

class CommutatorTestTester extends ChiselFlatSpec {
  val elemType = UInt(32.W)
  val fftSize  = 8

  behavior of "Commutator"
  it should s"pass with firrtl" taggedAs (Deprecated) in {
    CommutatorTestHelper.FirRTLTest(elemType, fftSize / 2, fftSize) should be(true)
  }

  it should s"pass with verilator" taggedAs (Deprecated, Unit) in {
    CommutatorTestHelper.verilatorTest(elemType, fftSize / 2, fftSize) should be(true)
  }
}
