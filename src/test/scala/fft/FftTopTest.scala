/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.fft

import bench.utils.hardware.RingElem
import bench.utils.convert._
import bench.utils.components.PosEdgeWrapper
import bench.utils.software.{BitVector, Comparator, Complex, Numeric}
import bench.utils.tests.LatencyExtractTester

import bench.tags._

import chisel3._
import chisel3.iotesters.ChiselFlatSpec
import chisel3.experimental.FixedPoint
import dsptools.numbers._

/** Testing FFT kernel
 *
 *  @param mut
 *    fft role being tested
 *  @param gen
 *    elementType to this FFT (should be complex over fixed points)
 *  @param bitWidth
 *    I/O bitwidth for the kernel
 *  @param size
 *    size of the ffts being computed in the kernel
 *  @param nbFft
 *    number of FFT to compute in order to perform the test
 */
class FftRoleTest[T <: Data: Ring](mut: PosEdgeWrapper, gen: T, bitWidth: Int, size: Int, nbFft: Int = 2)
    extends LatencyExtractTester(mut) {
  implicit val comparator = Comparator(1.0)
  implicit val signal     = gen

  // Data generation parameters
  val sampleFrequency = 100          // Sampling frequency
  val signalFrequency = Seq(1, 3, 5) // Frequencies of the signal

  val lanes = bitWidth / gen.getWidth

  def generateSamples(nb: Int): Array[Array[Numeric]] = {

    val samples = Array.ofDim[Numeric](nb, size)
    for (j <- 0 until nb) {
      for (i <- 0 until size) {
        val t   = (i * size + j).toDouble / sampleFrequency
        var sum = Complex(0, 0)
        signalFrequency.foreach(freq => sum += Complex(Math.cos(2 * Math.PI * freq * t), 0))
        samples(j)(i) = Numeric(sum)
        //samples(j)(i) = Numeric(Complex(i, 0))
      }
    }
    samples
  }

  def checkOutput(): Unit = {
    if (peek(mut.io.out.valid)) {
      val output = peek(mut.io.out.bits)
      val values = BitVector.deserialize(output, bitWidth, gen.getWidth)
      for (value <- values) {
        if (goldenFFT.hasNext) {
          val golden = goldenFFT.next()
          debug("Output : " + value + " vs " + golden)
          expect(comparator.compare(golden, value), "FFT design not conform to Golden reference")
        } else {
          logger.warning("An exceeding value was given (value = " + value + ")")
        }
      }
    }
  }

  val samplesArray = generateSamples(nbFft)
  val fftSW        = new Radix2DIF(size)
  val goldenFFT    = new fftSWIterator(fftSW, samplesArray)

  initSimu

  poke(mut.io.in.valid, true)
  for (samples <- samplesArray) {
    for (n <- 0 until size / lanes) {
      val array = new Array[Numeric](lanes)
      for (lane <- 0 until lanes) {
        array(lane) = samples(n * lanes + lane)
      }
      /* for(j <- 0 until lanes by 2) { array(j) = samples(n + size/lanes * (j / 2)) array(j + 1) = samples(n + size/2 +
       * size/lanes * (j / 2)) } */
      debug("Poke : ")
      array.foreach(v => debug(v.complexValue.get.toString))
      poke(mut.io.in.bits, BitVector.serialize(array.reverse.map(num => num.toBits(gen))))
      checkOutput()
      step(1)
    }
  }

  // Because flushing is not really possible (no way to know that's the end)
  // we continue to send "valid" data until we recover the last data.
  poke(mut.io.in.valid, true)
  for (_ <- 0 until mut.mut.asInstanceOf[FftRole[T]].getDelay()) {
    checkOutput()
    step(1)
  }
  poke(mut.io.in.valid, false)

  endSimu
  println(s"FFT of size $size with elem $gen [${gen.getWidth}] and I/O $bitWidth compeleted in $getCounter cycles")

  if (goldenFFT.hasNext) {
    logger.warning("Not all expected values was given")
  }

}

object FftRoleTestHelper extends logger.LazyLogging {
  def verilatorTest[T <: Data: Ring](gen: T, bitWidth: Int, size: Int): Boolean = {
    val nbFft = 2
    logger.info(s"${this.getClass.getSimpleName} : Verilator tester with elements of type $gen with size $size")
    iotesters.Driver.execute(
        Array(
            "--backend-name",
            "verilator",
            "--target-dir",
            "test_run_dir/test_fft/test_fft_top",
            "--top-name",
            "FftRole"
        ),
        () => new PosEdgeWrapper(new FftRole(bitWidth, RingElem(gen), size))
    ) { mut => new FftRoleTest(mut, gen, bitWidth, size, nbFft) }
  }
}

class FftTester extends ChiselFlatSpec {
  val elemType    = DspComplex(FixedPoint(32.W, 16.BP))
  val fftSizeList = List(32, 64, 256)
  val lanesList   = List(2, 32, 64)

  for (fftSize <- fftSizeList; lanes <- lanesList) {
    if (lanes <= fftSize) {
      val bitWidth = elemType.getWidth * lanes

      s"FFT of size $fftSize with $lanes lanes" should s"pass with verilator" taggedAs (Kernel, Long) in {
        FftRoleTestHelper.verilatorTest(elemType, bitWidth, fftSize) should be(true)
      }
    }
  }
}

class FftRoleTester extends ChiselFlatSpec {
  val elemType = DspComplex(FixedPoint(32.W, 16.BP))
  val fftSize  = 32
  val lanes    = 32
  val bitWidth = elemType.getWidth * lanes

  it should s"pass with verilator" taggedAs (Kernel) in {
    FftRoleTestHelper.verilatorTest(elemType, bitWidth, fftSize) should be(true)
  }
}
