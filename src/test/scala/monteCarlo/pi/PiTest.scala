/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.monteCarlo.pi

import bench.kernels.monteCarlo._

import bench.utils.components.PosEdgeWrapper
import bench.utils.components.random.TauswortheConfig
import bench.utils.exceptions._
import bench.utils.hardware.RingElem
import bench.utils.software.{BitVector, Comparator}
import bench.utils.tests.LatencyExtractTester

import bench.tags._

import logger.LazyLogging

import chisel3.experimental._
import chisel3.iotesters.ChiselFlatSpec
import chisel3.{iotesters, _}
import dsptools.numbers._

/* Testing the design */
class PiTest[T <: Data: Ring](mut: PosEdgeWrapper, gen: T, nbIterations: Int) extends LatencyExtractTester(mut) {
  implicit val signal     = gen
  implicit val comparator = Comparator(0.25)

  val nbResults = 2

  def waitForValue(): Unit = {
    while (peek(mut.io.out.valid) == 0) {
      step(1)
    }
  }

  initSimu
  for (_ <- 0 until nbResults) {
    waitForValue()
    val hwValue = BitVector.deserialize(peek(mut.io.out.bits), gen.getWidth, gen.getWidth)
    debug("Hardware result : " + hwValue(0))
    step(1)
  }
  endSimu
}

object PiHelper extends LazyLogging {
  def verilatorTest[T <: Data: Ring](
      gen: T,
      urngConfig: Iterator[(Array[TauswortheConfig], Array[TauswortheConfig])],
      nbIterations: Int,
      nbCore: Int
  ): Boolean = {
    val elemType = RingElem(gen)
    try {
      logger.info(s"${this.getClass.getSimpleName} : Verilator tester with elements of type $gen")
      iotesters.Driver.execute(
          Array(
              "--backend-name",
              "verilator",
              "--target-dir",
              "test_run_dir/test_pi",
              "--top-name",
              "Pi"
          ),
          () => {
            new PosEdgeWrapper(
                new MonteCarloRole(
                    elemType,
                    new PiFactory(
                        elemType,
                        urngConfig
                    ),
                    nbIterations,
                    nbCore
                )
            )
          }
      ) { c => new PiTest(c, gen, nbIterations) }
    } catch {
      case _: RepresentationOverflowException =>
        logger.warn("Overflow detected on design, not checking validity further")
        true
      case _: Throwable => false
    }
  }
}

/** Used for implementation and debug. */
class PiSpecTester extends ChiselFlatSpec {
  // Monte Carlo generic config
  val elem         = FixedPoint(32.W, 16.BP)
  val nbIterations = 16384
  val nbCore       = 4

  // Black Scholes specific config
  val tauswortheConfigIterator = TauswortheConfig.randomTupleIterator(123, elem.getWidth)

  "running verilator" should "validate Monte Carlo with Black Scholes cores" taggedAs (Kernel, Probabilistic) in {
    PiHelper.verilatorTest(
        elem,
        tauswortheConfigIterator,
        nbIterations,
        nbCore
    ) should be(true)
  }
}
