/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.monteCarlo.pi

import bench.utils.components.random.TauswortheConfig
import bench.utils.exceptions.RepresentationOverflowException
import bench.utils.hardware.RingElem
import bench.utils.tests.CustomTester

import plotly.{Histogram, Plotly}

import bench.tags._

import logger.LazyLogging

import chisel3._
import chisel3.experimental.FixedPoint
import chisel3.iotesters.ChiselFlatSpec
import dsptools.numbers.Ring

class PiCoreTest[T <: Data: Ring](c: PiCore[T], gen: T, nbIterations: Int) extends CustomTester(c, gen, 0.25) {
  var cntValues = 0
  var sum       = 0.0

  while (cntValues < nbIterations) {
    if (peek(c.io.output.valid) == 1) {
      debug("Output value " + peek(c.io.output.bits))
      sum += peek(c.io.output.bits).toDouble
      cntValues += 1
    }
    step(1)
  }

  val hwResult = 4 * (sum / nbIterations)
  debug("Hardware result : " + hwResult)
}

object PiCoreHelper extends LazyLogging {
  def verilatorTest[T <: Data: Ring](
      gen: T,
      urng: (Array[TauswortheConfig], Array[TauswortheConfig]),
      nbIterations: Int
  ): Boolean = {
    try {
      logger.info(s"${this.getClass.getSimpleName} : Verilator tester with elements of type $gen")
      iotesters.Driver.execute(
          Array(
              "--backend-name",
              "verilator",
              "--target-dir",
              "test_run_dir/test_pi_core",
              "--top-name",
              "PiCore"
          ),
          () => new PiCore(RingElem(gen), urng)
      ) { c =>
        new PiCoreTest(c, gen, nbIterations)
      }
    } catch {
      case _: RepresentationOverflowException =>
        logger.warn("Overflow detected on design, not checking validity further")
        true
      case _: Throwable => false
    }
  }
}

/** Used for implementation and debug. */
class PiCoreSpecTester extends ChiselFlatSpec {

  val elem         = FixedPoint(32.W, 16.BP)
  val nbIterations = 10000 // Nb iterations of Monte Carlo

  "running verilator" should "validate your design" taggedAs (Unit) in {
    PiCoreHelper.verilatorTest(
        elem,
        (TauswortheConfig.default(242642, 156416254, 543242), TauswortheConfig.default(132456, 5727251, 2431)),
        nbIterations
    ) should be(true)
  }
}
