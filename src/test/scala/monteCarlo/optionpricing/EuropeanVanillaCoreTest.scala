/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.monteCarlo.optionpricing

import bench.kernels.monteCarlo.software.EuropeanOptionPricing

import bench.utils.components.random.TauswortheConfig
import bench.utils.exceptions.RepresentationOverflowException
import bench.utils.hardware.RingElem
import bench.utils.tests.CustomTester

import bench.tags._

import plotly.element.HistNorm
import plotly.layout.Layout
import plotly.{Histogram, Plotly}

import logger.LazyLogging

import chisel3._
import chisel3.experimental.FixedPoint
import chisel3.iotesters.ChiselFlatSpec
import dsptools.numbers.Ring

class EuropeanVanillaCoreTest[T <: Data: Ring](
    c: EuropeanVanillaCore[T],
    gen: T,
    config: EuropeanVanillaConfig,
    nbIterations: Int,
    confidence: Double = 0.99
) extends CustomTester(c, gen, 0.25) {
  val sw = new EuropeanOptionPricing(config, nbIterations, confidence)

  var cntValues = 0
  var sum       = 0.0

  while (cntValues < nbIterations) {
    if (peek(c.io.output.valid) == 1) {
      debug("Output value " + peek(c.io.output.bits))
      sum += peek(c.io.output.bits).toDouble
      cntValues += 1
    }
    step(1)
  }

  val hwResult = sum / nbIterations
  val swResult = sw.monteCarloEulerMaruyama()
  expect(
      hwResult >= swResult._2._1 && hwResult <= swResult._2._2,
      "Result (" + hwResult + ") outside of the confidence interval [" +
        swResult._2._1 + "; " + swResult._2._2 + "] (ref = " + swResult._1 + ")"
  )

  logger.info(
      "Hardware result : " + hwResult + " (ref Euler-Maruyama : " +
        EuropeanOptionPricing.resultToString(swResult) + ", sol : " +
        EuropeanOptionPricing.resultToString(sw.monteCarlo()) + ")"
  )
}

class EuropeanVanillaCorePlot[T <: Data: Ring](
    c: EuropeanVanillaCore[T],
    gen: T,
    config: EuropeanVanillaConfig,
    nbIterations: Int
) extends CustomTester(c, gen, 0.25) {
  val array     = new Array[Double](nbIterations)
  var cntValues = 0

  while (cntValues < nbIterations) {
    if (peek(c.io.output.valid) == 1) {
      debug("Output value " + peek(c.io.output.bits))
      val hwValue = peek(c.io.output.bits)
      array(cntValues) = hwValue.toDouble
      cntValues += 1
    }
    step(1)
  }

  Plotly.plot(
      "plots/BoxMullerPDF",
      Seq(Histogram(array.toSeq, histnorm = HistNorm.ProbabilityDensity)),
      Layout(title = "Discrete approximation of the normal distribution with Box-Muller method")
  )
}

object EuropeanVanillaCoreHelper extends LazyLogging {
  def verilatorTest[T <: Data: Ring](
      gen: T,
      urng: (Array[TauswortheConfig], Array[TauswortheConfig]),
      config: EuropeanVanillaConfig,
      boxMullerLUTsize: Int,
      nbIterations: Int
  ): Boolean = {
    try {
      logger.info(s"${this.getClass.getSimpleName} : Verilator tester with elements of type $gen")
      iotesters.Driver.execute(
          Array(
              "--backend-name",
              "verilator",
              "--target-dir",
              "test_run_dir/test_european_vanilla_core",
              "--top-name",
              "EuropeanVanillaCore"
          ),
          () => EuropeanVanillaCore(RingElem(gen), urng, config, boxMullerLUTsize)
      ) { c =>
        new EuropeanVanillaCoreTest(c, gen, config, nbIterations)
      }
    } catch {
      case _: RepresentationOverflowException =>
        logger.warn("Overflow detected on design, not checking validity further")
        true
      case _: Throwable => false
    }
  }

  def plot[T <: Data: Ring](
      gen: T,
      urng: (Array[TauswortheConfig], Array[TauswortheConfig]),
      config: EuropeanVanillaConfig,
      boxMullerLUTsize: Int,
      nbIterations: Int
  ): Boolean = {
    try {
      logger.info(s"${this.getClass.getSimpleName} : Verilator tester with elements of type $gen")
      iotesters.Driver.execute(
          Array(
              "--backend-name",
              "verilator",
              "--target-dir",
              "test_run_dir/test_european_vanilla_core",
              "--top-name",
              "EuropeanVanillaCore"
          ),
          () => EuropeanVanillaCore(RingElem(gen), urng, config, boxMullerLUTsize)
      ) { c =>
        new EuropeanVanillaCorePlot(c, gen, config, nbIterations)
      }
    } catch {
      case _: RepresentationOverflowException =>
        logger.warn("Overflow detected on design, not checking validity further")
        true
      case _: Throwable => false
    }
  }
}

/** Used for implementation and debug. */
class EuropeanVanillaCoreSpecTester extends ChiselFlatSpec {

  val elem         = FixedPoint(32.W, 16.BP)
  val nbIterations = 512 // Nb iterations of Monte Carlo

  val config = new EuropeanVanillaPayOff(
      S0 = 100.0, // Option price
      //strikePrice = 100.0,    // Strike price
      volatility = 0.2,    // Volatility of the underlying (20%)
      riskFreeRate = 0.05, // Risk-free rate (5%)
      maturity = 1,        // One year until expiry
      N = 4                // Nb iterations of Euler-Maruyama
  )

  val boxMullerLUTsize = 256
  val tauswortheConfig = TauswortheConfig.randomTupleIterator(123, elem.getWidth).next()

  "running verilator" should "validate your design" taggedAs (Unit) in {
    EuropeanVanillaCoreHelper.verilatorTest(
        elem,
        tauswortheConfig,
        config,
        boxMullerLUTsize,
        nbIterations
    ) should be(true)
  }
}
