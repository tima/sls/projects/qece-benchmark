/**
 *  Part of chisel based QECE benchmark of FPGA kernels.
 *  Copyright (C) <2021>  <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble.
 *  Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or
 *  Olivier MULLER (olivier.muller@univ-grenoble-alpes.fr)
 *  for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package bench.kernels.monteCarlo.optionpricing

import bench.kernels.monteCarlo._

import bench.utils.components.PosEdgeWrapper
import bench.utils.components.random.TauswortheConfig
import bench.utils.exceptions._
import bench.utils.hardware.RingElem
import bench.utils.software.BitVector

import bench.tags._

import logger.LazyLogging
import scala.collection.mutable.ArrayBuffer

import qece.estimation.qor.ErrorFeaturedPeekPokeTester

import chisel3.experimental._
import chisel3.iotesters.ChiselFlatSpec
import chisel3.util.log2Ceil
import chisel3.{iotesters, _}
import dsptools.numbers._

/* Testing the design */
class EuropeanVanillaTest[T <: Data: Ring](
    mut: PosEdgeWrapper,
    gen: T,
    config: EuropeanVanillaConfig,
    nbSoftIterations: Int,
    nbSimu: Int
) extends ErrorFeaturedPeekPokeTester(mut) {
  implicit val signal = gen

  val sw        = new bench.kernels.monteCarlo.software.EuropeanOptionPricing(config, nbSoftIterations)
  val hwResults = ArrayBuffer[Double]()

  def waitForValue(): Unit = {
    while (peek(mut.io.out.valid) == 0) {
      step(1)
    }
  }

  for (_ <- 0 until nbSimu) {
    waitForValue()
    val hwValue = BitVector.deserialize(peek(mut.io.out.bits), gen.getWidth, gen.getWidth)
    hwResults.append(hwValue(0).toDouble)

    step(1)
  }

  // compute oracle value, using a large number of soft iterations
  val swResult = sw.monteCarlo()

  setNormalizedRootMeanSquareError(hwResults.toArray, Array.tabulate(hwResults.size)(_ => swResult._1))
}

object EuropeanVanillaHelper extends LazyLogging {
  def verilatorTest[T <: Data: Ring](
      gen: T,
      urngConfig: Iterator[(Array[TauswortheConfig], Array[TauswortheConfig])],
      config: EuropeanVanillaConfig,
      boxMullerLUTsize: Int,
      nbIterations: Int,
      nbCore: Int,
      nbSoftIteration: Int = 65536,
      nbSimu: Int = 100,
      maxError: Double = 0.01
  ): Boolean = {
    val elemType = RingElem(gen)
    try {
      logger.info(s"${this.getClass.getSimpleName} : Verilator tester with elements of type $gen")
      iotesters.Driver.execute(
          Array(
              "--backend-name",
              "verilator",
              "--target-dir",
              "test_run_dir/test_european_vanilla",
              "--top-name",
              "EuropeanVanilla"
          ),
          () => {
            new PosEdgeWrapper(
                new MonteCarloRole(
                    elemType,
                    EuropeanVanillaFactory(
                        elemType,
                        urngConfig,
                        config,
                        boxMullerLUTsize,
                        true
                    ),
                    nbIterations,
                    nbCore
                )
            )
          }
      ) { c =>
        val result = new EuropeanVanillaTest(c, gen, config, nbSoftIteration, nbSimu);
        assert(result.getError < maxError); result
      }
    } catch {
      case _: RepresentationOverflowException =>
        logger.warn("Overflow detected on design, not checking validity further")
        true
      case _: Throwable => false
    }
  }
}

class EuropeanVanillaTester extends ChiselFlatSpec {
  val elem             = FixedPoint(32.W, 16.BP)
  val nbIterationsList = List(512, 16384, 131072)
  val nbCoreList       = List(4, 32, 64)
  val NList            = List(4, 10, 50)

  val tauswortheConfigIterator = TauswortheConfig.randomTupleIterator(123, elem.getWidth)

  val boxMullerLUTsize = 256 // Configuration of Box Muller precision (number of entry in the ROM)

  for (nbIterations <- nbIterationsList) {
    for (nbCore <- nbCoreList) {
      for (n <- NList) {
        val callPriceConfig = new EuropeanVanillaCallPrice(
            S0 = 100.0,          // Option price
            strikePrice = 100.0, // Strike price
            volatility = 0.2,    // Volatility of the underlying (20%)
            riskFreeRate = 0.05, // Risk-free rate (5%)
            maturity = 1,        // One year until expiry
            N = n                // Nb iterations of Euler-Maruyama
        )
        val putPriceConfig = new EuropeanVanillaPutPrice(
            S0 = 100.0,          // Option price
            strikePrice = 100.0, // Strike price
            volatility = 0.2,    // Volatility of the underlying (20%)
            riskFreeRate = 0.05, // Risk-free rate (5%)
            maturity = 1,        // One year until expiry
            N = n                // Nb iterations of Euler-Maruyama
        )
        val payOffConfig = new EuropeanVanillaPayOff(
            S0 = 100.0,          // Option price
            volatility = 0.2,    // Volatility of the underlying (20%)
            riskFreeRate = 0.05, // Risk-free rate (5%)
            maturity = 1,        // One year until expiry
            N = n                // Nb iterations of Euler-Maruyama
        )
        s"testing European Vanilla call price (nbIterations: $nbIterations, nbCore: $nbCore, N: $n)" should
          "validate the design" taggedAs (Kernel, Long, Probabilistic) in {
          EuropeanVanillaHelper.verilatorTest(
              elem,
              tauswortheConfigIterator,
              callPriceConfig,
              boxMullerLUTsize,
              nbIterations,
              nbCore
          ) should be(true)
        }
        s"testing European Vanilla put price (nbIterations: $nbIterations, nbCore: $nbCore, N: $n)" should
          "validate the design" taggedAs (Kernel, Long, Probabilistic) in {
          EuropeanVanillaHelper.verilatorTest(
              elem,
              tauswortheConfigIterator,
              putPriceConfig,
              boxMullerLUTsize,
              nbIterations,
              nbCore
          ) should be(true)
        }
        s"testing European Vanilla pay off (nbIterations: $nbIterations, nbCore: $nbCore, N: $n)" should
          "validate the design" taggedAs (Kernel, Long, Probabilistic) in {
          EuropeanVanillaHelper.verilatorTest(
              elem,
              tauswortheConfigIterator,
              payOffConfig,
              boxMullerLUTsize,
              nbIterations,
              nbCore
          ) should be(true)
        }
      }
    }
  }
}

/** Used for implementation and debug. */
class EuropeanVanillaSpecTester extends ChiselFlatSpec with LazyLogging {
  // Monte Carlo generic config
  val elem         = FixedPoint(64.W, 32.BP)
  val nbIterations = 512
  val nbCore       = 4

  // Black Scholes specific config
  // val tauswortheConfigIterator = TauswortheConfig.randomTupleIterator(123, elem.getWidth)
  val tauswortheConfigIterator = TauswortheConfig.randomTupleIterator(42, elem.getWidth)
  val config = new EuropeanVanillaPayOff(
      S0 = 100.0, // Option price
      //strikePrice     = 100.0,   // Strike price
      volatility = 0.1,   // Volatility of the underlying (20%)
      riskFreeRate = 0.2, // Risk-free rate (5%)
      maturity = 1,       // One year until expiry
      N = 40              // Nb iterations of Euler-Maruyama
  )
  val boxMullerLUTsize = 256 // Configuration of Box Muller precision (number of entry in the ROM)

  // Overflow detection (supposing that the result will be near to S0). ONLY FOR PAY OFF.
  if (elem.getWidth - elem.binaryPoint.get < log2Ceil(math.ceil(nbIterations * config.S0).toInt)) {
    logger.warn("WARNING : An overflow can occur during the simulation")
  }

  "running verilator" should "validate Monte Carlo with Black Scholes cores" taggedAs (Kernel, Probabilistic) in {
    EuropeanVanillaHelper.verilatorTest(
        elem,
        tauswortheConfigIterator,
        config,
        boxMullerLUTsize,
        nbIterations,
        nbCore
    ) should be(true)
  }
}
