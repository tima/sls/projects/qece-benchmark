Chisel Benchmark
================

This project aims at providing a realistic, open-source benchmark for FPGA applications accelerators written using Chisel.<br/>

Each kernel is designed to be easily explorable, exposing high level parameters that directly impact accelerator generation.<br/>

A brief overview is given here.
However, you can check the [benchmark wiki](https://gricad-gitlab.univ-grenoble-alpes.fr/tima/sls/projects/qece-benchmark/-/wikis/home) for more information.

## Kernels

We propose 5 different kernels for FPGA acceleration.

Each kernel comes with a **verilog generation utils** in the project hierarchy. To modify the generated accelerators, you need to modify the `<KERNEL>Emitter.scala` file.

#### **Deflate**

Chisel implementation of the deflate algorithm.

Parameters are:

- **I/O bitwidth**: size of the bus I/O (number of bits sent each cycle)
- **depth**: lz77 dictionary depth
- **literalHuffCodes**: Huffman table used to encode literals
- **distanceHuffCodes**: Huffman table used to encode distance

#### **Fast Fourier Transform**

Chisel implementation of a parametrizable FFT accelerator.

Parameters are:

- **I/O bitwidth**: size of the bus I/O (number of bits sent each cycle)
- **element type**: type of elements used in the accelerator
- **size**: size of performed FFT

#### **Finite Impulse Response filter**

Chisel implementation of a parametrizable FIR filter accelerator.

Parameters are:

- **I/O bitwidth**: size of the bus I/O (number of bits sent each cycle)
- **element type**: type of elements used in the accelerator
- **coefficients**: coefficients of the filter being used

#### **General Matrix Multiply**

Chisel implementation of a parametrizable dense generic matrix multiply algorithm.

Parameters are:

- **I/O bitwidth**: size of the bus I/O (number of bits sent each cycle)
- **element type**: type of elements used in the accelerator
- **size**: dimension of the matrix being computed

#### **Monte Carlo**

Generic chisel implementation of the **monte carlo** method, applied to **pi computation** and **black scholes pricing**.

Parameters are:

- **element type**: type of elements used in the accelerator
- **factory**: used for building the accelerator - we use a **Factory pattern** for all monte carlo based kernels
- **nbIterations**: number of iterations to be runned in the monte carlo process
- **nbCore**: number of parallel core to run the iterations

For **Black Scholes** pricing, another parameter is used to define the number of **Euler Maruyama** iteration to approximate exponential.

#### **MultiLayer Perceptron**

A generic implementation of a multilayer perceptron is currently being developed as a sub project, and will be provided as soon as possible.

## QECE

This project uses [**QECE** (*Quick Exploration using Chisel Estimators*)](https://gricad-gitlab.univ-grenoble-alpes.fr/tima/sls/projects/qece) project for **Design Space Exploration**.

You thus need to install it, either by **cloning and locally publishing it**, or by retrieving it from a distant repository.
Please check the dedicated installation page on the QECE project for more information.

## Reference

If you plan on using this benchmark in your own work and publish it, please consider citing the following reference:
```
@article{10.1145/3590769,
  author = {Ferres, Bruno and Muller, Olivier and Rousseau, Fr\'{e}d\'{e}ric},
  title = {A Chisel Framework for Flexible Design Space Exploration through a Functional Approach},
  year = {2023},
  publisher = {Association for Computing Machinery},
  address = {New York, NY, USA},
  issn = {1084-4309},
  url = {https://doi.org/10.1145/3590769},
  doi = {10.1145/3590769},
  journal = {ACM Trans. Des. Autom. Electron. Syst.},
}
```

## Contact

Feel free to contact the [maintainer of the project](mailto:olivier.muller@univ-grenoble-alpes.fr).

The project was developed by [Bruno FERRES](mailto:bruno.ferres@grenoble-inp.org) in **SLS team** at [TIMA lab](http://tima.univ-grenoble-alpes.fr/tima/fr/index.html), under the supervision of [Frédéric ROUSSEAU](mailto:frederic.rousseau@univ-grenoble-alpes.fr) and [Olivier MULLER](mailto:olivier.muller@univ-grenoble-alpes.fr).
