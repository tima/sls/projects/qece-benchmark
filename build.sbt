// See README.md for license details.

ThisBuild / scalaVersion     := "2.12.12"
ThisBuild / version          := "1.1.0"
ThisBuild / organization     := "fr.tima"

parallelExecution in Test := true

lazy val root = (project in file("."))
.settings(
    name := "qece-benchmark",
    logLevel := Level.Warn,
    libraryDependencies ++= 
    Seq(
        "edu.berkeley.cs" %% "chisel3" % "3.4.3",
        "edu.berkeley.cs" %% "chisel-iotesters" % "1.5.3",
        "edu.berkeley.cs" %% "dsptools" % "1.4.3"
    ) ++ Seq(
        "org.plotly-scala" %% "plotly-render" % "0.7.2"
    ) ++ Seq( // qece import
        "fr.tima" %% "qece" % "1.1.0"
    ) ++ Seq( // used for reflectivity
        "org.scala-lang" % "scala-compiler" % scalaVersion.value
    ) ++ Seq(
        "org.apache.commons" % "commons-math3" % "3.3"
    ),
    scalacOptions ++= Seq(
        "-Xsource:2.11",
        "-language:existentials",
        "-language:implicitConversions",
        "-language:reflectiveCalls",
        "-deprecation",
        "-feature",
        "-unchecked",
        "-Ywarn-unused",
        "-Xcheckinit"
    ),
    javacOptions ++= Seq(
        "-source:1.8",
        "-target:1.8"
    ),
    // TODO: fix bug in data type
    // addCompilerPlugin("edu.berkeley.cs" % "chisel3-plugin" % "3.4.2" cross CrossVersion.full), 
    addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.1" cross CrossVersion.full)
)


// ======== Aliases ==========

// short tests - i.e. tests that are not tagged as 'Long' 
// run unit tests only
addCommandAlias("unitTests", "testOnly -- -n Unit -l Probabilistic -l Long -l Deprecated")

// run functioning tests over the different kernels
addCommandAlias("kernelTests", "testOnly -- -n Kernel -l Probabilistic -l Long -l Deprecated")

// run tests that are tagged as probabilistic - i.e. that may fail
addCommandAlias("probaTests", "testOnly -- -n Probabilistic -l Deprecated -l Long")

// run every kernel test, even the ones tagged as 'Long'
// kernel tests
addCommandAlias("fullKernelTests", "testOnly -- -n Kernel -l Probabilistic -l Deprecated")

// probabilistic tests
addCommandAlias("fullProbaTests", "testOnly -- -n Probabilistic -l Deprecated")

// every test
addCommandAlias("fullTests", "testOnly -- -l Probabilistic -l Deprecated")

